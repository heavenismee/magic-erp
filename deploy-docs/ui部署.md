# UI脚本部署

## 下载源码

```bash
# 克隆项目
mkdir /opt/erp/source && cd /opt/erp/source
git clone https://gitee.com/javastore/magic-erp-ui.git --branch 2.0.1
git clone https://gitee.com/bbc-se/deploy-script.git
```

## 安装基础环境

> 基础环境包括nodejs、pm2、nginx
> 注意：这个命令会禁用centos7的防火墙，如果不需要禁用防火墙，请编辑deploy.sh中第66行相关代码再运行。

```bash
cd /opt/erp/source/ui
bash ./deploy.sh base
```

## 修改域名配置

在这之前您需要规划好您的域名

~~~bash
vi /opt/erp/source/deploy-script/nginx/create-cfg.sh
~~~

修改其中的 域名及端口信息。

然后进入到脚本目录下：

```bash
cd /opt/erp/source/deploy-script/nginx/
```

非SSL站点，执行脚本生成配置文件：

~~~bash
sh create-cfg.sh
~~~

SSL站点，执行脚本生成配置文件：

~~~bash
sh create-cfg.sh ssl
~~~

## 启动ui

```bash
cd /opt/erp/source/ui
bash ./deploy.sh
```

## 启动nginx

```bash
/usr/local/nginx/sbin/nginx
```

### 验证

访问相应域名

如果重起nginx，则：

~~~bash
/usr/local/nginx/sbin/nginx -s reload
~~~



