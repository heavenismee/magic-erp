# API部署

安装常用工具

```bash
sudo yum install  -y  yum-utils wget vim mtr curl telent lsof git
```

## 安装jdk

下载jdk

```
mkdir -p /opt
cd /opt
wget https://file.shoptnt.cn/jdk/jdk-8u281-linux-x64.tar.gz
```

解压缩并重命名：

```
tar -xzvf jdk-8u281-linux-x64.tar.gz  
mv jdk1.8.0_281/ jdk8
```

配置环境变量：

```
vi /etc/profile
```

在文件的最后配置如下内容：

```
JAVA_HOME=/opt/jdk8
JRE_HOME=/opt/jdk8/jre
CLASS_PATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar:$JRE_HOME/lib
PATH=$PATH:$JAVA_HOME/bin:$JRE_HOME/bin
export JAVA_HOME JRE_HOME CLASS_PATH PATH
```

> 注意可能存在已有配置,请修改为正确的路径

使配置文件生效：

```
source /etc/profile
```

验证

```
java -version
```

> 如果不正确，可能需要退出重新登录

## 安装maven

~~~
cd /opt
wget http://mirrors.tuna.tsinghua.edu.cn/apache/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz
yum install -y vim 
~~~

~~~
tar -xzvf apache-maven-3.6.3-bin.tar.gz
~~~

配置环境变量：

```
vi /etc/profile
```

在文件的最后配置如下内容（增加MAEN_HOME并修改path）：

```
#其它略
MAVEN_HOME=/opt/apache-maven-3.6.3
PATH=$PATH:$JAVA_HOME/bin:$JRE_HOME/bin:$MAVEN_HOME/bin
#其它略
```

> 注意可能存在已有配置,请修改为正确的路径

使配置文件生效：

```
source /etc/profile
```

验证

```
mvn -v
```

### 配置maven私服

~~~bash
vim /opt/apache-maven-3.6.3/conf/settings.xml
~~~

在mirrors节点中新增mirror节点:

~~~xml
<mirror>
    <id>alimaven</id>
    <mirrorOf>central</mirrorOf>
    <name>aliyun maven</name>
    <url>https://maven.aliyun.com/repository/central</url>
</mirror>
~~~

在profiles节点中新增一个profile节点：

~~~xml
<profile>  
  <id>alimaven</id>  
  <repositories>  
    <repository>  
        <id>alimaven</id>  
      <name>aliyun maven</name>
      <url>https://maven.aliyun.com/repository/central</url>
        <releases><enabled>true</enabled></releases>  
        <snapshots><enabled>true</enabled></snapshots>  
    </repository>  
  </repositories>  
</profile>  
~~~

在activeProfiles节点中新增一个activeProfile 节点：

~~~xml
<activeProfiles>
  <activeProfile>alimaven</activeProfile>
</activeProfiles>  
~~~

注意：以上代码不要写到注释里，请仔细检查！


## clone源码

在这之前，请您参考"[git部署公钥指南](/deploy/nok8s/create-ssh-rsa)"

>  作为规范，我们约定源码全部放在  /opt/erp/source目录中，jar放在/opt/erp/server中

```
mkdir -p /opt/erp/server
mkdir -p /opt/erp/source
cd /opt/erp/source
```


import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

<Tabs>
 <TabItem value="erp" label="erp" default>

clone我们需要的源码：

```
git clone git@gitee.com:javastore/magic-erp.git
```

或者

```
git clone https://gitee.com/javastore/magic-erp.git
```

进行初次编译

~~~bash
cd /opt/erp/source/api
mvn clean install -DskipTests -f pom.xml
~~~


  </TabItem>
</Tabs>


### 配置config

```
配置源码中 server/src/main/resources/application-dev.yml文件
如果是生产环境则配置 application-prod.yml
配置好后提交代码，服务器这边记得重新拉一下代码
```

### 建立其它配置文件

~~~
mkdir -p /opt/erp/server/{config,logs}
~~~

~~~
vim /opt/erp/server/config/application.yml
~~~

粘贴如下配置

~~~
spring:
  profiles:
    active: dev
~~~

 如果是生产环境，将dev改为prod     

### 建立执行脚本

~~~
vim /opt/erp/server/start.sh
~~~

<Tabs>
<TabItem value="erp" label="erp">

粘贴如下内容：

```sh
#!/bin/bash

#版本
verson=7.2.2

#要部署的分支
branch=master

#源文件存储目录
source_dir=/opt/erp/source/api

#程序运行时目录
server_dir=/opt/erp/server
mkdir -p $server_dir"/logs/"
#要运行的api
apis=("server")
# 杀死进程
killone(){
ps -ef |grep java |grep $1  |grep -v 'grep'|awk '{print $2}'  | xargs kill -9
echo '停止'$1
}
# 启动程序
startone(){
nohup java -Xmx512m -Xms256m -Xss1024k  -jar $1"-"$verson.jar > $server_dir"/logs/"$1".out"  2>&1 &
echo '启动'$1
}
# 默认全部启动
startall="yes"
for api_name in  ${apis[@]}
do
if [[ "$1" != "" && "$1" = $api_name  && "$1" != "dontbuild" ]]
then
startall="no"
break
fi
done

echo "startall is "$startall

if [[ "$startall" = "yes" ]]; then
#杀死进程
for api_name in  ${apis[@]}
do
killone "$api_name"
echo "$api_name stop success"

    done
else
killone "$1"
fi

#检测端口状态
checkPort(){
r=`netstat -ntlp | awk '{print $4}' | grep :$1`
if [ "$r" == "" ]
then
return 0
else
return 1
fi

}

if [ "$1" != "dontbuild" ]
then
#更新源码
cd  $source_dir
git reset --hard $branch
git pull
mvn -T 1C clean install -DskipTests -f pom.xml
rm -rf $server_dir"/*.jar"

    for api_name in  ${apis[@]}
    do
        mv $source_dir"/"$api_name"/target/"$api_name"-"$verson.jar $server_dir
    done
else
echo "不构建，直接重启"    
fi

cd $server_dir

if [[ "$startall" == "yes" ]]; then
for api_name in  ${apis[@]}
do
startone "$api_name"
done
else
startone "$1"
fi

#进度条
progressBar(){

label=('|' '/' '-' '\')

per=$1
ii=0
str=''
myTimes=$2
text=$3
let index=myTimes%4
#拼接#字
while [ $ii -le  $per ]
do
str=#$str
let ii++
done

printf "[%-100s]%d%%  %s %s\r" $str $per ${label[$index]} $text


if [[ $per == 100 ]]; then
echo
fi

}

#声明所有的服务和端口号，以便检测
serviceMap["8080"]="server"


totalPer=0

#是否全部启动成功的标识
all_is_ok=""
last_text=''
times=0
processNum=0
processItem=100/1 #七个任务中每个任务所占进度
while :
do
all_is_ok="yes"
progressBar $totalPer $times $last_text

    #100次检查一下
    checked=$(($times%100))

    if [[ $checked == 0 ]]; then
        for key in ${!serviceMap[*]};do
           checkPort "$key"
           started_result=`echo $?`
           if [ "$started_result" == "1" ]
           then
             processNum=$(($processNum+1))
             #每启动一个服务进度
             totalPer=$(($processNum*$processItem))
             #显示启动成功的程序
             printf "%-57s%57s\n" ${serviceMap[$key]} '已启动' | sed 's/[ ]/-/g'
             #将启动成功的服务移除掉
             unset serviceMap["$key"]
           fi
        done
    fi


    sleep 0.1
    let times++
    if [[ ${#serviceMap[*]} == 0 ]]; then
        progressBar 100 1 "全部启动完成"
        break
    fi
done



```

</TabItem>
</Tabs>




进入脚本文件夹中：

~~~
cd /opt/erp/server
~~~

更新源码并且部署执行如下脚本：

~~~
sh start.sh
~~~

不更新源码直接重启执行如下脚本

~~~
sh start.sh dontbuild
~~~


### 验证


全部启动成功后，通过以下网址检查API是否成功。

 http://ip:8080/swagger-ui.html