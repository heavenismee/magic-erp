DROP TABLE IF EXISTS `system_oauth2_access_token`;
CREATE TABLE `system_oauth2_access_token` (
    `id` bigint(20) NOT NULL COMMENT '编号',
    `user_id` bigint(20) DEFAULT NULL COMMENT '用户编号',
    `user_type` tinyint(4) DEFAULT NULL COMMENT '用户类型',
    `access_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '访问令牌',
    `refresh_token` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '刷新令牌',
    `client_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '客户端编号',
    `scopes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '授权范围',
    `expires_time` bigint(20) DEFAULT NULL COMMENT '过期时间',
    `creator` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '创建者',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `updater` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '更新者',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_access_token` (`access_token`(191)) USING BTREE,
    KEY `idx_refresh_token` (`refresh_token`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='OAuth2 访问令牌';

DROP TABLE IF EXISTS `system_oauth2_approve`;
CREATE TABLE `system_oauth2_approve` (
    `id` bigint(20) NOT NULL COMMENT '编号',
    `user_id` bigint(20) DEFAULT NULL COMMENT '用户编号',
    `user_type` tinyint(4) DEFAULT NULL COMMENT '用户类型',
    `client_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '客户端编号',
    `scope` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '授权范围',
    `approved` bit(1) DEFAULT b'0' COMMENT '是否接受',
    `expires_time` bigint(20) DEFAULT NULL COMMENT '过期时间',
    `creator` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '创建者',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `updater` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '更新者',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='OAuth2 批准表';

DROP TABLE IF EXISTS `system_oauth2_client`;
CREATE TABLE `system_oauth2_client` (
    `id` bigint(20) NOT NULL COMMENT '编号',
    `client_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '客户端编号',
    `secret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '客户端密钥',
    `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '应用名',
    `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '应用图标',
    `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '应用描述',
    `status` tinyint(4) DEFAULT NULL COMMENT '状态 0正常 1禁用',
    `access_token_validity_seconds` int(11) DEFAULT NULL COMMENT '访问令牌的有效期',
    `refresh_token_validity_seconds` int(11) DEFAULT NULL COMMENT '刷新令牌的有效期',
    `redirect_uris` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '可重定向的 URI 地址',
    `authorized_grant_types` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '授权类型',
    `scopes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '授权范围',
    `auto_approve_scopes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '自动通过的授权范围',
    `creator` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '创建者',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `updater` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '更新者',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    `deleted` bit(1) DEFAULT b'0' COMMENT '是否删除',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='OAuth2 客户端表';

DROP TABLE IF EXISTS `system_oauth2_code`;
CREATE TABLE `system_oauth2_code` (
    `id` bigint(20) NOT NULL COMMENT '编号',
    `user_id` bigint(20) DEFAULT NULL COMMENT '用户编号',
    `user_type` tinyint(4) DEFAULT NULL COMMENT '用户类型',
    `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '授权码',
    `client_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '客户端编号',
    `scopes` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '授权范围',
    `expires_time` bigint(20) DEFAULT NULL COMMENT '过期时间',
    `redirect_uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '可重定向的 URI 地址',
    `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '状态',
    `creator` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '创建者',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `updater` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '更新者',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='OAuth2 授权码表';

DROP TABLE IF EXISTS `system_oauth2_refresh_token`;
CREATE TABLE `system_oauth2_refresh_token` (
    `id` bigint(20) NOT NULL COMMENT '编号',
    `user_id` bigint(20) DEFAULT NULL COMMENT '用户编号',
    `refresh_token` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '刷新令牌',
    `user_type` tinyint(4) DEFAULT NULL COMMENT '用户类型',
    `client_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '客户端编号',
    `scopes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '授权范围',
    `expires_time` bigint(20) DEFAULT NULL COMMENT '过期时间',
    `creator` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '创建者',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `updater` varchar(64) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '更新者',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='OAuth2 刷新令牌';