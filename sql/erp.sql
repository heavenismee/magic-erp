ALTER TABLE `es_menu`
    ADD COLUMN `type` VARCHAR (20) NULL COMMENT '菜单类型' AFTER `grade`;

DROP TABLE IF EXISTS `system_dict_type`;
CREATE TABLE `system_dict_type` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
    `name` VARCHAR(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '字典名称',
    `type` VARCHAR(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '字典类型',
    `status` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
    `remark` VARCHAR(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
    `creator` VARCHAR(64) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '创建者',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    `updater` VARCHAR(64) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '更新者',
    `deleted` BIT(1) NOT NULL DEFAULT b'0' COMMENT '是否删除',
    `deleted_time` bigint(20) DEFAULT NULL COMMENT '删除时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='字典类型表';

DROP TABLE IF EXISTS `system_dict_data`;
CREATE TABLE `system_dict_data` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
    `sort` int(11) NOT NULL DEFAULT '0' COMMENT '字典排序',
    `label` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '字典标签',
    `value` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '字典键值',
    `dict_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '字典类型',
    `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
    `color_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '颜色类型',
    `css_class` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'css 样式',
    `remark` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
    `default_flag` bit(1) DEFAULT b'0' COMMENT '是否默认',
    `creator` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '创建者',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    `updater` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '更新者',
    `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='字典数据表';

DROP TABLE IF EXISTS `erp_supplier`;
CREATE TABLE `erp_supplier` (
    `id` bigint(20) NOT NULL COMMENT '主键',
    `custom_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '客户名称',
    `custom_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '客户编号',
    `transfer` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '传值',
    `zipcode` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '邮政编码',
    `postal_address` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '通讯地址',
    `telephone` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '联系电话',
    `company_website` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '公司网站',
    `linkman` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '联系人',
    `email` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '电子邮件',
    `mobile` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '手机号码',
    `production_address` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '生产地址',
    `remark` longtext COLLATE utf8_bin COMMENT '备注',
    `disable_flag` bit(1) DEFAULT b'0' COMMENT '是否禁用',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    `creator` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
    `updater` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='供应商';

DROP TABLE IF EXISTS `erp_post`;
CREATE TABLE `erp_post` (
    `id` bigint(20) NOT NULL,
    `name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '岗位名称',
    `sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '岗位编号',
    `sort` int(11) DEFAULT NULL COMMENT '顺序',
    `description` longtext COLLATE utf8_bin COMMENT '岗位描述',
    `members` longtext COLLATE utf8_bin COMMENT '岗位成员',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    `creator` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
    `updater` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='岗位';

DROP TABLE IF EXISTS `system_dept`;
CREATE TABLE `system_dept` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
    `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '部门名称',
    `sn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '部门编码',
    `parent_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '父部门id',
    `region_ids` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '所在地区id',
    `region_names` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '所在地区名称',
    `type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '部门类型',
    `sort` int(11) NOT NULL DEFAULT '0' COMMENT '显示顺序',
    `remark` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '部门说明',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    `creator` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '创建者',
    `updater` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '更新者',
    `deleted` bit(1) DEFAULT b'0' COMMENT '是否删除',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='部门表';

ALTER TABLE `es_admin_user`
    ADD COLUMN `sn` VARCHAR (255) NULL COMMENT '用户编号',
    ADD COLUMN `dept_id` BIGINT NULL COMMENT '所属部门id',
    ADD COLUMN `post_id` BIGINT NULL COMMENT '所属岗位id',
    ADD COLUMN `sort` INT DEFAULT 0 NULL COMMENT '顺序',
    ADD COLUMN `nickname` VARCHAR (255) NULL COMMENT '昵称';



DROP TABLE IF EXISTS `erp_product`;
CREATE TABLE `erp_product` (
    `id` bigint(20) NOT NULL COMMENT '主键',
    `goods_id` bigint(20) DEFAULT NULL COMMENT '商品id',
    `sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品编号',
    `price` decimal(20,2) DEFAULT NULL COMMENT '销售价格',
    `cost_price` decimal(20,2) DEFAULT NULL COMMENT '成本价格',
    `mkt_price` decimal(20,2) DEFAULT NULL COMMENT '市场价格',
    `tax_rate` decimal(20,2) DEFAULT NULL COMMENT '税率',
    `weight` decimal(20,2) DEFAULT NULL COMMENT '重量',
    `unit` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '单位',
    `warning_value` int(11) DEFAULT '0' COMMENT '库存预警值',
    `specification` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '规格',
    `specs` longtext COLLATE utf8_bin COMMENT '规格列表',
    `name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品名称',
    `category_id` bigint(20) DEFAULT NULL COMMENT '分类id',
    `brand_id` bigint(20) DEFAULT NULL COMMENT '品牌id',
    `market_enable` bit(1) DEFAULT b'1' COMMENT '是否上架',
    `thumbnail` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '商品默认缩略图',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    `creator` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
    `updater` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='产品';

DROP TABLE IF EXISTS `erp_warehouse`;
CREATE TABLE `erp_warehouse` (
    `id` bigint(20) NOT NULL,
    `name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '仓库名称',
    `sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '仓库编号',
    `admin_id` bigint(255) DEFAULT NULL COMMENT '管理员id',
    `description` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '描述',
    `dept_id` bigint(20) DEFAULT NULL COMMENT '部门id',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    `creator` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
    `updater` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
    `deleted` bit(1) DEFAULT b'0' COMMENT '是否删除',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='仓库';

DROP TABLE IF EXISTS `erp_no_generate_rule`;
CREATE TABLE `erp_no_generate_rule` (
    `id` bigint(20) NOT NULL COMMENT '主键',
    `type` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '业务类型',
    `name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '规则名称',
    `seq_generate_type` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '顺序号的重置规则',
    `seq_begin_number` bigint(20) DEFAULT NULL COMMENT '顺序号的起始值',
    `open_flag` bit(1) DEFAULT NULL COMMENT '是否启用该规则',
    `item_list` longtext COLLATE utf8_bin COMMENT '规则项列表',
    `curr_seq_number` bigint(20) DEFAULT NULL COMMENT '当前顺序号',
    `seq_last_generate_time` datetime DEFAULT NULL COMMENT '最后一次生成顺序号的时间',
    `lock_version` bigint(20) DEFAULT '0' COMMENT '乐观锁版本号',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    `creator` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
    `updater` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='编号生成规则';

ALTER TABLE `es_role`
    ADD COLUMN `data_scope` VARCHAR (20) NULL COMMENT '数据权限的类型',
    ADD COLUMN `dept_ids` VARCHAR (1000) NULL COMMENT '部门id集合';

DROP TABLE IF EXISTS `erp_warehouse_entry`;
CREATE TABLE `erp_warehouse_entry` (
    `id` bigint(20) NOT NULL,
    `sn` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '入库单编号',
    `entry_time` bigint(20) DEFAULT NULL COMMENT '入库时间',
    `supplier_id` bigint(20) DEFAULT NULL COMMENT '供应商id',
    `supplier_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '供应商编号',
    `dept_id` bigint(20) DEFAULT NULL COMMENT '部门id',
    `warehouse_id` bigint(20) DEFAULT NULL COMMENT '仓库id',
    `dept_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '部门名称',
    `supplier_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '供应商名称',
    `warehouse_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '仓库名称',
    `contract_id` bigint(20) DEFAULT NULL COMMENT '合同id',
    `handled_by_id` bigint(20) DEFAULT NULL COMMENT '经手人id',
    `handled_by_name` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '经手人名称',
    `plate_number` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '车牌号',
    `purchase_plan_id` bigint(20) DEFAULT NULL COMMENT '采购计划id',
    `delivery_number` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '送货单号',
    `contract_attachment` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '合同附件',
    `audit_by` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '审核人',
    `status` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '入库单状态',
    `audit_remark` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '审核备注',
    `procurement_plan_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '采购计划编号',
    `contract_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '合同编号',
    `supplier_settlement_flag` bit(1) DEFAULT NULL COMMENT '是否已生成供应商结算单',
    `supplier_settlement_id` bigint(20) DEFAULT NULL COMMENT '供应商结算单id',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    `creator` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
    `updater` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='入库单';

DROP TABLE IF EXISTS `erp_warehouse_entry_product`;
CREATE TABLE `erp_warehouse_entry_product` (
    `id` bigint(20) NOT NULL,
    `warehouse_entry_id` bigint(20) DEFAULT NULL COMMENT '入库单id',
    `goods_id` bigint(20) DEFAULT NULL COMMENT '商品id',
    `product_id` bigint(20) DEFAULT NULL COMMENT '产品id',
    `sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品编号',
    `name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品名称',
    `specification` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品规格型号',
    `brand` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品品牌',
    `unit` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品单位',
    `product_barcode` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品条形码',
    `category_id` bigint(20) DEFAULT NULL COMMENT '产品分类id',
    `category_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品分类名称',
    `contract_num` int(11) DEFAULT NULL COMMENT '合同数量',
    `num` int(11) DEFAULT NULL COMMENT '本次入库数量',
    `contract_price` decimal(20,2) DEFAULT NULL COMMENT '合同单价',
    `total_price` decimal(20,2) DEFAULT NULL COMMENT '入库总价',
    `cost_price` decimal(20,2) DEFAULT NULL COMMENT '进货单价',
    `tax_rate` decimal(20,2) DEFAULT NULL COMMENT '税率',
    `return_num` int(11) DEFAULT NULL COMMENT '退货数量',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='入库单商品明细';

DROP TABLE IF EXISTS `erp_warehouse_entry_batch`;
CREATE TABLE `erp_warehouse_entry_batch` (
    `id` bigint(20) NOT NULL,
    `warehouse_entry_id` bigint(20) DEFAULT NULL COMMENT '入库单id',
    `warehouse_entry_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '入库单编号',
    `warehouse_entry_item_id` bigint(20) DEFAULT NULL COMMENT '入库单明细id',
    `sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '批次编号',
    `warehouse_id` bigint(20) DEFAULT NULL COMMENT '仓库id',
    `goods_id` bigint(20) DEFAULT NULL COMMENT '商品id',
    `product_id` bigint(20) DEFAULT NULL COMMENT '产品id',
    `product_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品编号',
    `product_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品名称',
    `product_specification` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品规格',
    `product_unit` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品单位',
    `product_barcode` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品条形码',
    `category_id` bigint(20) DEFAULT NULL COMMENT '产品分类id',
    `category_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品分类名称',
    `entry_num` int(11) DEFAULT NULL COMMENT '入库数量',
    `entry_price` decimal(20,2) DEFAULT NULL COMMENT '入库单价',
    `product_cost_price` decimal(20,2) DEFAULT NULL COMMENT '进货单价',
    `tax_rate` decimal(20,2) DEFAULT NULL COMMENT '税率',
    `remain_num` int(11) DEFAULT NULL COMMENT '剩余数量',
    `create_source` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '创建来源',
    `entry_time` bigint(20) DEFAULT NULL COMMENT '入库时间',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    `creator` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
    `updater` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='入库批次';

-- ----------------------------
-- Table structure for es_system_logs
-- ----------------------------
DROP TABLE IF EXISTS `es_system_logs`;
CREATE TABLE `es_system_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `method` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '请求方法',
  `params` longtext COLLATE utf8mb4_bin COMMENT '请求参数',
  `operate_time` bigint(20) DEFAULT NULL COMMENT '操作时间',
  `operate_detail` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '操作描述',
  `operate_ip` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'ip地址',
  `operator_name` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '操作员',
  `operator_id` bigint(20) DEFAULT NULL COMMENT '操作员ID',
  `seller_id` bigint(20) DEFAULT NULL COMMENT '商家ID',
  `level` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '级别',
  `loginClient` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '客户端',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `ind_sys_log` (`loginClient`,`level`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统日志表(es_system_logs)';

DROP TABLE IF EXISTS `erp_warehouse_out`;
CREATE TABLE `erp_warehouse_out` (
    `id` bigint(20) NOT NULL,
    `sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '出库单编号',
    `out_time` bigint(20) DEFAULT NULL COMMENT '出库日期',
    `dept_id` bigint(20) DEFAULT NULL COMMENT '部门id',
    `warehouse_id` bigint(20) DEFAULT NULL COMMENT '仓库id',
    `dept_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '部门名称',
    `warehouse_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '仓库名称',
    `member_real_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '用户姓名',
    `member_mobile` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '用户电话',
    `ship_region_ids` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '收货地区id集合',
    `ship_region_names` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '收货地区名称集合',
    `ship_addr` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '收货地址',
    `consignee` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '提货人',
    `order_id_list` varchar(1000) COLLATE utf8_bin DEFAULT NULL COMMENT '订单id集合',
    `order_sn_list` varchar(1000) COLLATE utf8_bin DEFAULT NULL COMMENT '订单编号集合',
    `delivery_type` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '配送方式',
    `status` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '状态',
    `logistics_company_id` bigint(20) DEFAULT NULL COMMENT '物流公司id',
    `logistics_company_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '物流公司名称',
    `tracking_number` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '物流单号',
    `freight_price` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '物流费用',
    `store_id` bigint(20) DEFAULT NULL COMMENT '自提门店id',
    `store_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '自提门店名称',
    `ship_time` bigint(20) DEFAULT NULL COMMENT '发货时间',
    `create_by` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '制单人',
    `audit_by_id` bigint(20) DEFAULT NULL COMMENT '审核人id',
    `audit_by_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '审核人名称',
    `warehouse_consignee` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '仓库提货人',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    `creator` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
    `updater` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='出库单';

DROP TABLE IF EXISTS `erp_warehouse_out_product`;
CREATE TABLE `erp_warehouse_out_product` (
    `id` bigint(20) NOT NULL COMMENT '主键',
    `warehouse_out_id` bigint(20) DEFAULT NULL COMMENT '出库单id',
    `warehouse_entry_id` bigint(20) DEFAULT NULL COMMENT '入库单id',
    `warehouse_entry_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '入库单编号',
    `warehouse_entry_item_id` bigint(20) DEFAULT NULL COMMENT '入库单明细id',
    `warehouse_entry_batch_id` bigint(20) DEFAULT NULL COMMENT '入库批次id',
    `warehouse_entry_batch_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '入库批次号',
    `order_id` bigint(20) DEFAULT NULL COMMENT '订单id',
    `order_sn` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '订单编号',
    `goods_id` bigint(20) DEFAULT NULL COMMENT '商品id',
    `product_id` bigint(20) DEFAULT NULL COMMENT '产品id',
    `product_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品编号',
    `product_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品名称',
    `product_specification` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品规格',
    `product_unit` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品单位',
    `product_barcode` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品条形码',
    `category_id` bigint(20) DEFAULT NULL COMMENT '产品分类id',
    `category_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品分类名称',
    `product_price` decimal(20,2) DEFAULT NULL COMMENT '产品单价',
    `product_cost_price` decimal(20,2) DEFAULT NULL COMMENT '进货单价',
    `tax_rate` decimal(20,2) DEFAULT NULL COMMENT '税率',
    `out_num` int(11) DEFAULT NULL COMMENT '出库数量',
    `return_num` int(11) DEFAULT NULL COMMENT '已退货数量',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='出库单商品明细';

-- ----------------------------
-- Table structure for es_procurement_plan
-- ----------------------------
DROP TABLE IF EXISTS `es_procurement_plan`;
CREATE TABLE `es_procurement_plan` (
  `id` bigint(20) NOT NULL COMMENT '主键ID',
  `sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '采购计划编号',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `dept_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '部门名称',
  `formation_person_id` bigint(20) DEFAULT NULL COMMENT '编制人员ID',
  `formation_person` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '编制人员',
  `formation_time` bigint(20) DEFAULT NULL COMMENT '编制时间',
  `procurement_desc` longtext COLLATE utf8_bin COMMENT '采购说明',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `supplier_id` bigint(20) DEFAULT NULL COMMENT '供应商ID',
  `supplier_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '供应商名称',
  `supplier_time` bigint(20) DEFAULT NULL COMMENT '计划供应时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='采购计划表(es_procurement_plan)';

-- ----------------------------
-- Table structure for es_procurement_plan_product
-- ----------------------------
DROP TABLE IF EXISTS `es_procurement_plan_product`;
CREATE TABLE `es_procurement_plan_product` (
  `id` bigint(20) NOT NULL COMMENT '主键ID',
  `plan_id` bigint(20) DEFAULT NULL COMMENT '采购计划ID',
  `goods_id` bigint(20) DEFAULT NULL COMMENT '商品ID',
  `product_id` bigint(20) DEFAULT NULL COMMENT '产品ID',
  `product_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品名称',
  `product_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品编号',
  `specification` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品规格',
  `category_id` bigint(20) DEFAULT NULL COMMENT '分类ID',
  `category_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '分类名称',
  `unit` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品单位',
  `barcode` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '条形码',
  `procurement_num` int(10) DEFAULT NULL COMMENT '计划采购数量',
  `supply_time` bigint(20) DEFAULT NULL COMMENT '计划供应时间',
  `remark` varchar(1000) COLLATE utf8_bin DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `index_plan` (`plan_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='采购计划产品表(es_procurement_plan_product)';

DROP TABLE IF EXISTS `erp_product_stock`;
CREATE TABLE `erp_product_stock` (
    `id` bigint(20) NOT NULL,
    `warehouse_id` bigint(20) DEFAULT NULL COMMENT '仓库id',
    `goods_id` bigint(20) DEFAULT NULL COMMENT '商品id',
    `product_id` bigint(20) DEFAULT NULL COMMENT '产品id',
    `num` int(11) DEFAULT NULL COMMENT '库存数量',
    PRIMARY KEY (`id`),
    UNIQUE KEY `unique` (`product_id`,`warehouse_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='商品库存';

-- ----------------------------
-- Table structure for es_procurement_contract
-- ----------------------------
DROP TABLE IF EXISTS `es_procurement_contract`;
CREATE TABLE `es_procurement_contract` (
  `id` bigint(20) NOT NULL COMMENT '主键ID',
  `sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '合同编号',
  `sign_time` bigint(20) DEFAULT NULL COMMENT '签订时间',
  `supplier_id` bigint(20) DEFAULT NULL COMMENT '供应商ID',
  `supplier_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '供应商名称',
  `creator_id` bigint(20) DEFAULT NULL COMMENT '制单人ID',
  `creator` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '制单人',
  `creator_time` bigint(20) DEFAULT NULL COMMENT '制单时间',
  `contract_annex` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '合同附件',
  `contract_desc` longtext COLLATE utf8_bin COMMENT '说明',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `contract_status` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '合同状态',
  `disabled` int(1) DEFAULT NULL COMMENT '是否删除 0：否，1：是',
  `plan_id` bigint(20) DEFAULT NULL COMMENT '采购计划ID',
  `plan_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '采购计划编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='采购合同表(es_procurement_contract)';

-- ----------------------------
-- Table structure for es_procurement_contract_product
-- ----------------------------
DROP TABLE IF EXISTS `es_procurement_contract_product`;
CREATE TABLE `es_procurement_contract_product` (
  `id` bigint(20) NOT NULL COMMENT '主键ID',
  `contract_id` bigint(20) DEFAULT NULL COMMENT '采购合同ID',
  `goods_id` bigint(20) DEFAULT NULL COMMENT '商品ID',
  `product_id` bigint(20) DEFAULT NULL COMMENT '产品ID',
  `product_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品名称',
  `product_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品编号',
  `specification` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品规格',
  `category_id` bigint(20) DEFAULT NULL COMMENT '分类ID',
  `category_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '分类名称',
  `unit` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品单位',
  `barcode` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '条形码',
  `price` decimal(20,2) DEFAULT NULL COMMENT '单价',
  `num` int(10) DEFAULT NULL COMMENT '数量',
  `tax_rate` decimal(20,2) DEFAULT NULL COMMENT '税率',
  `total_price` decimal(20,2) DEFAULT NULL COMMENT '合价',
  `stock_num` int(10) DEFAULT NULL COMMENT '入库数量',
  PRIMARY KEY (`id`),
  KEY `index_contract_id` (`contract_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='采购合同产品表(es_procurement_contract_product)';

ALTER TABLE `es_order`
    ADD COLUMN `warehouse_out_flag` BIT DEFAULT 0 NULL COMMENT '是否已出库',
    ADD COLUMN `warehouse_out_id` BIGINT NULL COMMENT '出库单id',
    ADD COLUMN `distribution_id` BIGINT NULL COMMENT '分销经理id',
    ADD COLUMN `distribution_name` VARCHAR (255) NULL COMMENT '分销经理姓名',
    ADD COLUMN `distribution_mobile` VARCHAR (255) NULL COMMENT '分销经理手机号',
    ADD COLUMN `marketing_id` BIGINT NULL COMMENT '营销经理id',
    ADD COLUMN `marketing_name` VARCHAR (255) NULL COMMENT '营销经理名称',
    ADD COLUMN `branch_id` BIGINT NULL COMMENT '自提门店id',
    ADD COLUMN `branch_name` VARCHAR (255) NULL COMMENT '自提门店名称',
    ADD COLUMN `technology_id` BIGINT NULL COMMENT '技术经理id',
    ADD COLUMN `technology_name` VARCHAR (255) NULL COMMENT '技术经理姓名',
    ADD COLUMN `real_name` VARCHAR (255) NULL COMMENT '会员真实姓名',
    ADD COLUMN `member_mobile` VARCHAR (255) NULL COMMENT '会员手机号',
    ADD COLUMN `member_identity` VARCHAR (255) NULL COMMENT '会员身份证号码',
    ADD COLUMN `after_sale_exist_flag` BIT NULL COMMENT '是否存在售后',
    ADD COLUMN `cost_price` DECIMAL (20, 2) NULL COMMENT '成本价';

DROP TABLE IF EXISTS `erp_marketing_manager`;
CREATE TABLE `erp_marketing_manager` (
    `id` bigint(20) NOT NULL COMMENT '主键',
    `member_id` bigint(20) NOT NULL COMMENT '会员id',
    `member_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '会员名称',
    `province_id` bigint(20) DEFAULT NULL COMMENT '所属省份ID',
    `city_id` bigint(20) DEFAULT NULL COMMENT '所属城市ID',
    `county_id` bigint(20) DEFAULT NULL COMMENT '所属县(区)ID',
    `town_id` bigint(20) DEFAULT NULL COMMENT '所属城镇ID',
    `province` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '所属省份名称',
    `city` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '所属城市名称',
    `county` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '所属县(区)名称',
    `town` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '所属城镇名称',
    `disable_flag` int(11) DEFAULT NULL COMMENT '0正常 1禁用',
    `region_path` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '地区path',
    `real_name` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '真实姓名',
    `mobile` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '电话',
    `remain_price` decimal(20,2) DEFAULT NULL COMMENT '提成金额',
    `freeze_price` decimal(20,2) DEFAULT NULL COMMENT '冻结金额',
    `already_price` decimal(20,2) DEFAULT NULL COMMENT '已提现金额',
    `dept_id` bigint(20) DEFAULT NULL COMMENT '部门id',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='营销经理';

-- ----------------------------
-- Table structure for es_lend_form
-- ----------------------------
DROP TABLE IF EXISTS `es_lend_form`;
CREATE TABLE `es_lend_form` (
  `id` bigint(20) NOT NULL COMMENT '主键ID',
  `sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '借出单编号',
  `status` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '借出单状态',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `dept_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '部门名称',
  `warehouse_id` bigint(20) DEFAULT NULL COMMENT '仓库ID',
  `warehouse_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '仓库名称',
  `lend_person_id` bigint(20) DEFAULT NULL COMMENT '借出人ID',
  `lend_person` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '借出人',
  `lend_register_id` bigint(20) DEFAULT NULL COMMENT '借出登记人ID',
  `lend_register` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '借出登记人',
  `lend_time` bigint(20) DEFAULT NULL COMMENT '借出时间',
  `lend_desc` varchar(1000) COLLATE utf8_bin DEFAULT NULL COMMENT '借出说明',
  `return_person_id` bigint(20) DEFAULT NULL COMMENT '归还人ID',
  `return_person` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '归还人',
  `return_register_id` bigint(20) DEFAULT NULL COMMENT '归还登记人ID',
  `return_register` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '归还登记人',
  `return_time` bigint(20) DEFAULT NULL COMMENT '归还时间',
  `return_desc` varchar(1000) COLLATE utf8_bin DEFAULT NULL COMMENT '归还说明',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `confirm_time` bigint(20) DEFAULT NULL COMMENT '确认时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='商品借出单表(es_lend_form)';

-- ----------------------------
-- Table structure for es_lend_form_product
-- ----------------------------
DROP TABLE IF EXISTS `es_lend_form_product`;
CREATE TABLE `es_lend_form_product` (
  `id` bigint(20) NOT NULL COMMENT '主键ID',
  `lend_id` bigint(20) DEFAULT NULL COMMENT '借出单ID',
  `stock_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '入库单号',
  `stock_batch_id` bigint(20) DEFAULT NULL COMMENT '入库批次ID',
  `stock_batch_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '入库批次单号',
  `goods_id` bigint(20) DEFAULT NULL COMMENT '商品ID',
  `product_id` bigint(20) DEFAULT NULL COMMENT '产品ID',
  `product_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品名称',
  `product_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品编号',
  `specification` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品规格',
  `category_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '分类名称',
  `unit` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品单位',
  `stock_num` int(10) DEFAULT NULL COMMENT '库存数量',
  `lend_num` int(10) DEFAULT NULL COMMENT '借出数量',
  `return_num` int(10) DEFAULT NULL COMMENT '归还数量',
  PRIMARY KEY (`id`),
  KEY `index_lend_id` (`lend_id`) USING BTREE,
  KEY `index_stock_sn` (`stock_sn`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='借出单产品表(es_lend_form_product)';

DROP TABLE IF EXISTS `erp_store`;
CREATE TABLE `erp_store` (
    `id` bigint(20) NOT NULL COMMENT '主键',
    `store_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '门店名称',
    `address` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '门店地址',
    `contact_person` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '联系人',
    `telephone` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '门店联系电话',
    `delete_flag` tinyint(4) DEFAULT NULL COMMENT '删除标志 0未删除 1已删除',
    `create_time` bigint(20) DEFAULT NULL COMMENT '门店创建时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='门店';

-- ----------------------------
-- Table structure for es_change_form
-- ----------------------------
DROP TABLE IF EXISTS `es_change_form`;
CREATE TABLE `es_change_form` (
  `id` bigint(20) NOT NULL COMMENT '主键ID',
  `sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '编号',
  `order_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '订单编号',
  `staff_id` bigint(20) DEFAULT NULL COMMENT '服务专员ID',
  `staff_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '服务专员名称',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `dept_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '部门名称',
  `warehouse_id` bigint(20) DEFAULT NULL COMMENT '仓库ID',
  `warehouse_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '仓库名称',
  `change_amount` decimal(20,2) DEFAULT NULL COMMENT '换货金额',
  `handled_by_id` bigint(20) DEFAULT NULL COMMENT '经手人ID',
  `handled_by` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '经手人',
  `change_desc` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '换货说明',
  `change_time` bigint(20) DEFAULT NULL COMMENT '换货时间',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `status` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '状态',
  `reject_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '审核驳回原因',
  `submit_time` bigint(20) DEFAULT NULL COMMENT '提交审核时间',
  `audit_time` bigint(20) DEFAULT NULL COMMENT '审核时间',
  `audit_person_id` bigint(20) DEFAULT NULL COMMENT '审核人ID',
  `audit_person_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '审核人名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='换货单(es_change_form)';

-- ----------------------------
-- Table structure for es_change_form_product
-- ----------------------------
DROP TABLE IF EXISTS `es_change_form_product`;
CREATE TABLE `es_change_form_product` (
  `id` bigint(20) NOT NULL COMMENT '主键ID',
  `change_id` bigint(20) DEFAULT NULL COMMENT '换货单ID',
  `stock_id` bigint(20) DEFAULT NULL COMMENT '入库单ID',
  `stock_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '入库单号',
  `stock_item_id` bigint(20) DEFAULT NULL COMMENT '入库单明细id',
  `stock_batch_id` bigint(20) DEFAULT NULL COMMENT '入库批次ID',
  `stock_batch_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '入库批次单号',
  `goods_id` bigint(20) DEFAULT NULL COMMENT '商品ID',
  `product_id` bigint(20) DEFAULT NULL COMMENT '产品ID',
  `product_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品名称',
  `product_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品编号',
  `specification` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品规格',
  `category_id` bigint(20) DEFAULT NULL COMMENT '分类ID',
  `category_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '分类名称',
  `unit` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品单位',
  `num` int(10) DEFAULT NULL COMMENT '数量',
  `amount` decimal(20,2) DEFAULT NULL COMMENT '金额',
  `cost_price` decimal(20,2) DEFAULT NULL COMMENT '成本价',
  `type` int(1) DEFAULT NULL COMMENT '产品类型 0：退货，1：换货',
  PRIMARY KEY (`id`),
  KEY `index_stock_sn` (`stock_sn`) USING BTREE,
  KEY `index_type` (`type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='换货单产品表(es_change_form_product)';

DROP TABLE IF EXISTS `erp_stock_transfer`;
CREATE TABLE `erp_stock_transfer` (
    `id` bigint(20) NOT NULL COMMENT '主键',
    `sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '编号',
    `out_dept_id` bigint(20) DEFAULT NULL COMMENT '调出方部门id',
    `out_warehouse_id` bigint(20) DEFAULT NULL COMMENT '调出方仓库id',
    `out_handled_by_id` bigint(20) DEFAULT NULL COMMENT '调出方仓库经手人id',
    `out_handled_by_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '调出方仓库经手人名称',
    `in_dept_id` bigint(20) DEFAULT NULL COMMENT '调入方部门id',
    `in_warehouse_id` bigint(20) DEFAULT NULL COMMENT '调入方仓库id',
    `in_handled_by_id` bigint(20) DEFAULT NULL COMMENT '调入方仓库经手人id',
    `in_handled_by_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '调入方仓库经手人名称',
    `out_dept_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '调出部门名称',
    `out_warehouse_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '调出仓库名称',
    `in_dept_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '调入部门名称',
    `in_warehouse_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '调入仓库名称',
    `attachment` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '附件',
    `status` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '状态',
    `transfer_time` bigint(20) DEFAULT NULL COMMENT '调拨时间',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    `creator` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
    `updater` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='库存调拨';

DROP TABLE IF EXISTS `erp_stock_transfer_product`;
CREATE TABLE `erp_stock_transfer_product` (
    `id` bigint(20) NOT NULL COMMENT '主键',
    `stock_transfer_id` bigint(20) DEFAULT NULL COMMENT '调拨单id',
    `warehouse_entry_id` bigint(20) DEFAULT NULL COMMENT '入库单id',
    `warehouse_entry_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '入库单号',
    `warehouse_entry_item_id` bigint(20) DEFAULT NULL COMMENT '入库单明细id',
    `warehouse_entry_batch_id` bigint(20) DEFAULT NULL COMMENT '入库批次id',
    `warehouse_entry_batch_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '入库批次号',
    `goods_id` bigint(20) DEFAULT NULL COMMENT '商品id',
    `product_id` bigint(20) DEFAULT NULL COMMENT '产品id',
    `product_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品编号',
    `product_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品名称',
    `product_specification` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品规格',
    `product_unit` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品单位',
    `product_barcode` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品条形码',
    `category_id` bigint(20) DEFAULT NULL COMMENT '产品分类id',
    `category_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品分类名称',
    `product_price` decimal(20,2) DEFAULT NULL COMMENT '入库单价',
    `product_cost_price` decimal(20,2) DEFAULT NULL COMMENT '进货单价',
    `tax_rate` decimal(20,2) DEFAULT NULL COMMENT '税率',
    `num` int(11) DEFAULT NULL COMMENT '调拨数量',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='调拨单商品明细';

-- ----------------------------
-- Table structure for es_stock_damage_report
-- ----------------------------
DROP TABLE IF EXISTS `es_stock_damage_report`;
CREATE TABLE `es_stock_damage_report` (
  `id` bigint(20) NOT NULL COMMENT '主键ID',
  `sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '编号',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `dept_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '部门名称',
  `warehouse_id` bigint(20) DEFAULT NULL COMMENT '仓库ID',
  `warehouse_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '仓库名称',
  `creator_id` bigint(20) DEFAULT NULL COMMENT '制单人ID',
  `creator` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '制单人',
  `report_time` bigint(20) DEFAULT NULL COMMENT '报损时间',
  `damage_type_key` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '报损类型(键)',
  `damage_type_value` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '报损类型(值)',
  `report_desc` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '报损说明',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `status` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '状态',
  `reject_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '审核驳回原因',
  `audit_time` bigint(20) DEFAULT NULL COMMENT '审核时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='库存报损单(es_stock_damage_report)';

-- ----------------------------
-- Table structure for es_stock_damage_report_product
-- ----------------------------
DROP TABLE IF EXISTS `es_stock_damage_report_product`;
CREATE TABLE `es_stock_damage_report_product` (
  `id` bigint(20) NOT NULL COMMENT '主键ID',
  `report_id` bigint(20) DEFAULT NULL COMMENT '报损单ID',
  `stock_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '入库单号',
  `batch_id` bigint(20) DEFAULT NULL COMMENT '批次ID',
  `batch_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '批次编号',
  `goods_id` bigint(20) DEFAULT NULL COMMENT '商品ID',
  `product_id` bigint(20) DEFAULT NULL COMMENT '产品ID',
  `product_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品名称',
  `product_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品编号',
  `specification` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品规格',
  `category_id` bigint(20) DEFAULT NULL COMMENT '分类ID',
  `category_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '分类名称',
  `unit` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品单位',
  `price` decimal(20,2) DEFAULT NULL COMMENT '单价',
  `cost_price` decimal(20,2) DEFAULT NULL COMMENT '成本价',
  `tax_rate` decimal(20,2) DEFAULT NULL COMMENT '税率',
  `stock_num` int(10) DEFAULT NULL COMMENT '库存数量',
  `report_num` int(10) DEFAULT NULL COMMENT '报损数量',
  `report_remark` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注',
  `type` int(10) DEFAULT NULL COMMENT '类型 0：增加，1：减少',
  PRIMARY KEY (`id`),
  KEY `index_report` (`report_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='库存报损产品表(es_stock_damage_report_product)';

DROP TABLE IF EXISTS `erp_supplier_return`;
CREATE TABLE `erp_supplier_return` (
    `id` bigint(20) NOT NULL COMMENT '主键',
    `sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '编号',
    `dept_id` bigint(20) DEFAULT NULL COMMENT '部门id',
    `warehouse_entry_id` bigint(20) DEFAULT NULL COMMENT '入库单id',
    `warehouse_entry_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '入库单编号',
    `warehouse_id` bigint(20) DEFAULT NULL COMMENT '仓库id',
    `supplier_id` bigint(20) DEFAULT NULL COMMENT '供应商id',
    `supplier_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '供应商编号',
    `dept_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '部门名称',
    `warehouse_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '仓库名称',
    `supplier_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '供应商名称',
    `contract_id` bigint(20) DEFAULT NULL COMMENT '合同id',
    `contract_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '合同编号',
    `handle_by_id` bigint(20) DEFAULT NULL COMMENT '经手人id',
    `handle_by_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '经手人名称',
    `attachment` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '附件',
    `return_time` bigint(20) DEFAULT NULL COMMENT '退货时间',
    `status` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '状态',
    `audit_by` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '审核人',
    `audit_remark` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '审核备注',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    `creator` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
    `updater` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='供应商退货';

DROP TABLE IF EXISTS `erp_supplier_return_item`;
CREATE TABLE `erp_supplier_return_item` (
    `id` bigint(20) NOT NULL COMMENT '主键',
    `supplier_return_id` bigint(20) DEFAULT NULL COMMENT '供应商退货id',
    `warehouse_entry_item_id` bigint(20) DEFAULT NULL COMMENT '入库单明细id',
    `warehouse_entry_batch_id` bigint(20) DEFAULT NULL COMMENT '批次id',
    `warehouse_entry_batch_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '批次编号',
    `goods_id` bigint(20) DEFAULT NULL COMMENT '商品id',
    `product_id` bigint(20) DEFAULT NULL COMMENT '产品id',
    `product_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品编号',
    `product_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品名称',
    `product_specification` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品规格',
    `product_unit` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品单位',
    `product_barcode` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品条形码',
    `category_id` bigint(20) DEFAULT NULL COMMENT '产品分类id',
    `category_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品分类名称',
    `product_price` decimal(20,2) DEFAULT NULL COMMENT '产品单价',
    `product_cost_price` decimal(20,2) DEFAULT NULL COMMENT '进货单价',
    `tax_rate` decimal(20,2) DEFAULT NULL COMMENT '税率',
    `entry_num` int(11) DEFAULT NULL COMMENT '入库数量',
    `return_num` int(11) DEFAULT NULL COMMENT '退货数量',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='供应商退货项';


-- ----------------------------
-- Table structure for es_stock_inventory
-- ----------------------------
DROP TABLE IF EXISTS `es_stock_inventory`;
CREATE TABLE `es_stock_inventory` (
  `id` bigint(20) NOT NULL COMMENT '主键ID',
  `sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '库存盘点单编号',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `dept_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '部门名称',
  `warehouse_id` bigint(20) DEFAULT NULL COMMENT '仓库ID',
  `warehouse_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '仓库名称',
  `inventory_person_id` bigint(20) DEFAULT NULL COMMENT '盘点人ID',
  `inventory_person` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '盘点人',
  `creator_id` bigint(20) DEFAULT NULL COMMENT '制单人ID',
  `creator` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '制单人',
  `inventory_time` bigint(20) DEFAULT NULL COMMENT '盘点时间',
  `inventory_desc` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '盘点说明',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `status` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '状态',
  `reject_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '审核驳回原因',
  `audit_time` bigint(20) DEFAULT NULL COMMENT '审核时间',
  `audit_person_id` bigint(20) DEFAULT NULL COMMENT '审核人ID',
  `audit_person_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '审核人名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='库存盘点单表(es_stock_inventory)';

-- ----------------------------
-- Table structure for es_stock_inventory_product
-- ----------------------------
DROP TABLE IF EXISTS `es_stock_inventory_product`;
CREATE TABLE `es_stock_inventory_product` (
  `id` bigint(20) NOT NULL COMMENT '主键ID',
  `inventory_id` bigint(20) DEFAULT NULL COMMENT '库存盘点单ID',
  `goods_id` bigint(20) DEFAULT NULL COMMENT '商品ID',
  `product_id` bigint(20) DEFAULT NULL COMMENT '产品ID',
  `product_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品名称',
  `product_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品编号',
  `specification` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品规格',
  `category_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '分类名称',
  `unit` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品单位',
  `stock_num` int(10) DEFAULT NULL COMMENT '库存数量',
  `inventory_num` int(10) DEFAULT NULL COMMENT '盘点数量',
  `diff_num` int(10) DEFAULT NULL COMMENT '差异数量',
  `remark` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `index_inventory_id` (`inventory_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='库存盘点单产品表(es_stock_inventory_product)';

DROP TABLE IF EXISTS `erp_order_return`;
CREATE TABLE `erp_order_return` (
    `id` bigint(20) NOT NULL COMMENT '主键',
    `sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '退货单编号',
    `order_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '要退货的销售订单',
    `dept_id` bigint(20) DEFAULT NULL COMMENT '部门id',
    `warehouse_id` bigint(20) DEFAULT NULL COMMENT '仓库id',
    `dept_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '部门名称',
    `warehouse_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '仓库名称',
    `distribution_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '销售经理',
    `handle_by_id` bigint(20) DEFAULT NULL COMMENT '经手人id',
    `handle_by_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '经手人名称',
    `status` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '状态',
    `return_time` bigint(20) DEFAULT NULL COMMENT '退货时间',
    `audit_by_id` bigint(20) DEFAULT NULL COMMENT '审核人id',
    `audit_by` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '审核人',
    `audit_remark` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '审核备注',
    `return_remark` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '退货说明',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    `creator` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
    `updater` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='订单退货';

DROP TABLE IF EXISTS `erp_order_return_item`;
CREATE TABLE `erp_order_return_item` (
    `id` bigint(20) NOT NULL COMMENT '主键',
    `order_return_id` bigint(20) DEFAULT NULL COMMENT '订单退货id',
    `warehouse_entry_id` bigint(20) DEFAULT NULL COMMENT '入库单id',
    `warehouse_entry_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '入库单号',
    `warehouse_entry_item_id` bigint(20) DEFAULT NULL COMMENT '入库单明细id',
    `warehouse_entry_batch_id` bigint(20) DEFAULT NULL COMMENT '入库批次id',
    `warehouse_entry_batch_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '入库批次号',
    `warehouse_out_item_id` bigint(20) DEFAULT NULL COMMENT '对应的出库项id',
    `goods_id` bigint(20) DEFAULT NULL COMMENT '商品id',
    `product_id` bigint(20) DEFAULT NULL COMMENT '产品id',
    `product_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品编号',
    `product_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品名称',
    `product_specification` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品规格',
    `product_unit` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品单位',
    `product_barcode` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品条形码',
    `category_id` bigint(20) DEFAULT NULL COMMENT '产品分类id',
    `category_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品分类名称',
    `product_price` decimal(20,2) DEFAULT NULL COMMENT '产品单价',
    `product_cost_price` decimal(20,2) DEFAULT NULL COMMENT '进货单价',
    `tax_rate` decimal(20,2) DEFAULT NULL COMMENT '税率',
    `order_num` int(11) DEFAULT NULL COMMENT '订单的购买数量',
    `return_num` int(11) DEFAULT NULL COMMENT '退货数量',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='订单退货项';

DROP TABLE IF EXISTS `erp_supplier_settlement`;
CREATE TABLE `erp_supplier_settlement` (
    `id` bigint(20) NOT NULL,
    `sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '结算单编号',
    `supplier_id` bigint(20) DEFAULT NULL COMMENT '供应商id',
    `supplier_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '供应商名称',
    `start_time` bigint(20) DEFAULT NULL COMMENT '结算开始时间',
    `end_time` bigint(20) DEFAULT NULL COMMENT '结算结束时间',
    `total_price` decimal(20,2) DEFAULT NULL COMMENT '合计金额',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    `creator` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
    `updater` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='供应商结算单';

DROP TABLE IF EXISTS `erp_supplier_settlement_item`;
CREATE TABLE `erp_supplier_settlement_item` (
    `id` bigint(20) NOT NULL COMMENT '主键',
    `supplier_settlement_id` bigint(20) DEFAULT NULL COMMENT '供应商结算单id',
    `warehouse_entry_id` bigint(20) DEFAULT NULL COMMENT '入库单id',
    `warehouse_entry_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '入库单编号',
    `warehouse_entry_item_id` bigint(20) DEFAULT NULL COMMENT '入库单明细id',
    `goods_id` bigint(20) DEFAULT NULL COMMENT '商品id',
    `product_id` bigint(20) DEFAULT NULL COMMENT '产品id',
    `product_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品编号',
    `product_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品名称',
    `product_specification` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品规格',
    `product_unit` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品单位',
    `product_barcode` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品条形码',
    `category_id` bigint(20) DEFAULT NULL COMMENT '产品分类id',
    `category_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品分类名称',
    `product_price` decimal(20,2) DEFAULT NULL COMMENT '产品单价',
    `product_cost_price` decimal(20,2) DEFAULT NULL COMMENT '进货单价',
    `tax_rate` decimal(20,2) DEFAULT NULL COMMENT '税率',
    `product_num` int(11) DEFAULT NULL COMMENT '结算数量',
    `total_price` decimal(20,2) DEFAULT NULL COMMENT '总价格',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='供应商结算单明细';

DROP TABLE IF EXISTS `erp_message_push`;
CREATE TABLE `erp_message_push` (
    `id` bigint(20) NOT NULL COMMENT '主键',
    `target_system` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '推送的目标系统',
    `business_type` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '消息业务类型',
    `summary` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '消息摘要（前端展示和快速定位数据用）',
    `content` longtext COLLATE utf8_bin COMMENT '消息内容',
    `produce_time` bigint(20) DEFAULT NULL COMMENT '消息生产时间',
    `status` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '消息状态',
    `fail_count` int(11) DEFAULT NULL COMMENT '推送失败次数',
    `remark` longtext COLLATE utf8_bin COMMENT '推送备注',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='消息推送';

DROP TABLE IF EXISTS `erp_message_receive`;
CREATE TABLE `erp_message_receive` (
    `id` bigint(20) NOT NULL COMMENT '主键',
    `type` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '消息类型',
    `msg_id` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '外部系统的消息id',
    `content` longtext COLLATE utf8_bin COMMENT '消息内容',
    `produce_time` bigint(20) DEFAULT NULL COMMENT '外部系统生成消息的时间',
    `receive_time` bigint(20) DEFAULT NULL COMMENT '接收消息的时间',
    `status` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '状态',
    `remark` longtext COLLATE utf8_bin COMMENT '处理备注',
    PRIMARY KEY (`id`),
    UNIQUE KEY `msg_id` (`msg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='消息接收';

-- ----------------------------
-- Table structure for es_stock_statistics
-- ----------------------------
DROP TABLE IF EXISTS `es_stock_statistics`;
CREATE TABLE `es_stock_statistics` (
  `id` bigint(20) NOT NULL COMMENT '主键ID',
  `warehouse_id` bigint(20) DEFAULT NULL COMMENT '仓库ID',
  `warehouse_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '仓库名称',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `dept_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '部门名称',
  `stock_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '入库单号',
  `batch_id` bigint(20) DEFAULT NULL COMMENT '批次ID',
  `batch_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '批次编号',
  `product_id` bigint(20) DEFAULT NULL COMMENT '产品ID',
  `product_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品名称',
  `product_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品编号',
  `specification` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品规格',
  `category_id` bigint(20) DEFAULT NULL COMMENT '分类ID',
  `category_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '分类名称',
  `unit` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品单位',
  `in_num` int(10) DEFAULT NULL COMMENT '入库数量',
  `out_num` int(10) DEFAULT NULL COMMENT '出库数量',
  `supplier_return_num` int(10) DEFAULT NULL COMMENT '供应商退货数量',
  `order_return_num` int(10) DEFAULT NULL COMMENT '订单退货数量',
  `transfer_out_num` int(10) DEFAULT NULL COMMENT '调出数量',
  `transfer_in_num` int(10) DEFAULT NULL COMMENT '调入数量',
  `damage_num` int(10) DEFAULT NULL COMMENT '库存调整(报损)数量',
  `stock_num` int(10) DEFAULT NULL COMMENT '库存数量',
  `warning_num` int(10) DEFAULT NULL COMMENT '预警数量',
  `remain_num` int(10) DEFAULT NULL COMMENT '剩余库存数量',
  `change_num` int(10) DEFAULT NULL COMMENT '换货数量',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `index_stock_sn` (`stock_sn`) USING BTREE,
  KEY `index_warehouse_id` (`warehouse_id`) USING BTREE,
  KEY `index_dept_id` (`dept_id`) USING BTREE,
  KEY `index_product_id` (`product_id`) USING BTREE,
  KEY `index_product_name` (`product_name`) USING BTREE,
  KEY `index_category_id` (`category_id`) USING BTREE,
  KEY `index_batch_id` (`batch_id`) USING BTREE,
  KEY `index_batch_sn` (`batch_sn`) USING BTREE,
  KEY `index_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='库存统计表(es_stock_statistics)';

DROP TABLE IF EXISTS `erp_goods`;
CREATE TABLE `erp_goods` (
    `id` bigint(20) NOT NULL,
    `sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '商品编号',
    `name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '商品名称',
    `category_id` bigint(20) DEFAULT NULL COMMENT '商品分类id',
    `brand_id` bigint(20) DEFAULT NULL COMMENT '品牌id',
    `market_enable` bit(1) DEFAULT b'1' COMMENT '是否上架',
    `intro` longtext COLLATE utf8_bin COMMENT '详情',
    `have_spec` bit(1) DEFAULT b'0' COMMENT '是否有规格',
    `page_title` varchar(1000) COLLATE utf8_bin DEFAULT NULL COMMENT 'seo标题',
    `meta_keywords` varchar(1000) COLLATE utf8_bin DEFAULT NULL COMMENT 'seo关键字',
    `meta_description` varchar(1000) COLLATE utf8_bin DEFAULT NULL COMMENT 'seo描述',
    `thumbnail` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '缩略图路径',
    `big` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '大图路径',
    `small` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '小图路径',
    `original` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '原图路径',
    `freight_type` tinyint(4) DEFAULT '1' COMMENT '谁承担运费 0：买家承担，1：卖家承担',
    `template_id` bigint(20) DEFAULT NULL COMMENT '运费模板id',
    `mobile_intro` longtext COLLATE utf8_bin COMMENT '商品移动端详情',
    `goods_video` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '商品视频',
    `price` decimal(20,2) DEFAULT NULL COMMENT '销售价格',
    `cost_price` decimal(20,2) DEFAULT NULL COMMENT '成本价格',
    `mkt_price` decimal(20,2) DEFAULT NULL COMMENT '市场价格',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    `creator` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
    `updater` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='商品';

DROP TABLE IF EXISTS `erp_stock_batch_flow`;
CREATE TABLE `erp_stock_batch_flow` (
    `id` bigint(20) NOT NULL,
    `warehouse_entry_id` bigint(20) DEFAULT NULL COMMENT '入库单id',
    `warehouse_entry_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '入库单编号',
    `batch_id` bigint(20) DEFAULT NULL COMMENT '批次id',
    `batch_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '批次编号',
    `dept_id` bigint(20) DEFAULT NULL COMMENT '部门id',
    `warehouse_id` bigint(20) DEFAULT NULL COMMENT '仓库id',
    `goods_id` bigint(20) DEFAULT NULL COMMENT '商品id',
    `product_id` bigint(20) DEFAULT NULL COMMENT '产品id',
    `product_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品编号',
    `product_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品名称',
    `product_specification` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品规格',
    `product_unit` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品单位',
    `product_barcode` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品条形码',
    `category_id` bigint(20) DEFAULT NULL COMMENT '产品分类id',
    `category_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '产品分类名称',
    `product_price` decimal(20,2) DEFAULT NULL COMMENT '产品单价',
    `product_cost_price` decimal(20,2) DEFAULT NULL COMMENT '产品进货单价',
    `tax_rate` decimal(20,2) DEFAULT NULL COMMENT '税率',
    `change_type` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '增加/减少',
    `change_num` int(11) DEFAULT NULL COMMENT '库存变更数量',
    `batch_remain_num` int(11) DEFAULT NULL COMMENT '批次剩余库存数量',
    `source_type` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '库存变更来源',
    `source_sn` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '库存变更来源单号',
    `create_time` bigint(20) DEFAULT NULL COMMENT '库存变更时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='库存批次流水';

DROP TABLE IF EXISTS `erp_member`;
CREATE TABLE `erp_member` (
    `id` bigint(20) NOT NULL,
    `sn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '编号',
    `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '姓名',
    `sex` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '性别',
    `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '手机号',
    `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '邮箱',
    `birthday` bigint(20) DEFAULT NULL COMMENT '出生日期',
    `remark` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
    `disable_flag` bit(1) DEFAULT NULL COMMENT '是否禁用',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    `creator` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '创建者',
    `updater` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '更新者',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='会员';

DROP TABLE IF EXISTS `erp_task`;
CREATE TABLE `erp_task` (
    `id` bigint(20) NOT NULL,
    `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '名称',
    `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '类型',
    `sub_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '子类型',
    `params` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '执行参数',
    `status` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '状态',
    `result` longtext COLLATE utf8_unicode_ci COMMENT '执行结果',
    `thread_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '线程id',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    `creator` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '创建者',
    `updater` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '更新者',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='任务';

DROP TABLE IF EXISTS `erp_collecting_account`;
CREATE TABLE `erp_collecting_account` (
    `id` bigint(20) NOT NULL,
    `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '账户名称',
    `sn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '编号',
    `default_flag` bit(1) DEFAULT NULL COMMENT '是否默认',
    `enable_flag` bit(1) DEFAULT NULL COMMENT '是否启用',
    `remark` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    `creator` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '创建者',
    `updater` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '更新者',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='收款账户';

DROP TABLE IF EXISTS `erp_order`;
CREATE TABLE `erp_order` (
    `id` bigint(20) NOT NULL,
    `sn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '订单编号',
    `status` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '订单状态',
    `member_id` bigint(20) DEFAULT NULL COMMENT '会员id',
    `member_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '会员名称',
    `member_mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '会员手机号',
    `order_time` bigint(20) DEFAULT NULL COMMENT '下单时间',
    `warehouse_id` bigint(20) DEFAULT NULL COMMENT '仓库id',
    `warehouse_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '仓库名称',
    `dept_id` bigint(20) DEFAULT NULL COMMENT '部门id',
    `dept_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '部门名称',
    `marketing_id` bigint(20) DEFAULT NULL COMMENT '销售经理id',
    `marketing_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '销售经理名称',
    `total_price` decimal(20,2) DEFAULT NULL COMMENT '原价',
    `discount_price` decimal(20,2) DEFAULT NULL COMMENT '优惠金额',
    `tax_price` decimal(20,2) DEFAULT NULL COMMENT '税额',
    `pay_price` decimal(20,2) DEFAULT NULL COMMENT '应付金额',
    `deposit_price` decimal(20,2) DEFAULT NULL COMMENT '定金金额',
    `attachment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '附件',
    `remark` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
    `delivery_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '配送方式',
    `store_id` bigint(20) DEFAULT NULL COMMENT '自提门店id',
    `store_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '自提门店名称',
    `warehouse_out_flag` bit(1) DEFAULT NULL COMMENT '是否已出库',
    `warehouse_out_id` bigint(20) DEFAULT NULL COMMENT '出库单id',
    `ship_flag` bit(1) DEFAULT NULL COMMENT '是否已发货',
    `ship_time` bigint(20) DEFAULT NULL COMMENT '发货时间',
    `logistics_company_id` bigint(20) DEFAULT NULL COMMENT '物流公司id',
    `logistics_company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '物流公司名称',
    `logistics_tracking_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '物流单号',
    `deleted` bit(1) DEFAULT b'0' COMMENT '是否删除',
    `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
    `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
    `creator` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '创建者',
    `updater` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '更新者',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='订单';

DROP TABLE IF EXISTS `erp_order_item`;
CREATE TABLE `erp_order_item` (
    `id` bigint(20) NOT NULL,
    `order_id` bigint(20) DEFAULT NULL COMMENT '订单id',
    `batch_id` bigint(20) DEFAULT NULL COMMENT '批次id',
    `batch_sn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '批次编号',
    `warehouse_entry_id` bigint(20) DEFAULT NULL COMMENT '入库单id',
    `warehouse_entry_sn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '入库单编号',
    `warehouse_entry_item_id` bigint(20) DEFAULT NULL COMMENT '入库单明细id',
    `goods_id` bigint(20) DEFAULT NULL COMMENT '商品id',
    `product_id` bigint(20) DEFAULT NULL COMMENT '产品id',
    `product_sn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '产品编号',
    `product_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '产品名称',
    `product_specification` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '产品规格',
    `product_unit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '产品单位',
    `category_id` bigint(20) DEFAULT NULL COMMENT '分类id',
    `category_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '分类名称',
    `tax_rate` decimal(20,2) DEFAULT NULL COMMENT '税率',
    `num` int(11) DEFAULT NULL COMMENT '数量',
    `cost_price` decimal(20,2) DEFAULT NULL COMMENT '成本单价',
    `total_cost_price` decimal(20,2) DEFAULT NULL COMMENT '成本总价',
    `price` decimal(20,2) DEFAULT NULL COMMENT '原价（单）',
    `total_price` decimal(20,2) DEFAULT NULL COMMENT '原价（总）',
    `tax_price` decimal(20,2) DEFAULT NULL COMMENT '税额（总）',
    `discount_price` decimal(20,2) DEFAULT NULL COMMENT '优惠金额（总）',
    `pay_price` decimal(20,2) DEFAULT NULL COMMENT '应付金额（总）',
    `remark` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='订单明细';