package com.hys.app.converter.datasync;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import com.hys.app.model.datasync.dos.MessagePushDO;
import com.hys.app.model.datasync.vo.MessagePushVO;
import com.hys.app.framework.database.WebPage;
import java.util.List;

/**
 * 消息推送 Convert
 *
 * @author zs
 * @since 2023-12-18 16:24:10
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface MessagePushConverter {

    MessagePushVO convert(MessagePushDO messagePushDO);
    
    List<MessagePushVO> convert(List<MessagePushDO> list);
    
    WebPage<MessagePushVO> convert(WebPage<MessagePushDO> webPage);
    
}

