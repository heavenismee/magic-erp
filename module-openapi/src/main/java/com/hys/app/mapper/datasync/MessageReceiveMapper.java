package com.hys.app.mapper.datasync;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.datasync.dos.MessageReceiveDO;
import com.hys.app.model.datasync.dto.MessageReceiveQueryParams;
import com.hys.app.model.datasync.enums.MessageReceiveStatusEnum;

/**
 * 消息接收的Mapper
 *
 * @author zs
 * @since 2023-12-19 17:41:37
 */
public interface MessageReceiveMapper extends BaseMapperX<MessageReceiveDO> {

    default WebPage<MessageReceiveDO> selectPage(MessageReceiveQueryParams params) {
        return lambdaQuery()
                .eqIfPresent(MessageReceiveDO::getType, params.getType())
                .eqIfPresent(MessageReceiveDO::getMsgId, params.getMsgId())
                .likeIfPresent(MessageReceiveDO::getContent, params.getContent())
                .betweenIfPresent(MessageReceiveDO::getProduceTime, params.getProduceTime())
                .betweenIfPresent(MessageReceiveDO::getReceiveTime, params.getReceiveTime())
                .eqIfPresent(MessageReceiveDO::getStatus, params.getStatus())
                .likeIfPresent(MessageReceiveDO::getRemark, params.getRemark())
                .orderByDesc(MessageReceiveDO::getId)
                .page(params);
    }

    default void updateStatus(Long id, MessageReceiveStatusEnum status, String remark) {
        lambdaUpdate()
                .set(MessageReceiveDO::getStatus, status)
                .set(MessageReceiveDO::getRemark, remark)
                .eq(MessageReceiveDO::getId, id)
                .update();
    }
}

