package com.hys.app.model.datasync.dto;

import com.hys.app.model.datasync.enums.MessagePushStatusEnum;
import com.hys.app.model.datasync.enums.TargetSystemEnum;
import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 消息推送查询参数
 *
 * @author zs
 * @since 2023-12-18 16:24:10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class MessagePushQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "target_system", value = "推送的目标系统")
    private TargetSystemEnum targetSystem;

    @ApiModelProperty(name = "business_type", value = "消息业务类型")
    private String businessType;

    @ApiModelProperty(name = "summary", value = "消息摘要（前端展示和快速定位数据用）")
    private String summary;

    @ApiModelProperty(name = "content", value = "消息内容")
    private String content;

    @ApiModelProperty(name = "produce_time", value = "消息生产时间")
    private Long[] produceTime;

    @ApiModelProperty(name = "status", value = "消息状态")
    private MessagePushStatusEnum status;

    @ApiModelProperty(name = "remark", value = "推送结果备注")
    private String remark;

}

