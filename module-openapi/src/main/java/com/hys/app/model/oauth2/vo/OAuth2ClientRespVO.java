package com.hys.app.model.oauth2.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 管理后台 - OAuth2 客户端 Response VO
 *
 * @author zs
 * @since 2024-02-20
 */
@ApiModel(value = "管理后台 - OAuth2 客户端 Response VO")
@Data
public class OAuth2ClientRespVO {

    @ApiModelProperty(value = "编号")
    private Long id;

    @ApiModelProperty(value = "客户端编号")
    private String clientId;

    @ApiModelProperty(value = "客户端密钥")
    private String secret;

    @ApiModelProperty(value = "应用名")
    private String name;

    @ApiModelProperty(value = "应用图标")
    private String logo;

    @ApiModelProperty(value = "应用描述")
    private String value;

    @ApiModelProperty(value = "状态，参见 CommonStatusEnum 枚举")
    private Integer status;

    @ApiModelProperty(value = "访问令牌的有效期")
    private Integer accessTokenValiditySeconds;

    @ApiModelProperty(value = "刷新令牌的有效期")
    private Integer refreshTokenValiditySeconds;

    @ApiModelProperty(value = "授权类型，参见 OAuth2GrantTypeEnum 枚举")
    private List<String> authorizedGrantTypes;

    @ApiModelProperty(value = "可重定向的 URI 地址")
    private List<String> redirectUris;

    @ApiModelProperty(value = "授权范围")
    private List<String> scopes;

    @ApiModelProperty(value = "自动通过的授权范围")
    private List<String> autoApproveScopes;

    @ApiModelProperty(value = "创建时间")
    private Long createTime;

    @ApiModelProperty(value = "应用描述")
    private String description;

}
