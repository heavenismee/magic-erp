package com.hys.app.model.oauth2.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * OAuth2 访问令牌 DO
 *
 * @author zs
 * @since 2024-02-18
 */
@TableName(value = "system_oauth2_access_token", autoResultMap = true)
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(chain = true)
public class OAuth2AccessTokenDO extends BaseDO {

    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;
    /**
     * 访问令牌
     */
    @ApiModelProperty(name = "sn", value = "访问令牌")
    private String accessToken;
    /**
     * 刷新令牌
     */
    @ApiModelProperty(name = "sn", value = "刷新令牌")
    private String refreshToken;
    /**
     * 用户编号
     */
    @ApiModelProperty(name = "sn", value = "编号")
    private Long userId;
    /**
     * 用户类型
     */
    @ApiModelProperty(name = "user_type", value = "用户类型")
    private Integer userType;
    /**
     * 客户端编号
     * <p>
     * 关联 {@link OAuth2ClientDO#getId()}
     */
    @ApiModelProperty(name = "sn", value = "客户端编号")
    private String clientId;
    /**
     * 授权范围
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    @ApiModelProperty(name = "sn", value = "授权范围")
    private List<String> scopes;
    /**
     * 过期时间
     */
    private Long expiresTime;

}
