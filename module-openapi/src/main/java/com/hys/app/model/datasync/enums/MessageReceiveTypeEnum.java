package com.hys.app.model.datasync.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 消息接收类型
 *
 * @author zs
 * @since 2023-12-19
 */
@Getter
@AllArgsConstructor
public enum MessageReceiveTypeEnum {

}
