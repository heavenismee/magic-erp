package com.hys.app.model.datasync.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 操作类型
 *
 * @author zs
 * @since 2023-12-27
 */
@Getter
@AllArgsConstructor
public enum OperateTypeEnum {

    /**
     * 新增
     */
    Add,

    /**
     * 更新
     */
    Update,

    /**
     * 删除
     */
    Delete,

}
