package com.hys.app.model.datasync.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 消息接收新增/编辑DTO
 *
 * @author zs
 * @since 2023-12-19 17:41:37
 */
@Data
public class MessageReceiveDTO {

    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "type", value = "消息类型")
    @NotBlank(message = "消息类型不能为空")
    private String type;
    
    @ApiModelProperty(name = "msg_id", value = "外部系统的消息id")
    @NotBlank(message = "外部系统的消息id不能为空")
    private String msgId;
    
    @ApiModelProperty(name = "content", value = "消息内容")
    @NotBlank(message = "消息内容不能为空")
    private String content;
    
    @ApiModelProperty(name = "produce_time", value = "外部系统生成消息的时间")
    @NotNull(message = "外部系统生成消息的时间不能为空")
    private Long produceTime;
    
    @ApiModelProperty(name = "receive_time", value = "接收消息的时间")
    @NotNull(message = "接收消息的时间不能为空")
    private Long receiveTime;
    
    @ApiModelProperty(name = "status", value = "状态")
    @NotBlank(message = "状态不能为空")
    private String status;
    
    @ApiModelProperty(name = "remark", value = "处理备注")
    @NotBlank(message = "处理备注不能为空")
    private String remark;
    
}

