package com.hys.app.model.oauth2.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 管理后台 - OAuth2 客户端分页 Request VO
 *
 * @author zs
 * @since 2024-02-20
 */
@ApiModel(description = "管理后台 - OAuth2 客户端分页 Request VO")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class OAuth2ClientPageReqVO extends BaseQueryParam {

    @ApiModelProperty(value = "客户端编号")
    private String clientId;

    @ApiModelProperty(value = "应用名，模糊匹配")
    private String name;

    @ApiModelProperty(value = "状态")
    private Integer status;

}
