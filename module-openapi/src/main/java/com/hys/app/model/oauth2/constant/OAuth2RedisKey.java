package com.hys.app.model.oauth2.constant;

import com.hys.app.model.oauth2.dos.OAuth2AccessTokenDO;

/**
 * OAuth2 Redis Key 枚举类
 *
 * @author zs
 * @since 2024-02-20
 */
public interface OAuth2RedisKey {

    /**
     * OAuth2 客户端的缓存
     * <p>
     * KEY 格式：user:{id}
     * VALUE 数据类型：String 客户端信息
     */
    String OAUTH_CLIENT = "oauth_client";

    /**
     * 访问令牌的缓存
     * <p>
     * KEY 格式：oauth2_access_token:{token}
     * VALUE 数据类型：String 访问令牌信息 {@link OAuth2AccessTokenDO}
     * <p>
     * 由于动态过期时间，使用 RedisTemplate 操作
     */
    String OAUTH2_ACCESS_TOKEN = "oauth2_access_token:%s";
}
