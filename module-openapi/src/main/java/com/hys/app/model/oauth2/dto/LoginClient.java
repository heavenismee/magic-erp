package com.hys.app.model.oauth2.dto;

import cn.hutool.core.map.MapUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hys.app.model.oauth2.dos.OAuth2ClientDO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.Map;

/**
 * OAuth2登录客户端信息
 *
 * @author zs
 * @since 2024-02-21
 */
@Data
@Accessors(chain = true)
public class LoginClient {

    /**
     * 客户端id {@link OAuth2ClientDO#getClientId()}
     */
    private String clientId;
    /**
     * 上下文字段，不进行持久化,用于临时缓存
     */
    @JsonIgnore
    private Map<String, Object> context;

    public void setContext(String key, Object value) {
        if (context == null) {
            context = new HashMap<>();
        }
        context.put(key, value);
    }

    public <T> T getContext(String key, Class<T> type) {
        return MapUtil.get(context, key, type);
    }

}
