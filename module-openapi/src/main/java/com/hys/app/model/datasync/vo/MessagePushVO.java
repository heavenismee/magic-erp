package com.hys.app.model.datasync.vo;

import com.hys.app.model.datasync.dos.MessagePushDO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 消息推送VO
 *
 * @author zs
 * @since 2023-12-18 16:24:10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class MessagePushVO extends MessagePushDO {

}

