package com.hys.app.model.oauth2.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 管理后台 - 访问令牌分页 Request VO
 *
 * @author zs
 * @since 2024-02-20
 */
@ApiModel(description = "管理后台 - 访问令牌分页 Request VO")
@Data
@EqualsAndHashCode(callSuper = true)
public class OAuth2AccessTokenPageReqVO extends BaseQueryParam {

    @ApiModelProperty(value = "用户编号")
    private Long userId;

    @ApiModelProperty(value = "用户类型，参见 UserTypeEnum 枚举")
    private Integer userType;

    @ApiModelProperty(value = "客户端编号")
    private String clientId;

}
