package com.hys.app.oauth2;

import com.hys.app.model.oauth2.dto.LoginClient;

/**
 * OAuth2User 上下文
 *
 * @author zs
 * @since 2024-02-21
 */
public class LoginClientHolder {

    private static final ThreadLocal<LoginClient> clientThreadLocal = new ThreadLocal<>();

    public static void set(LoginClient applicationDO) {
        clientThreadLocal.set(applicationDO);
    }

    public static void remove() {
        clientThreadLocal.remove();
    }

    public static LoginClient get() {
        return clientThreadLocal.get();
    }

    public static String getClientId() {
        return clientThreadLocal.get().getClientId();
    }

}
