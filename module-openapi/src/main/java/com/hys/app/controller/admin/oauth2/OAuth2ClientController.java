package com.hys.app.controller.admin.oauth2;

import cn.hutool.core.util.StrUtil;
import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.util.BeanUtils;
import com.hys.app.framework.util.StrUtils;
import com.hys.app.model.oauth2.dos.OAuth2ClientDO;
import com.hys.app.model.oauth2.dto.OAuth2ClientPageReqVO;
import com.hys.app.model.oauth2.dto.OAuth2ClientSaveReqVO;
import com.hys.app.model.oauth2.vo.OAuth2ClientRespVO;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.service.oauth2.OAuth2ClientManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * OAuth2.0 客户端 API
 *
 * @author zs
 * @since 2024-02-20
 */
@Api(tags = "管理后台 - OAuth2.0 客户端")
@RestController
@RequestMapping("/admin/systems/oauth2-client")
@Validated
public class OAuth2ClientController {

    @Autowired
    private OAuth2ClientManager oAuth2ClientManager;

    @PostMapping
    @ApiOperation(value = "创建 OAuth2 客户端")
    @Log(client = LogClient.admin, detail = "创建 OAuth2 客户端")
    public void createOAuth2Client(@Valid @RequestBody OAuth2ClientSaveReqVO createReqVO) {
        oAuth2ClientManager.createOAuth2Client(createReqVO);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "更新 OAuth2 客户端")
    @Log(client = LogClient.admin, detail = "修改id为[${updateReqVO.id}]的 OAuth2 客户端")
    public void updateOAuth2Client(@PathVariable Long id, @Valid @RequestBody OAuth2ClientSaveReqVO updateReqVO) {
        updateReqVO.setId(id);
        oAuth2ClientManager.updateOAuth2Client(updateReqVO);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除 OAuth2 客户端")
    @Log(client = LogClient.admin, detail = "删除id为[${id}]的 OAuth2 客户端")
    public void deleteOAuth2Client(@PathVariable Long id) {
        oAuth2ClientManager.deleteOAuth2Client(id);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "获得 OAuth2 客户端")
    @ApiImplicitParam(name = "id", value = "编号", required = true)
    public OAuth2ClientRespVO getOAuth2Client(@PathVariable Long id) {
        OAuth2ClientDO client = oAuth2ClientManager.getOAuth2Client(id);
        return BeanUtils.toBean(client, OAuth2ClientRespVO.class);
    }

    @GetMapping
    @ApiOperation(value = "获得 OAuth2 客户端分页")
    public WebPage<OAuth2ClientRespVO> getOAuth2ClientPage(@Valid OAuth2ClientPageReqVO pageVO) {
        WebPage<OAuth2ClientDO> pageResult = oAuth2ClientManager.getOAuth2ClientPage(pageVO);
        return BeanUtils.toBean(pageResult, OAuth2ClientRespVO.class);
    }

}
