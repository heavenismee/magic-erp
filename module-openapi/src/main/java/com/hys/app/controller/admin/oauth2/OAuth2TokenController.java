package com.hys.app.controller.admin.oauth2;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.util.BeanUtils;
import com.hys.app.model.oauth2.dos.OAuth2AccessTokenDO;
import com.hys.app.model.oauth2.dto.OAuth2AccessTokenPageReqVO;
import com.hys.app.model.oauth2.vo.OAuth2AccessTokenRespVO;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.service.oauth2.OAuth2TokenManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * OAuth2.0 令牌 API
 *
 * @author zs
 * @since 2024-02-20
 */
@Api(tags = "管理后台 - OAuth2.0 令牌")
@RestController
@RequestMapping("/admin/systems/oauth2-token")
public class OAuth2TokenController {

    @Autowired
    private OAuth2TokenManager oAuth2TokenManager;

    @GetMapping
    @ApiOperation(value = "获得访问令牌分页")
    public WebPage<OAuth2AccessTokenRespVO> getAccessTokenPage(@Valid OAuth2AccessTokenPageReqVO reqVO) {
        WebPage<OAuth2AccessTokenDO> pageResult = oAuth2TokenManager.getAccessTokenPage(reqVO);
        return BeanUtils.toBean(pageResult, OAuth2AccessTokenRespVO.class);
    }

    @DeleteMapping("/{access_tokens}")
    @ApiOperation(value = "删除访问令牌")
    @Log(client = LogClient.admin, detail = "删除访问令牌[${accessTokens}]")
    public void deleteAccessToken(@PathVariable("access_tokens") List<String> accessTokens) {
        accessTokens.forEach(oAuth2TokenManager::removeAccessToken);
    }

}
