package com.hys.app.controller.admin.datasync;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.datasync.dos.MessagePushDO;
import com.hys.app.model.datasync.dto.MessagePushQueryParams;
import com.hys.app.model.datasync.vo.MessagePushVO;
import com.hys.app.service.datasync.push.common.MessagePushManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 消息推送API
 *
 * @author zs
 * @since 2023-12-18 16:24:10
 */
@RestController
@RequestMapping("/admin/datasync/messagePush")
@Api(tags = "消息推送API")
@Validated
public class MessagePushController {

    @Autowired
    private MessagePushManager messagePushManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<MessagePushVO> list(MessagePushQueryParams queryParams) {
        return messagePushManager.list(queryParams);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public MessagePushVO getDetail(@PathVariable Long id) {
        return messagePushManager.getDetail(id);
    }

    @ApiOperation(value = "手动推送一条消息")
    @GetMapping("/{id}/push")
    public void push(@PathVariable Long id) {
        MessagePushDO messagePushDO = messagePushManager.getById(id);
        messagePushManager.pushSync(messagePushDO);
    }

}

