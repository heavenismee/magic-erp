package com.hys.app.service.oauth2.impl;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.hys.app.framework.exception.ServiceException;
import com.hys.app.model.oauth2.dos.OAuth2AccessTokenDO;
import com.hys.app.model.oauth2.dos.OAuth2CodeDO;
import com.hys.app.service.oauth2.OAuth2CodeManager;
import com.hys.app.service.oauth2.OAuth2GrantManager;
import com.hys.app.service.oauth2.OAuth2TokenManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * OAuth2 授予 Service 实现类
 * 从功能上，和 Spring Security OAuth 的 TokenGranter 的功能一致，提供访问令牌、刷新令牌的操作
 *
 * @author zs
 * @since 2024-02-20
 */
@Service
public class OAuth2GrantManagerImpl implements OAuth2GrantManager {

    @Autowired
    private OAuth2TokenManager oauth2TokenManager;
    @Autowired
    private OAuth2CodeManager oauth2CodeManager;

    @Override
    public OAuth2AccessTokenDO grantImplicit(Long userId, Integer userType,
                                             String clientId, List<String> scopes) {
        return oauth2TokenManager.createAccessToken(userId, userType, clientId, scopes);
    }

    @Override
    public String grantAuthorizationCodeForCode(Long userId, Integer userType,
                                                String clientId, List<String> scopes,
                                                String redirectUri, String state) {
        return oauth2CodeManager.createAuthorizationCode(userId, userType, clientId, scopes,
                redirectUri, state).getCode();
    }

    @Override
    public OAuth2AccessTokenDO grantAuthorizationCodeForAccessToken(String clientId, String code,
                                                                    String redirectUri, String state) {
        OAuth2CodeDO codeDO = oauth2CodeManager.consumeAuthorizationCode(code);
        Assert.notNull(codeDO, "授权码不能为空");
        // 校验 clientId 是否匹配
        if (!StrUtil.equals(clientId, codeDO.getClientId())) {
            throw new ServiceException("client_id 不匹配");
        }
        // 校验 redirectUri 是否匹配
        if (!StrUtil.equals(redirectUri, codeDO.getRedirectUri())) {
            throw new ServiceException("redirect_uri 不匹配");
        }
        // 校验 state 是否匹配
        state = StrUtil.nullToDefault(state, "");
        if (!StrUtil.equals(state, codeDO.getState())) {
            throw new ServiceException("state 不匹配");
        }

        // 创建访问令牌
        return oauth2TokenManager.createAccessToken(codeDO.getUserId(), codeDO.getUserType(),
                codeDO.getClientId(), codeDO.getScopes());
    }

    @Override
    public OAuth2AccessTokenDO grantPassword(String username, String password, String clientId, List<String> scopes) {
        // TODO 目前系统中只用到了客户端模式
        throw new UnsupportedOperationException("暂时不支持 password 授权模式");
    }

    @Override
    public OAuth2AccessTokenDO grantRefreshToken(String refreshToken, String clientId) {
        return oauth2TokenManager.refreshAccessToken(refreshToken, clientId);
    }

    @Override
    public OAuth2AccessTokenDO grantClientCredentials(String clientId, List<String> scopes) {
        // 创建访问令牌
        return oauth2TokenManager.createAccessToken(null, null,
                clientId, scopes);
    }

    @Override
    public boolean revokeToken(String clientId, String accessToken) {
        // 先查询，保证 clientId 时匹配的
        OAuth2AccessTokenDO accessTokenDO = oauth2TokenManager.getAccessToken(accessToken);
        if (accessTokenDO == null || ObjectUtil.notEqual(clientId, accessTokenDO.getClientId())) {
            return false;
        }
        // 再删除
        return oauth2TokenManager.removeAccessToken(accessToken) != null;
    }

}
