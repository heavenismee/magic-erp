package com.hys.app.service.oauth2.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import com.hys.app.framework.util.DateUtil;
import com.hys.app.mapper.oauth2.OAuth2ApproveMapper;
import com.hys.app.model.oauth2.dos.OAuth2ApproveDO;
import com.hys.app.model.oauth2.dos.OAuth2ClientDO;
import com.hys.app.service.oauth2.OAuth2ApproveManager;
import com.hys.app.service.oauth2.OAuth2ClientManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.hys.app.framework.util.CollectionUtils.convertSet;

/**
 * OAuth2 批准 Service 实现类
 * 从功能上，和 Spring Security OAuth 的 ApprovalStoreUserApprovalHandler 的功能一致，记录用户针对指定客户端的授权，减少手动确定。
 *
 * @author zs
 * @since 2024-02-20
 */
@Service
@Validated
public class OAuth2ApproveManagerImpl implements OAuth2ApproveManager {

    /**
     * 批准的过期时间，默认 30 天 (单位：秒)
     */
    private static final Integer TIMEOUT = 30 * 24 * 60 * 60;

    @Autowired
    private OAuth2ClientManager oauth2ClientManager;

    @Autowired
    private OAuth2ApproveMapper oauth2ApproveMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean checkForPreApproval(Long userId, Integer userType, String clientId, Collection<String> requestedScopes) {
        // 第一步，基于 Client 的自动授权计算，如果 scopes 都在自动授权中，则返回 true 通过
        OAuth2ClientDO clientDO = oauth2ClientManager.validOAuthClientFromCache(clientId);
        Assert.notNull(clientDO, "客户端不能为空");
        if (CollUtil.containsAll(clientDO.getAutoApproveScopes(), requestedScopes)) {
            // gh-877 - if all scopes are auto approved, approvals still need to be added to the approval store.
            Long expireTime = DateUtil.plusSeconds(TIMEOUT);
            for (String scope : requestedScopes) {
                saveApprove(userId, userType, clientId, scope, true, expireTime);
            }
            return true;
        }

        // 第二步，算上用户已经批准的授权。如果 scopes 都包含，则返回 true
        List<OAuth2ApproveDO> approveList = getApproveList(userId, userType, clientId);
        Set<String> scopes = convertSet(approveList, OAuth2ApproveDO::getScope, OAuth2ApproveDO::getApproved);
        return CollUtil.containsAll(scopes, requestedScopes);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateAfterApproval(Long userId, Integer userType, String clientId, Map<String, Boolean> requestedScopes) {
        // 如果 requestedScopes 为空，说明没有要求，则返回 true 通过
        if (CollUtil.isEmpty(requestedScopes)) {
            return true;
        }

        // 更新批准的信息, 需要至少有一个同意
        boolean success = false;
        Long expireTime = DateUtil.plusSeconds(TIMEOUT);
        for (Map.Entry<String, Boolean> entry : requestedScopes.entrySet()) {
            if (entry.getValue()) {
                success = true;
            }
            saveApprove(userId, userType, clientId, entry.getKey(), entry.getValue(), expireTime);
        }
        return success;
    }

    @Override
    public List<OAuth2ApproveDO> getApproveList(Long userId, Integer userType, String clientId) {
        List<OAuth2ApproveDO> approveList = oauth2ApproveMapper.selectListByUserIdAndUserTypeAndClientId(
                userId, userType, clientId);
        approveList.removeIf(o -> DateUtil.isExpired(o.getExpiresTime()));
        return approveList;
    }

    private void saveApprove(Long userId, Integer userType, String clientId,
                             String scope, Boolean approved, Long expireTime) {
        // 先更新
        OAuth2ApproveDO approveDO = new OAuth2ApproveDO().setUserId(userId).setUserType(userType)
                .setClientId(clientId).setScope(scope).setApproved(approved).setExpiresTime(expireTime);
        if (oauth2ApproveMapper.update(approveDO) == 1) {
            return;
        }
        // 失败，则说明不存在，进行更新
        oauth2ApproveMapper.insert(approveDO);
    }

}
