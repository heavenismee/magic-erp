package com.hys.app.mq.core.system;

import com.hys.app.mq.event.SystemLogsEvent;
import com.hys.app.model.system.dos.SystemLogs;
import com.hys.app.service.system.SystemLogsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 系统日志消费者
 *
 * @author fk
 * @version v2.0
 * @since v7.3.0
 * 2021年03月23日16:59:23
 */
@Service
public class SystemLogsConsumer implements SystemLogsEvent {


    @Autowired
    private SystemLogsManager systemLogsManager;


    @Override
    public void add(SystemLogs systemLogs) {

        systemLogsManager.add(systemLogs);
    }
}
