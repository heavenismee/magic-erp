package com.hys.app.mq.receiver;

import com.hys.app.mq.event.StockDamageAuditPassEvent;
import com.hys.app.model.base.rabbitmq.AmqpExchange;
import com.hys.app.model.erp.dto.message.StockDamageAuditPassMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 库存报损单审核通过
 *
 * @author zs
 * @since 2024-01-09
 */
@Component
public class StockDamageAuditPassReceiver {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<StockDamageAuditPassEvent> events;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.STOCK_DAMAGE_AUDIT_PASS + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.STOCK_DAMAGE_AUDIT_PASS, type = ExchangeTypes.FANOUT)
    ))
    public void receive(StockDamageAuditPassMessage message) {
        if (events != null) {
            for (StockDamageAuditPassEvent event : events) {
                try {
                    event.onStockDamageAuditPass(message);
                } catch (Exception e) {
                    logger.error("库存报损单审核通过消息出错" + event.getClass().getName(), e);
                }
            }
        }
    }
}
