package com.hys.app.mq.receiver;

import com.hys.app.mq.event.SupplierReturnAuditPassEvent;
import com.hys.app.model.base.rabbitmq.AmqpExchange;
import com.hys.app.model.erp.dto.message.SupplierReturnAuditPassMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 供应商退货单审核通过
 *
 * @author zs
 * @since 2024-01-08
 */
@Component
public class SupplierReturnAuditPassReceiver {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<SupplierReturnAuditPassEvent> events;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.SUPPLIER_RETURN_AUDIT_PASS + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.SUPPLIER_RETURN_AUDIT_PASS, type = ExchangeTypes.FANOUT)
    ))
    public void receive(SupplierReturnAuditPassMessage message) {
        if (events != null) {
            for (SupplierReturnAuditPassEvent event : events) {
                try {
                    event.onSupplierReturnAuditPass(message);
                } catch (Exception e) {
                    logger.error("供应商退货单审核通过消息出错" + event.getClass().getName(), e);
                }
            }
        }
    }
}
