package com.hys.app.mq.receiver;

import com.hys.app.mq.event.ChangeFormAuditPassEvent;
import com.hys.app.model.base.rabbitmq.AmqpExchange;
import com.hys.app.model.erp.dto.message.ChangeFormAuditPassMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 换货单审核通过
 *
 * @author zs
 * @since 2024-01-09
 */
@Component
public class ChangeFormAuditPassReceiver {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<ChangeFormAuditPassEvent> events;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.CHANGE_FORM_AUDIT_PASS + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.CHANGE_FORM_AUDIT_PASS, type = ExchangeTypes.FANOUT)
    ))
    public void receive(ChangeFormAuditPassMessage message) {
        if (events != null) {
            for (ChangeFormAuditPassEvent event : events) {
                try {
                    event.onChangeFormAuditPass(message);
                } catch (Exception e) {
                    logger.error("换货单审核通过消息出错" + event.getClass().getName(), e);
                }
            }
        }
    }
}
