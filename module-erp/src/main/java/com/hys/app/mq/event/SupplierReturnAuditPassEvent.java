package com.hys.app.mq.event;

import com.hys.app.model.erp.dto.message.SupplierReturnAuditPassMessage;

/**
 * 供应商退货单审核通过事件
 *
 * @author zs
 * @since 2024-01-08
 */
public interface SupplierReturnAuditPassEvent {

    /**
     * 供应商退货单审核通过
     *
     * @param message
     */
    void onSupplierReturnAuditPass(SupplierReturnAuditPassMessage message);

}
