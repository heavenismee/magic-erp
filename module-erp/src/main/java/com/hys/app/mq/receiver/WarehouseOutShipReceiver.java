package com.hys.app.mq.receiver;

import com.hys.app.mq.event.WarehouseOutShipEvent;
import com.hys.app.model.base.rabbitmq.AmqpExchange;
import com.hys.app.model.erp.dto.message.WarehouseOutShipMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 出库单发货消息
 *
 * @author zs
 * @since 2024-01-09
 */
@Component
public class WarehouseOutShipReceiver {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<WarehouseOutShipEvent> events;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.WAREHOUSE_OUT_SHIP + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.WAREHOUSE_OUT_SHIP, type = ExchangeTypes.FANOUT)
    ))
    public void receive(WarehouseOutShipMessage message) {
        if (events != null) {
            for (WarehouseOutShipEvent event : events) {
                try {
                    event.onWarehouseOutShip(message);
                } catch (Exception e) {
                    logger.error("出库单发货消息出错" + event.getClass().getName(), e);
                }
            }
        }
    }
}
