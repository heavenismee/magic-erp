package com.hys.app.controller.erp.sale;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.OrderReturnItemQueryParams;
import com.hys.app.model.erp.vo.OrderReturnItemVO;
import com.hys.app.service.erp.OrderReturnItemManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单退货项API
 *
 * @author zs
 * @since 2023-12-14 15:42:07
 */
@RestController
@RequestMapping("/admin/erp/orderReturnItem")
@Api(tags = "订单退货项API")
@Validated
public class OrderReturnItemManagerController {

    @Autowired
    private OrderReturnItemManager orderReturnItemManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<OrderReturnItemVO> list(OrderReturnItemQueryParams queryParams) {
        return orderReturnItemManager.list(queryParams);
    }
}

