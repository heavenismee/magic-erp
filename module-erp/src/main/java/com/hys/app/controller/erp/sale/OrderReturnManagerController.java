package com.hys.app.controller.erp.sale;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.OrderReturnQueryParams;
import com.hys.app.model.erp.dto.OrderReturnStatisticsQueryParam;
import com.hys.app.model.erp.enums.OrderReturnStatusEnum;
import com.hys.app.model.erp.vo.OrderReturnVO;
import com.hys.app.model.erp.dto.OrderReturnDTO;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.service.erp.OrderReturnManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 订单退货API
 *
 * @author zs
 * @since 2023-12-14 15:42:07
 */
@RestController
@RequestMapping("/admin/erp/orderReturn")
@Api(tags = "订单退货API")
@Validated
public class OrderReturnManagerController {

    @Autowired
    private OrderReturnManager orderReturnManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<OrderReturnVO> list(OrderReturnQueryParams queryParams) {
        return orderReturnManager.list(queryParams);
    }

    @ApiOperation(value = "添加")
    @PostMapping
    @Log(client = LogClient.admin, detail = "创建订单退货单")
    public void add(@RequestBody @Valid OrderReturnDTO orderReturnDTO) {
        orderReturnManager.add(orderReturnDTO);
    }

    @ApiOperation(value = "修改")
    @PutMapping("/{id}")
    @Log(client = LogClient.admin, detail = "修改ID为[${id}]的订单退货单")
    public void edit(@PathVariable Long id, @RequestBody @Valid OrderReturnDTO orderReturnDTO) {
        orderReturnDTO.setId(id);
        orderReturnManager.edit(orderReturnDTO);
    }

    @ApiOperation(value = "提交")
    @PostMapping("/{id}/submit")
    @Log(client = LogClient.admin, detail = "提交ID为[${id}]的订单退货单")
    public void submit(@PathVariable Long id) {
        orderReturnManager.submit(id);
    }

    @ApiOperation(value = "撤销提交")
    @PostMapping("/{id}/withdraw")
    @Log(client = LogClient.admin, detail = "撤销提交ID为[${id}]的订单退货单")
    public void withdraw(@PathVariable Long id) {
        orderReturnManager.withdraw(id);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public OrderReturnVO getDetail(@PathVariable Long id) {
        return orderReturnManager.getDetail(id);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/{ids}")
    @Log(client = LogClient.admin, detail = "删除ID为[${ids}]的订单退货单")
    public void delete(@PathVariable List<Long> ids) {
        orderReturnManager.delete(ids);
    }

    @ApiOperation(value = "审核")
    @PostMapping("/{ids}/audit")
    @Log(client = LogClient.admin, detail = "审核ID为[${ids}]的订单退货单")
    public void audit(@PathVariable List<Long> ids, @NotNull OrderReturnStatusEnum status, String remark) {
        orderReturnManager.audit(ids, status, remark);
    }

    @ApiOperation(value = "查询订单退货统计分页列表数据")
    @GetMapping("/statistics")
    public WebPage statistics(OrderReturnStatisticsQueryParam params) {
        return orderReturnManager.statistics(params);
    }

    @ApiOperation(value = "导出订单退货统计列表数据")
    @GetMapping("/export")
    public void export(HttpServletResponse response, OrderReturnStatisticsQueryParam params) {
        this.orderReturnManager.export(response, params);
    }
}

