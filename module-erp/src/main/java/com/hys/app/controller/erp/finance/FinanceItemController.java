package com.hys.app.controller.erp.finance;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.FinanceItemDTO;
import com.hys.app.model.erp.dto.FinanceItemQueryParams;
import com.hys.app.model.erp.enums.FinanceIncomeTypeEnum;
import com.hys.app.model.erp.vo.FinanceItemVO;
import com.hys.app.service.erp.FinanceItemManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 财务明细 API
 *
 * @author zsong
 * 2024-03-15 17:40:23
 */
@RestController
@RequestMapping("/admin/erp/financeItem")
@Api(tags = "财务明细API")
@Validated
public class FinanceItemController {

    @Autowired
    private FinanceItemManager financeItemManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<FinanceItemVO> list(FinanceItemQueryParams queryParams) {
        return financeItemManager.list(queryParams);
    }

    @ApiOperation(value = "新增")
    @PostMapping
    public void add(@RequestBody @Valid FinanceItemDTO financeItemDTO) {
        financeItemDTO.setSourceType(FinanceIncomeTypeEnum.Other.name());
        financeItemManager.add(financeItemDTO);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public FinanceItemVO getDetail(@PathVariable Long id) {
        return financeItemManager.getDetail(id);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/{ids}")
    public void delete(@PathVariable List<Long> ids) {
        financeItemManager.delete(ids);
    }


}

