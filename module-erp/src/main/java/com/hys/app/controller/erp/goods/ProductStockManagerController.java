package com.hys.app.controller.erp.goods;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.ProductStockQueryParams;
import com.hys.app.model.erp.vo.ProductStockVO;
import com.hys.app.model.erp.vo.ProductStockWarningVO;
import com.hys.app.service.erp.ProductStockManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 商品库存相关API
 *
 * @author zs
 * @since 2023-12-07 10:08:44
 */
@RestController
@RequestMapping("/admin/erp/productStock")
@Api(tags = "商品库存相关API")
@Validated
public class ProductStockManagerController {

    @Autowired
    private ProductStockManager productStockManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<ProductStockVO> list(ProductStockQueryParams queryParams) {
        return productStockManager.list(queryParams);
    }

    @ApiOperation(value = "查询商品库存预警分页列表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "分页数", dataType = "int", paramType = "query", required = true),
            @ApiImplicitParam(name = "page_size", value = "每页数量", dataType = "int", paramType = "query", required = true),
            @ApiImplicitParam(name = "warehouse_id", value = "仓库ID", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "dept_id", value = "部门ID", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "商品名称", dataType = "String", paramType = "query")
    })
    @GetMapping("/list-stock-warning")
    public WebPage<ProductStockWarningVO> listWarningStock(@ApiIgnore Long pageNo, @ApiIgnore Long pageSize,
                                                           @ApiIgnore Long warehouseId, @ApiIgnore Long deptId, @ApiIgnore String name) {
        return productStockManager.listWarningStock(pageNo, pageSize, warehouseId, deptId, name);
    }
}

