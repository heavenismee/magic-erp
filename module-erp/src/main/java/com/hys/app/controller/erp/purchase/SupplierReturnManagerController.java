package com.hys.app.controller.erp.purchase;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.SupplierReturnQueryParams;
import com.hys.app.model.erp.dto.SupplierReturnStatisticsParam;
import com.hys.app.model.erp.enums.SupplierReturnStatusEnum;
import com.hys.app.model.erp.vo.SupplierReturnVO;
import com.hys.app.model.erp.dto.SupplierReturnDTO;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.service.erp.SupplierReturnManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 供应商退货API
 *
 * @author zs
 * @since 2023-12-14 11:12:46
 */
@RestController
@RequestMapping("/admin/erp/supplierReturn")
@Api(tags = "供应商退货API")
@Validated
public class SupplierReturnManagerController {

    @Autowired
    private SupplierReturnManager supplierReturnManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<SupplierReturnVO> list(SupplierReturnQueryParams queryParams) {
        return supplierReturnManager.list(queryParams);
    }

    @ApiOperation(value = "添加")
    @PostMapping
    @Log(client = LogClient.admin, detail = "创建供应商退货单")
    public void add(@RequestBody @Valid SupplierReturnDTO supplierReturnDTO) {
        supplierReturnManager.add(supplierReturnDTO);
    }

    @ApiOperation(value = "修改")
    @PutMapping("/{id}")
    @Log(client = LogClient.admin, detail = "修改ID为[${id}]的供应商退货单")
    public void edit(@PathVariable Long id, @RequestBody @Valid SupplierReturnDTO supplierReturnDTO) {
        supplierReturnDTO.setId(id);
        supplierReturnManager.edit(supplierReturnDTO);
    }

    @ApiOperation(value = "提交")
    @PostMapping("/{id}/submit")
    @Log(client = LogClient.admin, detail = "提交ID为[${id}]的供应商退货单")
    public void submit(@PathVariable Long id) {
        supplierReturnManager.submit(id);
    }

    @ApiOperation(value = "撤销提交")
    @PostMapping("/{id}/withdraw")
    @Log(client = LogClient.admin, detail = "撤销提交ID为[${id}]的供应商退货单")
    public void withdraw(@PathVariable Long id) {
        supplierReturnManager.withdraw(id);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public SupplierReturnVO getDetail(@PathVariable Long id) {
        return supplierReturnManager.getDetail(id);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/{ids}")
    @Log(client = LogClient.admin, detail = "删除ID为[${ids}]的供应商退货单")
    public void delete(@PathVariable List<Long> ids) {
        supplierReturnManager.delete(ids);
    }

    @ApiOperation(value = "审核")
    @PostMapping("/{ids}/audit")
    @Log(client = LogClient.admin, detail = "审核ID为[${ids}]的供应商退货单")
    public void audit(@PathVariable List<Long> ids, @NotNull SupplierReturnStatusEnum status, String remark) {
        supplierReturnManager.audit(ids, status, remark);
    }

    @ApiOperation(value = "查询供应商退货统计分页列表数据")
    @GetMapping("/statistics")
    public WebPage statistics(SupplierReturnStatisticsParam params) {
        return supplierReturnManager.statistics(params);
    }

    @ApiOperation(value = "导出供应商退货统计列表")
    @GetMapping("/export")
    public void export(HttpServletResponse response, SupplierReturnStatisticsParam params) {
        this.supplierReturnManager.export(response, params);
    }
}

