package com.hys.app.controller.erp.basic;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.MemberDTO;
import com.hys.app.model.erp.dto.MemberQueryParams;
import com.hys.app.model.erp.vo.MemberVO;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.service.erp.MemberManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

/**
 * 会员 API
 *
 * @author zsong
 * 2024-01-23 09:28:16
 */
@RestController
@RequestMapping("/admin/erp/member")
@Api(tags = "会员API")
@Validated
public class MemberController {

    @Autowired
    private MemberManager memberManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<MemberVO> list(MemberQueryParams queryParams) {
        return memberManager.list(queryParams);
    }

    @ApiOperation(value = "添加")
    @PostMapping
    @Log(client = LogClient.admin, detail = "新增会员")
    public void add(@RequestBody @Valid MemberDTO memberDTO) {
        memberManager.add(memberDTO);
    }

    @ApiOperation(value = "修改")
    @PutMapping("/{id}")
    @Log(client = LogClient.admin, detail = "修改id为[${id}]的会员")
    public void edit(@PathVariable Long id, @RequestBody @Valid MemberDTO memberDTO) {
        memberDTO.setId(id);
        memberManager.edit(memberDTO);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public MemberVO getDetail(@PathVariable Long id) {
        return memberManager.getDetail(id);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/{ids}")
    @Log(client = LogClient.admin, detail = "删除id为[${ids}]的会员")
    public void delete(@PathVariable List<Long> ids) {
        memberManager.delete(ids);
    }

    @PostMapping("/import")
    @ApiOperation(value = "导入")
    public void importExcel(@RequestParam("file") @RequestPart MultipartFile file) {
        memberManager.importExcel(file);
    }

    @ApiOperation(value = "禁用")
    @PutMapping("/{id}/disable")
    @Log(client = LogClient.admin, detail = "禁用ID为[${id}]的会员信息")
    public void disable(@PathVariable Long id) {
        memberManager.setDisable(id,true);
    }

    @ApiOperation(value = "恢复")
    @PutMapping("/{id}/enable")
    @Log(client = LogClient.admin, detail = "恢复ID为[${id}]的会员信息")
    public void enable(@PathVariable Long id) {
        memberManager.setDisable(id,false);
    }

}

