package com.hys.app.controller.erp.sale;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.OrderPaymentQueryParams;
import com.hys.app.model.erp.vo.OrderPaymentVO;
import com.hys.app.service.erp.OrderPaymentManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单支付明细 API
 *
 * @author zsong
 * 2024-01-24 17:00:32
 */
@RestController
@RequestMapping("/admin/erp/orderPayment")
@Api(tags = "订单支付明细API")
@Validated
public class OrderPaymentController {

    @Autowired
    private OrderPaymentManager orderPaymentManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<OrderPaymentVO> list(OrderPaymentQueryParams queryParams) {
        return orderPaymentManager.list(queryParams);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public OrderPaymentVO getDetail(@PathVariable Long id) {
        return orderPaymentManager.getDetail(id);
    }

}

