package com.hys.app.controller.system;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.PostQueryParams;
import com.hys.app.model.erp.vo.PostVO;
import com.hys.app.model.erp.dto.PostDTO;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.service.erp.PostManager;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

/**
 * 岗位相关API
 *
 * @author zs
 * 2023-11-29 17:15:22
 */
@RestController
@RequestMapping("/admin/erp/post")
@Api(tags = "岗位相关API")
@Validated
public class PostManagerController {

    @Autowired
    private PostManager postManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<PostVO> list(PostQueryParams queryParams) {
        return postManager.list(queryParams);
    }

    @ApiOperation(value = "查询全部")
    @GetMapping("/list-all")
    public List<PostVO> list() {
        return postManager.listAll();
    }

    @ApiOperation(value = "添加")
    @PostMapping
    @Log(client = LogClient.admin, detail = "添加名称为[${postDTO.name}]的岗位")
    public void add(@RequestBody @Valid PostDTO postDTO) {
        postManager.add(postDTO);
    }

    @ApiOperation(value = "修改")
    @PutMapping("/{id}")
    @Log(client = LogClient.admin, detail = "编辑ID为[${id}]的岗位")
    public void edit(@PathVariable Long id, @RequestBody @Valid PostDTO postDTO) {
        postDTO.setId(id);
        postManager.edit(postDTO);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public PostVO getDetail(@PathVariable Long id) {
        return postManager.getDetail(id);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/{ids}")
    @Log(client = LogClient.admin, detail = "删除ID为${ids}的岗位")
    public void delete(@PathVariable Long[] ids) {
        postManager.delete(Arrays.asList(ids));
    }
}

