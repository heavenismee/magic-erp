package com.hys.app.controller.erp.goods;

import com.hys.app.model.system.dos.ShipTemplateDO;
import com.hys.app.model.system.vo.ShipTemplateSellerVO;
import com.hys.app.model.system.vo.ShipTemplateVO;
import com.hys.app.service.system.ShipTemplateManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;

/**
 * 运费模板相关API
 * @author duanmingyu
 * @since 7.2.2_rc
 * @version 1.0
 * 2021-08-03
 */
@Api(description = "运费模板API")
@RestController
@RequestMapping("/admin/trade/ship-templates")
@Validated
public class ShipTemplateManagerController {

    @Autowired
    private ShipTemplateManager shipTemplateManager;


    @ApiOperation(value = "查询运费模版列表数据集合", response = ShipTemplateVO.class)
    @GetMapping
    public List<ShipTemplateSellerVO> list() {
        return this.shipTemplateManager.getStoreTemplate();
    }

    @ApiOperation(value = "添加运费模版", response = ShipTemplateDO.class)
    @PostMapping
    public ShipTemplateDO add(@Valid @RequestBody ShipTemplateSellerVO shipTemplate) {

        return this.shipTemplateManager.save(shipTemplate);
    }

    @PutMapping(value = "/{template_id}")
    @ApiOperation(value = "修改运费模版", response = ShipTemplateDO.class)
    @ApiImplicitParam(name = "template_id", value = "模版id", required = true, dataType = "int", paramType = "path")
    public ShipTemplateDO edit(@Valid @RequestBody ShipTemplateSellerVO shipTemplate, @ApiIgnore @PathVariable("template_id") Long templateId) {
        shipTemplate.setId(templateId);
        return this.shipTemplateManager.edit(shipTemplate);
    }


    @DeleteMapping(value = "/{template_id}")
    @ApiOperation(value = "删除运费模版")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "template_id", value = "要删除的运费模版主键", required = true, dataType = "int", paramType = "path")
    })
    public void delete(@ApiIgnore @PathVariable("template_id") Long templateId) {
        this.shipTemplateManager.delete(templateId);
    }

    @GetMapping(value = "/{template_id}")
    @ApiOperation(value = "查询一个运费模版")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "template_id", value = "要查询的运费模版主键", required = true, dataType = "int", paramType = "path")
    })
    public ShipTemplateSellerVO get(@ApiIgnore @PathVariable("template_id") Long templateId) {

        ShipTemplateSellerVO shipTemplate = this.shipTemplateManager.getFromDB(templateId);

        return shipTemplate;
    }
}
