package com.hys.app.framework.datapermission.dept;

import com.hys.app.service.system.RoleManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * 基于部门的数据权限 AutoConfiguration
 *
 * @author zs
 */
@Configuration
public class DeptDataPermissionConfiguration {

    @Bean
    public DeptDataPermissionRule deptDataPermissionRule(RoleManager roleManager,
                                                         List<DeptDataPermissionRuleCustomizer> customizers) {
        // 创建 DeptDataPermissionRule 对象
        DeptDataPermissionRule rule = new DeptDataPermissionRule(roleManager);
        // 补全表配置
        customizers.forEach(customizer -> customizer.customize(rule));
        return rule;
    }

}
