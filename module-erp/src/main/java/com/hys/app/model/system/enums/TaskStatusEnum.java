package com.hys.app.model.system.enums;

/**
 * 任务状态
 * @author zs
 * @since 2024-01-23
 **/
public enum TaskStatusEnum {
    /**
     * 执行中
     */
    Running,

    /**
     * 执行成功
     */
    Success,

    /**
     * 执行失败
     */
    Error,

    /**
     * 手动终止
     */
    Stop,

}
