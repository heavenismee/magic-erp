package com.hys.app.model.system.vo;

import com.hys.app.model.system.dos.TaskDO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 任务详情
 *
 * @author zsong
 * 2024-01-23 10:43:35
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "任务详情")
public class TaskVO extends TaskDO {

}

