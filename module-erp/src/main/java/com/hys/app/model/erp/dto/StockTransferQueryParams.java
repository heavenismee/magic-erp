package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import com.hys.app.model.erp.enums.StockTransferStatusEnum;
import com.hys.app.model.erp.enums.StockTransferTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Set;

/**
 * 库存调拨查询参数
 *
 * @author zs
 * @since 2023-12-12 11:59:06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class StockTransferQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "sn", value = "编号")
    private String sn;

    @ApiModelProperty(name = "out_dept_id", value = "调出方部门id")
    private Long outDeptId;

    @ApiModelProperty(name = "out_warehouse_id", value = "调出方仓库id")
    private Long outWarehouseId;

    @ApiModelProperty(name = "out_handled_by", value = "调出方仓库经手人")
    private String outHandledBy;

    @ApiModelProperty(name = "in_dept_id", value = "调入方部门id")
    private Long inDeptId;

    @ApiModelProperty(name = "in_warehouse_id", value = "调入方仓库id")
    private Long inWarehouseId;

    @ApiModelProperty(name = "in_handled_by", value = "调入方仓库经手人")
    private String inHandledBy;

    @ApiModelProperty(name = "status", value = "状态")
    private StockTransferStatusEnum status;

    @ApiModelProperty(name = "start_time", value = "开始时间")
    private Long startTime;

    @ApiModelProperty(name = "end_time", value = "截止时间")
    private Long endTime;

    @ApiModelProperty(name = "type", value = "调拨类型")
    private StockTransferTypeEnum type;

    @JsonIgnore
    @ApiModelProperty(value = "部门id集合", hidden = true)
    private Set<Long> deptIdList;

}

