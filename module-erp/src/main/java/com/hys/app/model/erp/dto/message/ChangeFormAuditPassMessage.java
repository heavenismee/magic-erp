package com.hys.app.model.erp.dto.message;

import com.hys.app.model.erp.dos.ChangeForm;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 换货单审核通过消息
 *
 * @author zs
 * @since 2024-01-09
 */
@Data
public class ChangeFormAuditPassMessage implements Serializable {

    private static final long serialVersionUID = 524607318299159659L;

    private List<ChangeForm> list;

}
