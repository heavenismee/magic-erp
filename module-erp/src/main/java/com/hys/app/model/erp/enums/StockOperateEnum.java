package com.hys.app.model.erp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 库存操作枚举
 *
 * @author zs
 * @since 2023-12-05
 */
@Getter
@AllArgsConstructor
public enum StockOperateEnum {

    /**
     * 增加
     */
    Increase,
    /**
     * 减少
     */
    Reduce,

}
