package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 订单明细分页查询参数
 *
 * @author zsong
 * 2024-01-24 16:39:38
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "订单明细分页查询参数")
public class OrderItemQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "order_id", value = "订单id")
    private Long orderId;
    
    @ApiModelProperty(name = "goods_id", value = "商品id")
    private Long goodsId;
    
    @ApiModelProperty(name = "product_id", value = "产品id")
    private Long productId;
    
    @ApiModelProperty(name = "product_sn", value = "产品编号")
    private String productSn;
    
    @ApiModelProperty(name = "product_name", value = "产品名称")
    private String productName;
    
    @ApiModelProperty(name = "product_specification", value = "产品规格")
    private String productSpecification;
    
    @ApiModelProperty(name = "product_unit", value = "产品单位")
    private String productUnit;
    
    @ApiModelProperty(name = "category_id", value = "分类id")
    private Long categoryId;
    
    @ApiModelProperty(name = "category_name", value = "分类名称")
    private String categoryName;
    
    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;
    
    @ApiModelProperty(name = "create_time", value = "创建时间")
    private Long[] createTime;
    
}

