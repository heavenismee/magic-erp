package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.WarehouseEntryBatchDO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 入库批次详情
 *
 * @author zs
 * @since 2023-12-08 11:49:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WarehouseEntryBatchVO extends WarehouseEntryBatchDO {

}

