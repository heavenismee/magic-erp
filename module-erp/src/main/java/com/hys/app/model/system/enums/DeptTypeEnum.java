package com.hys.app.model.system.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 部门类型枚举
 *
 * @author zs
 * @since 2023-11-30
 */
@Getter
@AllArgsConstructor
public enum DeptTypeEnum {

    /**
     * 机构
     */
    INSTITUTION,
    /**
     * 管理部门
     */
    MANAGER,
    /**
     * 分部
     */
    BRANCH


}
