package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 调拨单商品明细查询参数
 *
 * @author zs
 * @since 2023-12-12 16:34:15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class StockTransferProductQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "stock_transfer_id", value = "调拨单id")
    private Long stockTransferId;
    
    @ApiModelProperty(name = "warehouse_entry_sn", value = "入库单号")
    private String warehouseEntrySn;
    
    @ApiModelProperty(name = "warehouse_entry_batch_sn", value = "入库批次号")
    private String warehouseEntryBatchSn;
    
    @ApiModelProperty(name = "product_id", value = "商品id")
    private Long productId;
    
    @ApiModelProperty(name = "product_sn", value = "商品编号")
    private String productSn;
    
    @ApiModelProperty(name = "product_name", value = "商品名称")
    private String productName;
    
    @ApiModelProperty(name = "product_specification", value = "商品规格")
    private String productSpecification;
    
    @ApiModelProperty(name = "product_unit", value = "商品单位")
    private String productUnit;
    
    @ApiModelProperty(name = "product_price", value = "入库单价")
    private Double productPrice;
    
    @ApiModelProperty(name = "num", value = "调拨数量")
    private Integer num;
    
}

