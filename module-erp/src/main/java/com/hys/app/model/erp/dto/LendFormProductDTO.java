package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 借出单产品实体DTO
 * @Author dmy
 * 2023-12-05
 */
@ApiModel
public class LendFormProductDTO implements Serializable {

    private static final long serialVersionUID = 4592041316954015127L;

    /**
     * 入库单编号
     */
    @ApiModelProperty(name = "stock_sn", value = "入库单编号", required = true)
    @NotEmpty(message = "入库单编号不能为空")
    private String stockSn;
    /**
     * 入库批次ID
     */
    @ApiModelProperty(name = "stock_batch_id", value = "入库批次ID")
    private Long stockBatchId;
    /**
     * 入库批次号
     */
    @ApiModelProperty(name = "stock_batch_sn", value = "入库批次号")
    private String stockBatchSn;
    /**
     * 商品ID
     */
    @ApiModelProperty(name = "goods_id", value = "商品ID", required = true)
    @NotNull(message = "商品ID不能为空")
    private Long goodsId;
    /**
     * 产品ID
     */
    @ApiModelProperty(name = "product_id", value = "产品ID", required = true)
    @NotNull(message = "产品ID不能为空")
    private Long productId;
    /**
     * 产品名称
     */
    @ApiModelProperty(name = "product_name", value = "产品名称", required = true)
    @NotEmpty(message = "产品名称不能为空")
    private String productName;
    /**
     * 产品编号
     */
    @ApiModelProperty(name = "product_sn", value = "产品编号", required = true)
    @NotEmpty(message = "产品编号不能为空")
    private String productSn;
    /**
     * 产品规格
     */
    @ApiModelProperty(name = "specification", value = "产品规格", required = true)
    @NotEmpty(message = "产品规格不能为空")
    private String specification;
    /**
     * 分类名称
     */
    @ApiModelProperty(name = "category_name", value = "分类名称", required = true)
    @NotEmpty(message = "分类名称不能为空")
    private String categoryName;
    /**
     * 产品单位
     */
    @ApiModelProperty(name = "unit", value = "产品单位", required = true)
    @NotEmpty(message = "产品单位不能为空")
    private String unit;
    /**
     * 库存数量
     */
    @ApiModelProperty(name = "stock_num", value = "库存数量", required = true)
    @NotNull(message = "库存数量不能为空")
    private Integer stockNum;
    /**
     * 借出数量
     */
    @ApiModelProperty(name = "lend_num", value = "借出数量", required = true)
    @NotNull(message = "借出数量不能为空")
    private Integer lendNum;

    public String getStockSn() {
        return stockSn;
    }

    public void setStockSn(String stockSn) {
        this.stockSn = stockSn;
    }

    public Long getStockBatchId() {
        return stockBatchId;
    }

    public void setStockBatchId(Long stockBatchId) {
        this.stockBatchId = stockBatchId;
    }

    public String getStockBatchSn() {
        return stockBatchSn;
    }

    public void setStockBatchSn(String stockBatchSn) {
        this.stockBatchSn = stockBatchSn;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSn() {
        return productSn;
    }

    public void setProductSn(String productSn) {
        this.productSn = productSn;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public Integer getLendNum() {
        return lendNum;
    }

    public void setLendNum(Integer lendNum) {
        this.lendNum = lendNum;
    }

    @Override
    public String toString() {
        return "LendFormProductDTO{" +
                "stockSn='" + stockSn + '\'' +
                ", stockBatchId=" + stockBatchId +
                ", stockBatchSn='" + stockBatchSn + '\'' +
                ", goodsId=" + goodsId +
                ", productId=" + productId +
                ", productName='" + productName + '\'' +
                ", productSn='" + productSn + '\'' +
                ", specification='" + specification + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", unit='" + unit + '\'' +
                ", stockNum=" + stockNum +
                ", lendNum=" + lendNum +
                '}';
    }
}
