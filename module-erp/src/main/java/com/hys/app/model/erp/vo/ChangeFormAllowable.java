package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.ChangeForm;
import com.hys.app.model.erp.enums.ChangeFormStatusEnum;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 换货单操作类
 *
 * @author dmy
 * 2023-12-06
 */
public class ChangeFormAllowable implements Serializable {

    private static final long serialVersionUID = -6085884143018842732L;

    @ApiModelProperty(name = "allow_edit", value = "是否允许编辑")
    private Boolean allowEdit;

    @ApiModelProperty(name = "allow_delete", value = "是否允许删除")
    private Boolean allowDelete;

    @ApiModelProperty(name = "allow_submit", value = "是否允许提交")
    private Boolean allowSubmit;

    @ApiModelProperty(name = "allow_cancel", value = "是否允许撤销")
    private Boolean allowCancel;

    @ApiModelProperty(name = "allow_audit", value = "是否允许审核")
    private Boolean allowAudit;

    public Boolean getAllowEdit() {
        return allowEdit;
    }

    public void setAllowEdit(Boolean allowEdit) {
        this.allowEdit = allowEdit;
    }

    public Boolean getAllowDelete() {
        return allowDelete;
    }

    public void setAllowDelete(Boolean allowDelete) {
        this.allowDelete = allowDelete;
    }

    public Boolean getAllowSubmit() {
        return allowSubmit;
    }

    public void setAllowSubmit(Boolean allowSubmit) {
        this.allowSubmit = allowSubmit;
    }

    public Boolean getAllowCancel() {
        return allowCancel;
    }

    public void setAllowCancel(Boolean allowCancel) {
        this.allowCancel = allowCancel;
    }

    public Boolean getAllowAudit() {
        return allowAudit;
    }

    public void setAllowAudit(Boolean allowAudit) {
        this.allowAudit = allowAudit;
    }

    @Override
    public String toString() {
        return "ChangeFormAllowable{" +
                "allowEdit=" + allowEdit +
                ", allowDelete=" + allowDelete +
                ", allowSubmit=" + allowSubmit +
                ", allowCancel=" + allowCancel +
                ", allowAudit=" + allowAudit +
                '}';
    }

    public ChangeFormAllowable(ChangeForm changeForm) {
        //获取换货单状态
        String status = changeForm.getStatus();
        //只有状态为未提交或审核驳回的换货单才可编辑
        this.setAllowEdit(ChangeFormStatusEnum.NEW.name().equals(status) || ChangeFormStatusEnum.REJECT.name().equals(status));
        //只有状态为未提交或审核驳回的换货单才可删除
        this.setAllowDelete(ChangeFormStatusEnum.NEW.name().equals(status) || ChangeFormStatusEnum.REJECT.name().equals(status));
        //只有状态为未提交的换货单才可提交审核
        this.setAllowSubmit(ChangeFormStatusEnum.NEW.name().equals(status));
        //只有状态为已提交（待审核）的换货单才可撤销
        this.setAllowCancel(ChangeFormStatusEnum.WAIT.name().equals(status));
        //只有状态为已提交（待审核）的换货单才可进行审核操作
        this.setAllowAudit(ChangeFormStatusEnum.WAIT.name().equals(status));
    }
}
