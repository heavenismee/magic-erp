package com.hys.app.model.erp.dto;

import com.hys.app.model.erp.enums.StockOperateEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 库存更新
 *
 * @author zs
 * @since 2024-01-15
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StockUpdateDTO {

    @ApiModelProperty(name = "batch_id", value = "批次id")
    private Long batchId;

    @ApiModelProperty(name = "batch_sn", value = "批次sn(目前扣减批次是根据批次id扣减的，如果扣减失败该字段用于展示用)")
    private String batchSn;

    @ApiModelProperty(name = "operate", value = "增加/减少")
    private StockOperateEnum operate;

    @ApiModelProperty(name = "change_num", value = "变化库存数量，必须大于0")
    private Integer changeNum;

}

