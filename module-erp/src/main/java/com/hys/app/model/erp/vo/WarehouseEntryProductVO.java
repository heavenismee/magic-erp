package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.WarehouseEntryProductDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 入库单商品明细详情
 *
 * @author zs
 * @since 2023-12-05 15:30:10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WarehouseEntryProductVO extends WarehouseEntryProductDO {

    @ApiModelProperty(name = "already_num", value = "已入库数量")
    private Integer alreadyNum;

    @ApiModelProperty(name = "contract_price", value = "合同单价")
    private Double contractPrice;
}

