package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 扣减商品库存
 *
 * @author zs
 * @since 2023-12-07
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductStockIncreaseDTO {

    @ApiModelProperty(name = "warehouse_id", value = "仓库id")
    private Long warehouseId;

    @ApiModelProperty(name = "product_id", value = "商品id")
    private Long productId;

    @ApiModelProperty(name = "change_num", value = "变更的库存数量，必须大于0")
    private Integer changeNum;
}

