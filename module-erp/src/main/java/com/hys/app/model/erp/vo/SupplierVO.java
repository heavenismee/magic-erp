package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.SupplierDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 供应商详情
 *
 * @author zs
 * 2023-11-29 14:20:18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SupplierVO extends SupplierDO {

}

