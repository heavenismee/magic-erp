package com.hys.app.model.erp.vo;

import lombok.Data;
import com.alibaba.excel.annotation.ExcelProperty;

/**
 * 收款账户excel导出
 *
 * @author zsong
 * 2024-01-24 14:43:18
 */
@Data
public class CollectingAccountExcelVO {

    @ExcelProperty("账户名称")
    private String name;
    
    @ExcelProperty("编号")
    private String sn;
    
    @ExcelProperty("是否默认")
    private Boolean defaultFlag;
    
    @ExcelProperty("是否启用")
    private Boolean enableFlag;
    
    @ExcelProperty("备注")
    private String remark;
    
}

