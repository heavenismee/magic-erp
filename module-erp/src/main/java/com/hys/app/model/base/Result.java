package com.hys.app.model.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 消息处理结果
 *
 * @author zs
 * @since 2023-12-20
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result {

    /**
     * 是否成功
     */
    private Boolean success;

    /**
     * 备注
     */
    private String remark;

    public static Result ok() {
        return new Result(true, "成功");
    }

    public static Result ok(String remark) {
        return new Result(true, remark);
    }

    public static Result fail(String... errorList) {
        return new Result(false, String.join(",", errorList));
    }

}

