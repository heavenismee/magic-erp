package com.hys.app.model.erp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 订单退货状态枚举
 *
 * @author zs
 * @since 2023-12-14
 */
@Getter
@AllArgsConstructor
public enum OrderReturnStatusEnum {

    /**
     * 未提交
     */
    NotSubmit,
    /**
     * 已提交
     */
    Submit,
    /**
     * 审核通过
     */
    AuditPass,
    /**
     * 审核驳回
     */
    AuditReject,


}
