package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.WarehouseEntryBatchDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 出库单选择的批次
 *
 * @author zs
 * @since 2023-12-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WarehouseOutBatchVO extends WarehouseEntryBatchDO {

    @ApiModelProperty(name = "num", value = "出库数量")
    private Integer num;
}

