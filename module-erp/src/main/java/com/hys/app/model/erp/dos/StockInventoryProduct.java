package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Objects;

/**
 * 库存盘点单产品实体
 *
 * @author dmy
 * 2023-12-05
 */
@TableName("es_stock_inventory_product")
@ApiModel
public class StockInventoryProduct implements Serializable {

    private static final long serialVersionUID = -8999547639817129356L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;
    /**
     * 库存盘点单ID
     */
    @ApiModelProperty(name = "inventory_id", value = "库存盘点单ID")
    private Long inventoryId;
    /**
     * 商品ID
     */
    @ApiModelProperty(name = "goods_id", value = "商品ID")
    private Long goodsId;
    /**
     * 产品ID
     */
    @ApiModelProperty(name = "product_id", value = "产品ID")
    private Long productId;
    /**
     * 产品名称
     */
    @ApiModelProperty(name = "product_name", value = "产品名称")
    private String productName;
    /**
     * 产品编号
     */
    @ApiModelProperty(name = "product_sn", value = "产品编号")
    private String productSn;
    /**
     * 产品规格
     */
    @ApiModelProperty(name = "specification", value = "产品规格")
    private String specification;
    /**
     * 分类名称
     */
    @ApiModelProperty(name = "category_name", value = "分类名称")
    private String categoryName;
    /**
     * 产品单位
     */
    @ApiModelProperty(name = "unit", value = "产品单位")
    private String unit;
    /**
     * 库存数量
     */
    @ApiModelProperty(name = "stock_num", value = "库存数量")
    private Integer stockNum;
    /**
     * 盘点数量
     */
    @ApiModelProperty(name = "inventory_num", value = "盘点数量")
    private Integer inventoryNum;
    /**
     * 差异数量
     */
    @ApiModelProperty(name = "diff_num", value = "差异数量")
    private Integer diffNum;
    /**
     * 备注
     */
    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(Long inventoryId) {
        this.inventoryId = inventoryId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSn() {
        return productSn;
    }

    public void setProductSn(String productSn) {
        this.productSn = productSn;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public Integer getInventoryNum() {
        return inventoryNum;
    }

    public void setInventoryNum(Integer inventoryNum) {
        this.inventoryNum = inventoryNum;
    }

    public Integer getDiffNum() {
        return diffNum;
    }

    public void setDiffNum(Integer diffNum) {
        this.diffNum = diffNum;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StockInventoryProduct that = (StockInventoryProduct) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(inventoryId, that.inventoryId) &&
                Objects.equals(goodsId, that.goodsId) &&
                Objects.equals(productId, that.productId) &&
                Objects.equals(productName, that.productName) &&
                Objects.equals(productSn, that.productSn) &&
                Objects.equals(specification, that.specification) &&
                Objects.equals(categoryName, that.categoryName) &&
                Objects.equals(unit, that.unit) &&
                Objects.equals(stockNum, that.stockNum) &&
                Objects.equals(inventoryNum, that.inventoryNum) &&
                Objects.equals(diffNum, that.diffNum) &&
                Objects.equals(remark, that.remark);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, inventoryId, goodsId, productId, productName, productSn, specification, categoryName, unit, stockNum, inventoryNum, diffNum, remark);
    }

    @Override
    public String toString() {
        return "StockInventoryProduct{" +
                "id=" + id +
                ", inventoryId=" + inventoryId +
                ", goodsId=" + goodsId +
                ", productId=" + productId +
                ", productName='" + productName + '\'' +
                ", productSn='" + productSn + '\'' +
                ", specification='" + specification + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", unit='" + unit + '\'' +
                ", stockNum=" + stockNum +
                ", inventoryNum=" + inventoryNum +
                ", diffNum=" + diffNum +
                ", remark='" + remark + '\'' +
                '}';
    }
}
