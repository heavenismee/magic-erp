package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.WarehouseOutItemDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 出库单统计实体
 *
 * @author dmy
 * 2023-12-05
 */
@ApiModel
public class WarehouseOutStatistics extends WarehouseOutItemDO implements Serializable {

    private static final long serialVersionUID = 8274293006839940916L;

    @ApiModelProperty(name = "dept_name", value = "部门名称")
    private String deptName;

    @ApiModelProperty(name = "warehouse_name", value = "仓库名称")
    private String warehouseName;

    @ApiModelProperty(name = "distribution_name", value = "订单的服务专员")
    private String distributionName;

    @ApiModelProperty(name = "sn", value = "出库单编号")
    private String sn;

    @ApiModelProperty(name = "out_time", value = "出库时间")
    private Long outTime;

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getDistributionName() {
        return distributionName;
    }

    public void setDistributionName(String distributionName) {
        this.distributionName = distributionName;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Long getOutTime() {
        return outTime;
    }

    public void setOutTime(Long outTime) {
        this.outTime = outTime;
    }

    @Override
    public String toString() {
        return "WarehouseOutStatistics{" +
                "deptName='" + deptName + '\'' +
                ", warehouseName='" + warehouseName + '\'' +
                ", distributionName='" + distributionName + '\'' +
                ", sn='" + sn + '\'' +
                ", outTime=" + outTime +
                '}';
    }
}
