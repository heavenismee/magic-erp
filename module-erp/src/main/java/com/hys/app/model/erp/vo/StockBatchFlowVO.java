package com.hys.app.model.erp.vo;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import com.hys.app.model.erp.dos.StockBatchFlowDO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 库存批次流水详情
 *
 * @author zsong
 * 2024-01-16 11:39:01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "库存批次流水详情")
public class StockBatchFlowVO extends StockBatchFlowDO {

}

