package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 * 供应商退货项实体类
 *
 * @author zs
 * @since 2023-12-14 11:12:46
 */
@TableName("erp_supplier_return_item")
@Data
public class SupplierReturnItemDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "supplier_return_id", value = "供应商退货id")
    private Long supplierReturnId;

    @ApiModelProperty(name = "warehouse_entry_item_id", value = "入库单明细id")
    private Long warehouseEntryItemId;

    @ApiModelProperty(name = "warehouse_entry_batch_id", value = "批次id")
    private Long warehouseEntryBatchId;

    @ApiModelProperty(name = "warehouse_entry_batch_sn", value = "批次编号")
    private String warehouseEntryBatchSn;

    @ApiModelProperty(name = "goods_id", value = "商品id")
    private Long goodsId;

    @ApiModelProperty(name = "product_id", value = "产品id")
    private Long productId;

    @ApiModelProperty(name = "product_sn", value = "产品编号")
    private String productSn;

    @ApiModelProperty(name = "product_name", value = "产品名称")
    private String productName;

    @ApiModelProperty(name = "product_specification", value = "产品规格")
    private String productSpecification;

    @ApiModelProperty(name = "product_unit", value = "产品单位")
    private String productUnit;

    @ApiModelProperty(name = "product_barcode", value = "产品条形码")
    private String productBarcode;

    @ApiModelProperty(name = "category_id", value = "产品分类id")
    private Long categoryId;

    @ApiModelProperty(name = "category_name", value = "产品分类名称")
    private String categoryName;

    @ApiModelProperty(name = "product_price", value = "产品单价")
    private Double productPrice;

    @ApiModelProperty(name = "product_cost_price", value = "进货单价")
    private Double productCostPrice;

    @ApiModelProperty(name = "tax_rate", value = "税率")
    private Double taxRate;

    @ApiModelProperty(name = "entry_num", value = "入库数量")
    private Integer entryNum;

    @ApiModelProperty(name = "return_num", value = "退货数量")
    private Integer returnNum;

}
