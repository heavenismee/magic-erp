package com.hys.app.model.erp.dto;

import com.hys.app.model.erp.dos.*;
import com.hys.app.model.erp.enums.StockStatisticsTypeEnum;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 库存统计消息实体
 *
 * @author dmy
 * 2023-12-05
 */
@ApiModel
@Data
public class StockStatisticsMessage implements Serializable {

    private static final long serialVersionUID = 524607318299159659L;

    /**
     * 库存统计操作类型
     */
    private StockStatisticsTypeEnum stockStatisticsTypeEnum;

    /**
     * 库存调拨信息
     */
    private List<StockTransferDO> stockTransferList;

}
