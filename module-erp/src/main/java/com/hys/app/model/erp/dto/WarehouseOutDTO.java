package com.hys.app.model.erp.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hys.app.model.erp.dos.OrderDO;
import com.hys.app.model.erp.dos.OrderItemDO;
import com.hys.app.model.erp.dos.WarehouseEntryBatchDO;
import com.hys.app.model.system.dos.AdminUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * 出库单新增/编辑DTO
 *
 * @author zs
 * @since 2023-12-07 16:50:20
 */
@Data
public class WarehouseOutDTO {

    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "out_time", value = "出库日期")
    @NotNull(message = "出库日期不能为空")
    private Long outTime;

    @ApiModelProperty(name = "warehouse_consignee_id", value = "仓库提货人id")
    private Long warehouseConsigneeId;

    @ApiModelProperty(name = "order_list", value = "订单列表")
    @NotEmpty(message = "订单列表不能为空")
    @Valid
    private List<Order> orderList;

    // ========= 非前端传参 =========

    @JsonIgnore
    @ApiModelProperty(value = "订单", hidden = true)
    private Map<Long, OrderDO> orderMap;

    @JsonIgnore
    @ApiModelProperty(value = "订单项", hidden = true)
    private Map<Long, OrderItemDO> orderItemMap;

    @JsonIgnore
    @ApiModelProperty(value = "批次", hidden = true)
    private Map<Long, WarehouseEntryBatchDO> batchMap;

    @JsonIgnore
    @ApiModelProperty(value = "仓库提货人", hidden = true)
    private AdminUser warehouseConsignee;

    // ========= 内部类 =========

    @Data
    public static class Order {

        @ApiModelProperty(name = "order_id", value = "订单id")
        @NotNull(message = "订单id不能为空")
        private Long orderId;

        @ApiModelProperty(name = "order_item_list", value = "订单项列表")
        @NotEmpty(message = "订单项不能为空")
        @Valid
        private List<OrderItem> orderItemList;
    }

    @Data
    public static class OrderItem {

        @ApiModelProperty(name = "order_item_id", value = "订单项id")
        @NotNull(message = "订单项id不能为空")
        private Long orderItemId;

        @ApiModelProperty(name = "batch_list", value = "批次列表")
        @NotEmpty(message = "批次列表不能为空")
        @Valid
        private List<StockBatch> batchList;
    }

    @Data
    public static class StockBatch {

        @ApiModelProperty(name = "batch_id", value = "批次id")
        @NotNull(message = "批次id不能为空")
        private Long batchId;

        @ApiModelProperty(name = "out_num", value = "要出库的数量")
        @NotNull(message = "要出库的数量不能为空")
        @Min(value = 1, message = "要出库的数量必须大于0")
        private Integer outNum;

    }

}

