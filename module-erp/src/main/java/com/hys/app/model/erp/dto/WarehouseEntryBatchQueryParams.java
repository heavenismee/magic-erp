package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 入库批次查询参数
 *
 * @author zs
 * @since 2023-12-08 11:49:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WarehouseEntryBatchQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "warehouse_entry_id", value = "入库单id")
    private Long warehouseEntryId;

    @ApiModelProperty(name = "warehouse_entry_sn", value = "入库单编号")
    private String warehouseEntrySn;

    @ApiModelProperty(name = "sn", value = "批次编号")
    private String sn;
    
    @ApiModelProperty(name = "warehouse_id", value = "仓库id")
    private Long warehouseId;
    
    @ApiModelProperty(name = "product_id", value = "商品id")
    private Long productId;
    
    @ApiModelProperty(name = "entry_num", value = "入库数量")
    private Integer entryNum;
    
    @ApiModelProperty(name = "entry_price", value = "入库单价")
    private Double entryPrice;
    
    @ApiModelProperty(name = "remain_num", value = "剩余数量")
    private Integer remainNum;
    
    @ApiModelProperty(name = "entry_time", value = "入库时间")
    private Long entryTime;
    
}

