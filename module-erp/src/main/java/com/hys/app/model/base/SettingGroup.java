package com.hys.app.model.base;

/**
 * 系统设置分组枚举
 * Created by kingapex on 2018/3/19.
 *
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2018/3/19
 */
public enum SettingGroup {

    /**
     * 站点设置
     */
    SITE,

    /**
     * 商品设置
     */
    GOODS
}
