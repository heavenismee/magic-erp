package com.hys.app.model.erp.dto;

import com.hys.app.model.erp.dos.WarehouseEntryBatchDO;
import com.hys.app.model.erp.dos.WarehouseEntryDO;
import com.hys.app.model.system.dos.AdminUser;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * 供应商退货新增/编辑DTO
 *
 * @author zs
 * @since 2023-12-14 11:12:46
 */
@Data
public class SupplierReturnDTO {

    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "dept_id", value = "部门id")
    @NotNull(message = "部门id不能为空")
    private Long deptId;
    
    @ApiModelProperty(name = "warehouse_entry_id", value = "入库单id")
    @NotNull(message = "入库单id不能为空")
    private Long warehouseEntryId;
    
    @ApiModelProperty(name = "handle_by_id", value = "经手人id")
    @NotNull(message = "经手人id不能为空")
    private Long handleById;
    
    @ApiModelProperty(name = "attachment", value = "附件")
    private String attachment;
    
    @ApiModelProperty(name = "return_time", value = "退货时间")
    @NotNull(message = "退货时间不能为空")
    private Long returnTime;

    @ApiModelProperty(name = "item_list", value = "退货明细")
    @NotEmpty(message = "退货明细不能为空")
    @Valid
    private List<SupplierReturnItemDTO> itemList;


    @JsonIgnore
    private WarehouseEntryDO warehouseEntryDO;

    @JsonIgnore
    private Map<Long, WarehouseEntryBatchDO> batchMap;

    @JsonIgnore
    private AdminUser handleBy;

}

