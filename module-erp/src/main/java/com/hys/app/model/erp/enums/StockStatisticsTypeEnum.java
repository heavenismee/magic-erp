package com.hys.app.model.erp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 库存统计操作类型枚举
 *
 * @author dmy
 * @since 2023-12-05
 */
@Getter
@AllArgsConstructor
public enum StockStatisticsTypeEnum {

    /**
     * 新增
     */
    ADD,

    /**
     * 出库
     */
    OUT_STOCK,

    /**
     * 供应商退货
     */
    SUPPLIER_RETURN,

    /**
     * 订单退货
     */
    ORDER_RETURN,

    /**
     * 库存调拨
     */
    STOCK_TRANSFER,

    /**
     * 库存报损
     */
    STOCK_DAMAGE,

    /**
     * 换货
     */
    CHANGE_FORM,

    /**
     * 库存预警
     */
    STOCK_WARNING
}
