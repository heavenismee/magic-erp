package com.hys.app.model.erp.dto;

import com.hys.app.model.erp.enums.NoBusinessTypeEnum;
import com.hys.app.model.erp.enums.NoSeqResetTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 编号生成规则新增/编辑DTO
 *
 * @author zs
 * 2023-12-01 11:37:56
 */
@Data
public class NoGenerateRuleDTO {

    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "type", value = "业务类型")
    @NotNull(message = "业务类型不能为空")
    private NoBusinessTypeEnum type;
    
    @ApiModelProperty(name = "name", value = "规则名称")
    @NotNull(message = "规则名称不能为空")
    private String name;
    
    @ApiModelProperty(name = "seq_generate_type", value = "顺序号的重置规则")
    @NotNull(message = "顺序号的生成规则不能为空")
    private NoSeqResetTypeEnum seqGenerateType;
    
    @ApiModelProperty(name = "seq_begin_number", value = "顺序号的起始值")
    @NotNull(message = "顺序号的起始值不能为空")
    @Min(value = 0, message = "顺序号的起始值最小为0")
    private Long seqBeginNumber;
    
    @ApiModelProperty(name = "open_flag", value = "是否启用该规则")
    @NotNull(message = "是否启用该规则不能为空")
    private Boolean openFlag;
    
    @ApiModelProperty(name = "item_list", value = "规则项列表")
    @NotEmpty(message = "规则项列表不能为空")
    @Valid
    private List<NoGenerateRuleItem> itemList;

}

