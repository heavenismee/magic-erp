package com.hys.app.model.system.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import com.hys.app.model.system.enums.TaskStatusEnum;
import com.hys.app.model.system.enums.TaskSubTypeEnum;
import com.hys.app.model.system.enums.TaskTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;

/**
 * 任务 DO
 *
 * @author zsong
 * 2024-01-23 10:43:35
 */
@TableName("erp_task")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class TaskDO extends BaseDO {

    private static final long serialVersionUID = 1L;
    
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(name = "name", value = "名称")
    private String name;
    
    @ApiModelProperty(name = "type", value = "类型")
    private TaskTypeEnum type;
    
    @ApiModelProperty(name = "sub_type", value = "子类型")
    private TaskSubTypeEnum subType;
    
    @ApiModelProperty(name = "params", value = "执行参数")
    private String params;
    
    @ApiModelProperty(name = "status", value = "状态")
    private TaskStatusEnum status;
    
    @ApiModelProperty(name = "result", value = "执行结果")
    private String result;
    
    @ApiModelProperty(name = "thread_id", value = "线程id")
    private String threadId;
    
}
