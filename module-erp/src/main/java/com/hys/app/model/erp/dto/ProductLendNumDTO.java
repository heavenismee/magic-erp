package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 商品借出数量DTO
 *
 * @author dmy
 * 2023-12-05
 */
@ApiModel
public class ProductLendNumDTO implements Serializable {

    private static final long serialVersionUID = -5400941655560721961L;

    /**
     * 入库单编号
     */
    @ApiModelProperty(name = "stock_sn", value = "入库单编号", required = true)
    @NotEmpty(message = "入库单编号不能为空")
    private String stockSn;
    /**
     * 产品ID
     */
    @ApiModelProperty(name = "product_id", value = "产品ID", required = true)
    @NotNull(message = "产品ID不能为空")
    private Long productId;
    /**
     * 借出数量
     */
    @ApiModelProperty(name = "lend_num", value = "借出数量")
    private Integer lendNum;
    /**
     * 产品借出数量说明
     */
    @ApiModelProperty(name = "remark", value = "产品借出数量说明")
    private String remark;

    public String getStockSn() {
        return stockSn;
    }

    public void setStockSn(String stockSn) {
        this.stockSn = stockSn;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getLendNum() {
        return lendNum;
    }

    public void setLendNum(Integer lendNum) {
        this.lendNum = lendNum;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "ProductLendNumDTO{" +
                "stockSn='" + stockSn + '\'' +
                ", productId=" + productId +
                ", lendNum=" + lendNum +
                ", remark='" + remark + '\'' +
                '}';
    }
}
