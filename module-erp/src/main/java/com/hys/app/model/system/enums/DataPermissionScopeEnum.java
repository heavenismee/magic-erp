package com.hys.app.model.system.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 数据权限范围枚举类，用于实现数据级别的权限
 *
 * @author zs
 * @since 2023-12-01
 */
@Getter
@AllArgsConstructor
public enum DataPermissionScopeEnum {

    /**
     * 全部数据权限
     */
    ALL,
    /**
     * 指定部门数据权限
     */
    DEPT_CUSTOM

}
