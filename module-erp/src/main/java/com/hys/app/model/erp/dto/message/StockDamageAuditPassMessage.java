package com.hys.app.model.erp.dto.message;

import com.hys.app.model.erp.dos.StockDamageReport;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 库存报损单单审核通过消息
 *
 * @author zs
 * @since 2024-01-09
 */
@Data
public class StockDamageAuditPassMessage implements Serializable {

    private static final long serialVersionUID = 524607318299159659L;

    private List<StockDamageReport> list;

}
