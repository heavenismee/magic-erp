package com.hys.app.model.erp.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hys.app.model.erp.enums.SexEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 会员 新增|编辑 请求参数
 *
 * @author zsong
 * 2024-01-23 09:28:16
 */
@Data
@ApiModel(value = "会员 新增|编辑 请求参数")
public class MemberDTO {

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "sn", value = "编号")
    @NotBlank(message = "编号不能为空")
    private String sn;
    
    @ApiModelProperty(name = "name", value = "姓名")
    private String name;
    
    @ApiModelProperty(name = "sex", value = "性别")
    private SexEnum sex;
    
    @ApiModelProperty(name = "mobile", value = "手机号")
    private String mobile;
    
    @ApiModelProperty(name = "email", value = "邮箱")
    private String email;
    
    @ApiModelProperty(name = "birthday", value = "出生日期")
    private Long birthday;
    
    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;

}

