package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.WarehouseEntryProductDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 入库单统计信息实体
 *
 * @author dmy
 * 2023-12-05
 */
@ApiModel
public class WarehouseEntryStatistics extends WarehouseEntryProductDO implements Serializable {

    private static final long serialVersionUID = -82299563560196011L;

    @ApiModelProperty(name = "dept_name", value = "部门名称")
    private String deptName;

    @ApiModelProperty(name = "supplier_name", value = "供应商名称")
    private String supplierName;

    @ApiModelProperty(name = "warehouse_name", value = "仓库名称")
    private String warehouseName;

    @ApiModelProperty(name = "entry_sn", value = "入库单编号")
    private String entrySn;

    @ApiModelProperty(name = "entry_time", value = "入库时间")
    private Long entryTime;

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getEntrySn() {
        return entrySn;
    }

    public void setEntrySn(String entrySn) {
        this.entrySn = entrySn;
    }

    public Long getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(Long entryTime) {
        this.entryTime = entryTime;
    }

    @Override
    public String toString() {
        return "WarehouseEntryStatistics{" +
                "deptName='" + deptName + '\'' +
                ", supplierName='" + supplierName + '\'' +
                ", warehouseName='" + warehouseName + '\'' +
                ", entrySn='" + entrySn + '\'' +
                ", entryTime=" + entryTime +
                '}';
    }
}
