package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 采购计划查询参数实体
 * @Author dmy
 * 2023-12-05
 */
@ApiModel
public class ProcurementPlanQueryParam extends BaseQueryParam implements Serializable {

    private static final long serialVersionUID = 9079454040194428187L;

    @ApiModelProperty(name = "keyword", value = "关键字")
    private String keyword;

    @ApiModelProperty(name = "sn", value = "采购计划编号")
    private String sn;

    @ApiModelProperty(name = "dept_id", value = "部门ID")
    private Long deptId;

    @ApiModelProperty(name = "start_time", value = "开始时间")
    private Long startTime;

    @ApiModelProperty(name = "end_time", value = "结束时间")
    private Long endTime;

    @ApiModelProperty(name = "formation_person_id", value = "编制人员ID")
    private Long formationPersonId;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Long getFormationPersonId() {
        return formationPersonId;
    }

    public void setFormationPersonId(Long formationPersonId) {
        this.formationPersonId = formationPersonId;
    }

    @Override
    public String toString() {
        return "ProcurementPlanQueryParam{" +
                "keyword='" + keyword + '\'' +
                ", sn='" + sn + '\'' +
                ", deptId=" + deptId +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", formationPersonId=" + formationPersonId +
                '}';
    }
}
