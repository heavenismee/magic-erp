package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 岗位查询参数
 *
 * @author zs
 * 2023-11-29 17:15:22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class PostQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "keyword", value = "关键字")
    private String keyword;

    @ApiModelProperty(name = "name", value = "岗位名称")
    private String name;

    @ApiModelProperty(name = "sn", value = "岗位编号")
    private String sn;
    
    @ApiModelProperty(name = "sort", value = "顺序")
    private Integer sort;
    
    @ApiModelProperty(name = "description", value = "岗位描述")
    private String description;
    
    @ApiModelProperty(name = "members", value = "岗位成员")
    private String members;
    
}

