package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.SupplierSettlementItemDO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 供应商结算单明细VO
 *
 * @author zs
 * @since 2023-12-15 14:09:20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SupplierSettlementItemVO extends SupplierSettlementItemDO {

}

