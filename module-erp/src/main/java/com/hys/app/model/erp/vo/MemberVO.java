package com.hys.app.model.erp.vo;

import io.swagger.annotations.ApiModel;
import com.hys.app.model.erp.dos.MemberDO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 会员详情
 *
 * @author zsong
 * 2024-01-23 09:28:16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "会员详情")
public class MemberVO extends MemberDO {

}

