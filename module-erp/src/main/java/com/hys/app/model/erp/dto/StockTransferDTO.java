package com.hys.app.model.erp.dto;

import com.hys.app.model.erp.dos.WarehouseDO;
import com.hys.app.model.erp.dos.WarehouseEntryBatchDO;
import com.hys.app.model.system.dos.AdminUser;
import com.hys.app.model.system.dos.DeptDO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.AssertFalse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * 库存调拨新增/编辑DTO
 *
 * @author zs
 * @since 2023-12-12 11:59:06
 */
@Data
public class StockTransferDTO {

    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "out_dept_id", value = "调出方部门id")
    @NotNull(message = "调出方部门id不能为空")
    private Long outDeptId;

    @ApiModelProperty(name = "out_warehouse_id", value = "调出方仓库id")
    @NotNull(message = "调出方仓库id不能为空")
    private Long outWarehouseId;

    @ApiModelProperty(name = "out_handled_by_id", value = "调出方仓库经手人id")
    @NotNull(message = "调出方仓库经手人id不能为空")
    private Long outHandledById;

    @ApiModelProperty(name = "in_dept_id", value = "调入方部门id")
    @NotNull(message = "调入方部门id不能为空")
    private Long inDeptId;

    @ApiModelProperty(name = "in_warehouse_id", value = "调入方仓库id")
    @NotNull(message = "调入方仓库id不能为空")
    private Long inWarehouseId;

    @ApiModelProperty(name = "in_handled_by_id", value = "调入方仓库经手人id")
    private Long inHandledById;

    @ApiModelProperty(name = "attachment", value = "附件")
    @NotEmpty(message = "附件不能为空")
    private String attachment;

    @ApiModelProperty(name = "transfer_time", value = "调拨时间")
    @NotNull(message = "调拨时间不能为空")
    private Long transferTime;

    @ApiModelProperty(name = "product_list", value = "商品列表")
    @NotEmpty(message = "商品列表不能为空")
    @Valid
    private List<StockTransferProductDTO> productList;

    @AssertFalse(message = "调出方仓库和调入方仓库不能相同")
    private boolean isSameWarehouse() {
        return outWarehouseId.equals(inWarehouseId);
    }


    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private Map<Long, WarehouseEntryBatchDO> batchMap;

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private DeptDO outDeptDO;

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private DeptDO inDeptDO;

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private WarehouseDO outWarehouseDO;

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private WarehouseDO inWarehouseDO;

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private AdminUser outHandleBy;

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private AdminUser inHandleBy;

}

