package com.hys.app.model.erp.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 出库单商品明细详情
 *
 * @author zs
 * @since 2023-12-07 17:06:32
 */
@Data
public class WarehouseOutOrderVO {

    @ApiModelProperty(name = "order_id", value = "订单id")
    private Long orderId;

    @ApiModelProperty(name = "order_sn", value = "订单编号")
    private String orderSn;

    @ApiModelProperty(name = "product_list", value = "商品列表")
    private List<WarehouseOutItemVO> productList;

}

