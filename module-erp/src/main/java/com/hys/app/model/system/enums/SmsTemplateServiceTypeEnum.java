package com.hys.app.model.system.enums;

/**
 * 短信模板业务类型枚举
 * @author zhangsong
 * @version v1.0
 * @since v7.3.0
 * 2021-01-26 16:04:23
 */
public enum SmsTemplateServiceTypeEnum {

    /**
     * 商户消息模板
     */
    SHOP("商户消息模板"),

    /**
     * 会员消息模板
     */
    MEMBER("会员消息模板");

    private String description;

    SmsTemplateServiceTypeEnum(String description) {
        this.description = description;
    }

    public String description() {
        return description;
    }
    public String value(){
        return this.name();
    }
}
