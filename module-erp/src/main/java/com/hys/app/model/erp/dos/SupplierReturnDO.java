package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import com.hys.app.model.erp.enums.SupplierReturnStatusEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;


/**
 * 供应商退货实体类
 *
 * @author zs
 * @since 2023-12-14 11:12:46
 */
@TableName("erp_supplier_return")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SupplierReturnDO extends BaseDO {

    private static final long serialVersionUID = 1L;
    
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "sn", value = "编号")
    private String sn;
    
    @ApiModelProperty(name = "dept_id", value = "部门id")
    private Long deptId;
    
    @ApiModelProperty(name = "warehouse_entry_id", value = "入库单id")
    private Long warehouseEntryId;
    
    @ApiModelProperty(name = "warehouse_entry_sn", value = "入库单编号")
    private String warehouseEntrySn;
    
    @ApiModelProperty(name = "warehouse_id", value = "仓库id")
    private Long warehouseId;
    
    @ApiModelProperty(name = "supplier_id", value = "供应商id")
    private Long supplierId;

    @ApiModelProperty(name = "supplier_sn", value = "供应商编号")
    private String supplierSn;

    @ApiModelProperty(name = "dept_name", value = "部门名称")
    private String deptName;

    @ApiModelProperty(name = "warehouse_name", value = "仓库名称")
    private String warehouseName;

    @ApiModelProperty(name = "supplier_name", value = "供应商名称")
    private String supplierName;
    
    @ApiModelProperty(name = "contract_id", value = "合同id")
    private Long contractId;

    @ApiModelProperty(name = "contract_sn", value = "合同编号")
    private String contractSn;

    @ApiModelProperty(name = "handle_by_id", value = "经手人id")
    private Long handleById;

    @ApiModelProperty(name = "handle_by_name", value = "经手人名称")
    private String handleByName;

    @ApiModelProperty(name = "attachment", value = "附件")
    private String attachment;
    
    @ApiModelProperty(name = "return_time", value = "退货时间")
    private Long returnTime;
    
    @ApiModelProperty(name = "status", value = "状态")
    private SupplierReturnStatusEnum status;
    
    @ApiModelProperty(name = "audit_by", value = "审核人")
    private String auditBy;

    @ApiModelProperty(name = "audit_remark", value = "审核备注")
    private String auditRemark;

}
