package com.hys.app.model.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Set;


@ApiModel(description = "管理后台 - 部门列表 Request VO")
@Data
public class DeptQueryParams {

    @ApiModelProperty(value = "部门名称，模糊匹配")
    private String name;

    @ApiModelProperty(value = "只查询当前用户有权限的部门")
    private Boolean onlyPermission;

    @ApiModelProperty(value = "只查询当前用户有权限的部门", hidden = true)
    private Set<Long> deptIds;

}
