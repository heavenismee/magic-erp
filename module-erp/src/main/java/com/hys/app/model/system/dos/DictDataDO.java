package com.hys.app.model.system.dos;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 字典数据表
 *
 * @author zs
 * @since 2023-11-28
 */
@TableName("system_dict_data")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class DictDataDO extends BaseDO {

    /**
     * 字典数据编号
     */
    @TableId
    private Long id;
    /**
     * 字典排序
     */
    private Integer sort;
    /**
     * 字典标签
     */
    private String label;
    /**
     * 字典值
     */
    private String value;
    /**
     * 字典类型 {@link DictDataDO#getDictType()}
     */
    private String dictType;
    /**
     * 备注
     */
    private String remark;
    /**
     * 是否为默认值
     */
    private Boolean defaultFlag;

}
