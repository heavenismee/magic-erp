package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hys.app.model.erp.enums.StockChangeSourceEnum;
import com.hys.app.model.erp.enums.StockOperateEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 库存批次流水 DO
 *
 * @author zsong
 * 2024-01-16 11:39:01
 */
@TableName("erp_stock_batch_flow")
@Data
public class StockBatchFlowDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(name = "warehouse_entry_id", value = "入库单id")
    private Long warehouseEntryId;

    @ApiModelProperty(name = "warehouse_entry_sn", value = "入库单编号")
    private String warehouseEntrySn;

    @ApiModelProperty(name = "batch_id", value = "批次id")
    private Long batchId;

    @ApiModelProperty(name = "batch_sn", value = "批次编号")
    private String batchSn;

    @ApiModelProperty(name = "dept_id", value = "部门id")
    private Long deptId;

    @ApiModelProperty(name = "warehouse_id", value = "仓库id")
    private Long warehouseId;

    @ApiModelProperty(name = "goods_id", value = "商品id")
    private Long goodsId;

    @ApiModelProperty(name = "product_id", value = "产品id")
    private Long productId;

    @ApiModelProperty(name = "product_sn", value = "产品编号")
    private String productSn;

    @ApiModelProperty(name = "product_name", value = "产品名称")
    private String productName;

    @ApiModelProperty(name = "product_specification", value = "产品规格")
    private String productSpecification;

    @ApiModelProperty(name = "product_unit", value = "产品单位")
    private String productUnit;

    @ApiModelProperty(name = "product_barcode", value = "产品条形码")
    private String productBarcode;

    @ApiModelProperty(name = "category_id", value = "产品分类id")
    private Long categoryId;

    @ApiModelProperty(name = "category_name", value = "产品分类名称")
    private String categoryName;

    @ApiModelProperty(name = "product_price", value = "产品单价")
    private Double productPrice;

    @ApiModelProperty(name = "product_cost_price", value = "产品进货单价")
    private Double productCostPrice;

    @ApiModelProperty(name = "tax_rate", value = "税率")
    private Double taxRate;

    @ApiModelProperty(name = "change_type", value = "增加/减少")
    private StockOperateEnum changeType;

    @ApiModelProperty(name = "change_num", value = "库存变更数量")
    private Integer changeNum;

    @ApiModelProperty(name = "batch_remain_num", value = "批次剩余库存数量")
    private Integer batchRemainNum;

    @ApiModelProperty(name = "source_type", value = "库存变更来源")
    private StockChangeSourceEnum sourceType;

    @ApiModelProperty(name = "source_sn", value = "库存变更来源单号")
    private String sourceSn;

    @ApiModelProperty(name = "create_time", value = "库存变更时间")
    private Long createTime;

}
