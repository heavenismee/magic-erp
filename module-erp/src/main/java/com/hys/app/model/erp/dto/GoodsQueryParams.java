package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 商品查询参数
 *
 * @author zs
 * 2023-12-26 15:56:58
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class GoodsQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "keyword", value = "关键字")
    private String keyword;

    @ApiModelProperty(name = "sn", value = "商品编号")
    private String sn;
    
    @ApiModelProperty(name = "name", value = "商品名称")
    private String name;
    
    @ApiModelProperty(name = "barcode", value = "条形码")
    private String barcode;
    
    @ApiModelProperty(name = "category_id", value = "商品分类id")
    private Long categoryId;
    
}

