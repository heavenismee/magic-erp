package com.hys.app.model.erp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 库存调拨单状态枚举
 *
 * @author zs
 * @since 2023-12-12
 */
@Getter
@AllArgsConstructor
public enum StockTransferStatusEnum {

    /**
     * 未提交
     */
    NotSubmit,
    /**
     * 已提交
     */
    Submit,
    /**
     * 已确认
     */
    Confirm,
    /**
     * 审核驳回
     */
    AuditReject,


}
