package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 调拨单商品明细新增/编辑DTO
 *
 * @author zs
 * @since 2023-12-12 16:34:15
 */
@Data
public class StockTransferProductDTO {

    @ApiModelProperty(name = "warehouse_entry_batch_id", value = "入库批次id")
    @NotNull(message = "入库批次id不能为空")
    private Long warehouseEntryBatchId;

    @ApiModelProperty(name = "num", value = "调拨数量")
    @NotNull(message = "调拨数量不能为空")
    @Min(value = 1, message = "调拨数量最小为1")
    private Integer num;
    
}

