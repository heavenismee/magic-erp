package com.hys.app.model.erp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 财务支出类型枚举
 *
 * @author zs
 * @since 2024-03-18
 */
@Getter
@AllArgsConstructor
public enum FinanceExpandTypeEnum {

    /**
     * 采购支出
     */
    Purchase,
    /**
     * 物流支出
     */
    Logistics,
    /**
     * 营销费用支出
     */
    Promotion,
    /**
     * 订单退货支出
     */
    OrderReturn,
    /**
     * 其他支出
     */
    Other,

}
