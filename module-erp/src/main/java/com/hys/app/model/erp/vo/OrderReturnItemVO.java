package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.OrderReturnItemDO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 订单退货项VO
 *
 * @author zs
 * @since 2023-12-14 15:42:07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class OrderReturnItemVO extends OrderReturnItemDO {

}

