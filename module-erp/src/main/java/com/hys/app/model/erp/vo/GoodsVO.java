package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.GoodsDO;
import com.hys.app.model.goods.dos.GoodsGalleryDO;
import com.hys.app.model.goods.vo.GoodsParamsGroupVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 商品VO
 *
 * @author zs
 * 2023-12-26 15:56:58
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class GoodsVO extends GoodsDO {

    @ApiModelProperty(name = "category_name", value = "分类名称")
    private String categoryName;

    @ApiModelProperty(name = "category_name", value = "分类id集合")
    private Long[] categoryIds;

    @ApiModelProperty(name = "brand_name", value = "品牌名称")
    private String brandName;

    @ApiModelProperty(name = "product_list", value = "产品列表")
    private List<ProductVO> productList;

    @ApiModelProperty(name = "image_list", value = "商品相册")
    private List<GoodsGalleryDO> imageList;

    @ApiModelProperty(name = "params_list", value = "商品参数")
    private List<GoodsParamsGroupVO> paramsList;

}

