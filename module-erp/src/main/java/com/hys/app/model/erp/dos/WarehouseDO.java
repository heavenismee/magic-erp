package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;


/**
 * 仓库实体类
 *
 * @author zs
 * 2023-11-30 16:35:16
 */
@TableName("erp_warehouse")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WarehouseDO extends BaseDO {

    private static final long serialVersionUID = 1L;
    
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "name", value = "仓库名称")
    private String name;
    
    @ApiModelProperty(name = "sn", value = "仓库编号")
    private String sn;
    
    @ApiModelProperty(name = "admin_id", value = "管理员id")
    private Long adminId;
    
    @ApiModelProperty(name = "description", value = "描述")
    private String description;
    
    @ApiModelProperty(name = "dept_id", value = "部门id")
    private Long deptId;

    @TableLogic
    @ApiModelProperty(name = "deleted", value = "是否删除")
    private Boolean deleted;
}
