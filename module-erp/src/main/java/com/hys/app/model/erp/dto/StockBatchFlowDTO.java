package com.hys.app.model.erp.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 库存批次流水 新增/编辑 请求参数
 *
 * @author zsong
 * 2024-01-16 11:39:01
 */
@Data
@ApiModel(value = "库存批次流水 新增|编辑 请求参数")
public class StockBatchFlowDTO {

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "warehouse_entry_id", value = "入库单id")
    @NotNull(message = "入库单id不能为空")
    private Long warehouseEntryId;
    
    @ApiModelProperty(name = "warehouse_entry_sn", value = "入库单编号")
    @NotBlank(message = "入库单编号不能为空")
    private String warehouseEntrySn;
    
    @ApiModelProperty(name = "batch_id", value = "批次id")
    @NotNull(message = "批次id不能为空")
    private Long batchId;
    
    @ApiModelProperty(name = "batch_sn", value = "批次编号")
    @NotBlank(message = "批次编号不能为空")
    private String batchSn;
    
    @ApiModelProperty(name = "warehouse_id", value = "仓库id")
    @NotNull(message = "仓库id不能为空")
    private Long warehouseId;
    
    @ApiModelProperty(name = "goods_id", value = "商品id")
    @NotNull(message = "商品id不能为空")
    private Long goodsId;
    
    @ApiModelProperty(name = "product_id", value = "产品id")
    @NotNull(message = "产品id不能为空")
    private Long productId;
    
    @ApiModelProperty(name = "product_sn", value = "产品编号")
    @NotBlank(message = "产品编号不能为空")
    private String productSn;
    
    @ApiModelProperty(name = "product_name", value = "产品名称")
    @NotBlank(message = "产品名称不能为空")
    private String productName;
    
    @ApiModelProperty(name = "product_specification", value = "产品规格")
    @NotBlank(message = "产品规格不能为空")
    private String productSpecification;
    
    @ApiModelProperty(name = "product_unit", value = "产品单位")
    @NotBlank(message = "产品单位不能为空")
    private String productUnit;
    
    @ApiModelProperty(name = "product_barcode", value = "产品条形码")
    @NotBlank(message = "产品条形码不能为空")
    private String productBarcode;
    
    @ApiModelProperty(name = "category_id", value = "产品分类id")
    @NotNull(message = "产品分类id不能为空")
    private Long categoryId;
    
    @ApiModelProperty(name = "category_name", value = "产品分类名称")
    @NotBlank(message = "产品分类名称不能为空")
    private String categoryName;
    
    @ApiModelProperty(name = "product_price", value = "产品单价")
    @NotNull(message = "产品单价不能为空")
    private Double productPrice;
    
    @ApiModelProperty(name = "product_cost_price", value = "产品进货单价")
    @NotNull(message = "产品进货单价不能为空")
    private Double productCostPrice;
    
    @ApiModelProperty(name = "tax_rate", value = "税率")
    @NotNull(message = "税率不能为空")
    private Double taxRate;
    
    @ApiModelProperty(name = "change_num", value = "库存变更数量")
    @NotNull(message = "库存变更数量不能为空")
    private Integer changeNum;
    
    @ApiModelProperty(name = "batch_remain_num", value = "批次剩余库存数量")
    @NotNull(message = "批次剩余库存数量不能为空")
    private Integer batchRemainNum;
    
    @ApiModelProperty(name = "source_type", value = "库存变更来源")
    @NotBlank(message = "库存变更来源不能为空")
    private String sourceType;
    
    @ApiModelProperty(name = "source_sn", value = "库存变更来源单号")
    @NotBlank(message = "库存变更来源单号不能为空")
    private String sourceSn;
    
}

