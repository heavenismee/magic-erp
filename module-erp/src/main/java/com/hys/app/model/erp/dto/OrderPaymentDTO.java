package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

/**
 * 订单支付明细 新增|编辑 请求参数
 *
 * @author zsong
 * 2024-01-24 17:00:32
 */
@Data
@ApiModel(value = "订单支付明细 新增|编辑 请求参数")
public class OrderPaymentDTO {

    @ApiModelProperty(name = "collecting_account_id", value = "收款账户id")
    @NotNull(message = "收款账户id不能为空")
    private Long collectingAccountId;

    @ApiModelProperty(name = "price", value = "支付金额")
    @NotNull(message = "支付金额不能为空")
    @DecimalMin(value = "0.01", message = "支付金额必须大于0")
    private Double price;

    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;

}

