package com.hys.app.model.erp.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 产品规格
 *
 * @author zs
 * @since 2024-01-22
 */
@Data
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ProductSpec {

    @ApiModelProperty(name = "spec_id", value = "规格项id")
    private Long specId;

    @ApiModelProperty(name = "spec_name", value = "规格名称")
    private String specName;

    @ApiModelProperty(name = "spec_value_id", value = "规格值id")
    private Long specValueId;

    @ApiModelProperty(value = "规格值名字")
    private String specValue;

}

