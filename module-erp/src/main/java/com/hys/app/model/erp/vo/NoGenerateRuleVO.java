package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.NoGenerateRuleDO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 编号生成规则详情
 *
 * @author zs
 * 2023-12-01 11:37:56
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class NoGenerateRuleVO extends NoGenerateRuleDO {

}

