package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;
import com.hys.app.model.erp.dto.ProductDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * 商品实体类
 *
 * @author zs
 * 2023-12-26 15:56:58
 */
@TableName("erp_goods")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class GoodsDO extends BaseDO {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "sn", value = "商品编号")
    private String sn;

    @ApiModelProperty(name = "name", value = "商品名称")
    private String name;

    @ApiModelProperty(name = "category_id", value = "商品分类id")
    private Long categoryId;

    @ApiModelProperty(name = "brand_id", value = "品牌id")
    private Long brandId;

    @ApiModelProperty(name = "market_enable", value = "是否上架 true上架")
    private Boolean marketEnable;

    @ApiModelProperty(name = "description", value = "商品详情")
    private String description;

    @ApiModelProperty(name = "have_spec", value = "是否有规格")
    private Boolean haveSpec;

    @ApiModelProperty(name = "image", value = "图片")
    private String image;

    @ApiModelProperty(name = "freight_template_id", value = "运费模板id（0代表包邮）")
    private Long freightTemplateId;

    @ApiModelProperty(name = "video", value = "商品视频")
    private String video;

    // =========== 以下是使用价格最低的sku的数据 ===========

    @ApiModelProperty(name = "price", value = "销售价格")
    private Double price;

    @ApiModelProperty(name = "cost_price", value = "成本价格")
    private Double costPrice;

    @ApiModelProperty(name = "mkt_price", value = "市场价格")
    private Double mktPrice;

    public void setPropertiesFromProduct(ProductDTO minPriceProduct){
        this.setPrice(minPriceProduct.getPrice());
        this.setCostPrice(minPriceProduct.getCostPrice());
        this.setMktPrice(minPriceProduct.getMktPrice());
    }
}
