package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.ProcurementContract;
import com.hys.app.model.erp.enums.ContractStatusEnum;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 合同操作类
 *
 * @author dmy
 * 2023-12-06
 */
public class ContractAllowable implements Serializable {

    private static final long serialVersionUID = 7059611108938091554L;

    @ApiModelProperty(name = "allow_edit", value = "是否允许编辑")
    private Boolean allowEdit;

    @ApiModelProperty(name = "allow_delete", value = "是否允许删除")
    private Boolean allowDelete;

    @ApiModelProperty(name = "allow_execute", value = "是否允许执行")
    private Boolean allowExecute;

    @ApiModelProperty(name = "allow_close", value = "是否允许关闭")
    private Boolean allowClose;

    public Boolean getAllowEdit() {
        return allowEdit;
    }

    public void setAllowEdit(Boolean allowEdit) {
        this.allowEdit = allowEdit;
    }

    public Boolean getAllowDelete() {
        return allowDelete;
    }

    public void setAllowDelete(Boolean allowDelete) {
        this.allowDelete = allowDelete;
    }

    public Boolean getAllowExecute() {
        return allowExecute;
    }

    public void setAllowExecute(Boolean allowExecute) {
        this.allowExecute = allowExecute;
    }

    public Boolean getAllowClose() {
        return allowClose;
    }

    public void setAllowClose(Boolean allowClose) {
        this.allowClose = allowClose;
    }

    @Override
    public String toString() {
        return "ContractAllowable{" +
                "allowEdit=" + allowEdit +
                ", allowDelete=" + allowDelete +
                ", allowExecute=" + allowExecute +
                ", allowClose=" + allowClose +
                '}';
    }

    public ContractAllowable(ProcurementContract contract) {
        //获取合同状态
        String status = contract.getContractStatus();
        //只有状态为未执行的合同才可编辑
        this.setAllowEdit(ContractStatusEnum.NEW.name().equals(status));
        //只有状态为未执行或已关闭的合同才可删除
        this.setAllowDelete(ContractStatusEnum.NEW.name().equals(status) || ContractStatusEnum.CLOSED.name().equals(status));
        //只有状态为未执行的合同才可执行
        this.setAllowExecute(ContractStatusEnum.NEW.name().equals(status));
        //只有状态为已执行的合同才可关闭
        this.setAllowClose(ContractStatusEnum.EXECUTING.name().equals(status));
    }
}
