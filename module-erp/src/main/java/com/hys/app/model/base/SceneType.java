package com.hys.app.model.base;

/**
 * 图片验证码业务类型
 *
 * @author zh
 * @version v2.0
 * @since v7.0
 * 2018年3月19日 下午4:35:32
 */
public enum SceneType {
    //验证码登录
    LOGIN("验证码登录");

    private String description;

    SceneType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }


}
