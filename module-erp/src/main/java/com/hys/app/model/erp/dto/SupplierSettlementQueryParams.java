package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 供应商结算单查询参数
 *
 * @author zs
 * @since 2023-12-15 14:09:09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SupplierSettlementQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "sn", value = "结算单编号")
    private String sn;
    
    @ApiModelProperty(name = "supplier_id", value = "供应商id")
    private Long supplierId;
    
    @ApiModelProperty(name = "start_time", value = "结算开始时间")
    private Long startTime;
    
    @ApiModelProperty(name = "end_time", value = "结算结束时间")
    private Long endTime;
    
    @ApiModelProperty(name = "total_price", value = "合计金额")
    private Double totalPrice;
    
    @ApiModelProperty(name = "status", value = "状态")
    private String status;
    
}

