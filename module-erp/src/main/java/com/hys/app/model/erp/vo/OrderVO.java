package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.OrderDO;
import com.hys.app.model.erp.dos.OrderPaymentDO;
import com.hys.app.model.erp.dos.StoreDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 订单详情
 *
 * @author zsong
 * 2024-01-24 15:58:31
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "订单详情")
public class OrderVO extends OrderDO {

    @ApiModelProperty(name = "item_list", value = "订单项集合")
    private List<OrderItemVO> itemList;

    @ApiModelProperty(name = "payment_list", value = "支付列表集合")
    private List<OrderPaymentDO> paymentList;

    @ApiModelProperty(name = "store", value = "门店信息")
    private StoreDO store;

    @ApiModelProperty(name = "allowable", value = "允许的操作")
    private OrderAllowable allowable;

    @ApiModelProperty(name = "status_text", value = "订单状态（中文描述）")
    private String statusText;

    @ApiModelProperty(name = "delivery_type_text", value = "配送类型（中文描述）")
    private String deliveryTypeText;

    @ApiModelProperty(name = "payment_status_text", value = "支付状态（中文描述）")
    private String paymentStatusText;

}

