package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.StockTransferProductDO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 调拨单商品明细VO
 *
 * @author zs
 * @since 2023-12-12 16:34:15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class StockTransferProductVO extends StockTransferProductDO {

}

