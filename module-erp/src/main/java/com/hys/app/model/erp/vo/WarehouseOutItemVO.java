package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.WarehouseOutItemDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * 出库单商品明细详情
 *
 * @author zs
 * @since 2023-12-07 17:06:32
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
public class WarehouseOutItemVO extends WarehouseOutItemDO {

}

