package com.hys.app.model.support;

/**
 * 日志级别
 * @author fk
 * @data 2021年11月29日11:44:22
 * @version 1.0
 **/
public enum LogClient {

    /**
     * 管理端
     */
    admin,

    /**
     * 商家端
     */
    seller;


}
