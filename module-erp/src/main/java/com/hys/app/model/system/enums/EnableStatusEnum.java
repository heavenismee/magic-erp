package com.hys.app.model.system.enums;

/**
 * 启用状态枚举
 * @author zhangsong
 * @version v1.0
 * @since v7.3.0
 * 2021-01-26 16:04:23
 */
public enum EnableStatusEnum {

    /**
     * 启用
     */
    OPEN("启用"),

    /**
     * 关闭
     */
    CLOSE("关闭");

    private String description;

    EnableStatusEnum(String description) {
        this.description = description;
    }

    public String description() {
        return description;
    }
    public String value(){
        return this.name();
    }
}
