package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.StockDamageReportProduct;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 库存报损单商品统计实体
 *
 * @author dmy
 * 2023-12-05
 */
@ApiModel
public class StockDamageReportStatistics extends StockDamageReportProduct implements Serializable {

    private static final long serialVersionUID = -8350085332978744204L;

    /**
     * 报损单编号
     */
    @ApiModelProperty(name = "sn", value = "报损单编号")
    private String sn;
    /**
     * 报损时间
     */
    @ApiModelProperty(name = "report_time", value = "报损时间")
    private Long reportTime;
    /**
     * 报损类型
     */
    @ApiModelProperty(name = "damage_type_key", value = "报损类型")
    private String damageTypeKey;

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Long getReportTime() {
        return reportTime;
    }

    public void setReportTime(Long reportTime) {
        this.reportTime = reportTime;
    }

    public String getDamageTypeKey() {
        return damageTypeKey;
    }

    public void setDamageTypeKey(String damageTypeKey) {
        this.damageTypeKey = damageTypeKey;
    }

    @Override
    public String toString() {
        return "StockDamageReportStatistics{" +
                "sn='" + sn + '\'' +
                ", reportTime=" + reportTime +
                ", damageTypeKey='" + damageTypeKey + '\'' +
                '}';
    }
}
