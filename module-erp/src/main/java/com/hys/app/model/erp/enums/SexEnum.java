package com.hys.app.model.erp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 性别枚举
 *
 * @author zs
 * @since 2024-01-23
 */
@Getter
@AllArgsConstructor
public enum SexEnum {

    /**
     * 男
     */
    Men,
    /**
     * 女
     */
    Woman,

}
