package com.hys.app.model.base;

import lombok.experimental.UtilityClass;

/**
 * 常量
 *
 * @author zs
 * @since 2024-01-11
 */
@UtilityClass
public class ThreadPoolConstant {

    /**
     * 消息推送线程池名称
     */
    public static final String MESSAGE_PUSH_POOL = "messagePushThreadPool";

    /**
     * 公共线程池名称
     */
    public static final String COMMON_POOL = "commonThreadPool";

}
