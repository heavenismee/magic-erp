package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 换货单产品实体DTO
 *
 * @Author dmy
 * 2023-12-05
 */
@ApiModel
public class ChangeFormProductDTO implements Serializable {

    private static final long serialVersionUID = 6144583067545448067L;

    /**
     * 入库单ID
     */
    @NotNull(message = "入库单ID不能为空")
    @ApiModelProperty(name = "stock_id", value = "入库单ID")
    private Long stockId;
    /**
     * 入库单编号
     */
    @ApiModelProperty(name = "stock_sn", value = "入库单编号", required = true)
    @NotEmpty(message = "入库单编号不能为空")
    private String stockSn;
    /**
     * 入库单明细id
     */
    @NotNull(message = "入库单明细id不能为空")
    @ApiModelProperty(name = "stock_item_id", value = "入库单明细id")
    private Long stockItemId;
    /**
     * 入库批次ID
     */
    @ApiModelProperty(name = "stock_batch_id", value = "入库批次ID", required = true)
    @NotNull(message = "入库批次ID不能为空")
    private Long stockBatchId;
    /**
     * 入库批次号
     */
    @ApiModelProperty(name = "stock_batch_sn", value = "入库批次号", required = true)
    @NotEmpty(message = "入库批次编号不能为空")
    private String stockBatchSn;
    /**
     * 商品ID
     */
    @ApiModelProperty(name = "goods_id", value = "商品ID", required = true)
    @NotNull(message = "商品ID不能为空")
    private Long goodsId;
    /**
     * 产品ID
     */
    @ApiModelProperty(name = "product_id", value = "产品ID", required = true)
    @NotNull(message = "产品ID不能为空")
    private Long productId;
    /**
     * 产品名称
     */
    @ApiModelProperty(name = "product_name", value = "产品名称", required = true)
    @NotEmpty(message = "产品名称不能为空")
    private String productName;
    /**
     * 产品编号
     */
    @ApiModelProperty(name = "product_sn", value = "产品编号", required = true)
    @NotEmpty(message = "产品编号不能为空")
    private String productSn;
    /**
     * 产品规格
     */
    @ApiModelProperty(name = "specification", value = "产品规格", required = true)
    @NotEmpty(message = "产品规格不能为空")
    private String specification;
    /**
     * 分类ID
     */
    @ApiModelProperty(name = "category_id", value = "分类ID", required = true)
    @NotNull(message = "分类ID不能为空")
    private Long categoryId;
    /**
     * 分类名称
     */
    @ApiModelProperty(name = "category_name", value = "分类名称", required = true)
    @NotEmpty(message = "分类名称不能为空")
    private String categoryName;
    /**
     * 产品单位
     */
    @ApiModelProperty(name = "unit", value = "产品单位", required = true)
    @NotEmpty(message = "产品单位不能为空")
    private String unit;
    /**
     * 数量
     */
    @ApiModelProperty(name = "num", value = "数量", required = true)
    @NotNull(message = "数量不能为空")
    private Integer num;
    /**
     * 金额
     */
    @ApiModelProperty(name = "amount", value = "金额", required = true)
    @NotNull(message = "金额不能为空")
    private Double amount;

    /**
     * 成本价
     */
    @ApiModelProperty(name = "cost_price", value = "成本价")
    @NotNull(message = "成本价不能为空")
    private Double costPrice;

    public Long getStockId() {
        return stockId;
    }

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    public String getStockSn() {
        return stockSn;
    }

    public void setStockSn(String stockSn) {
        this.stockSn = stockSn;
    }

    public Long getStockItemId() {
        return stockItemId;
    }

    public void setStockItemId(Long stockItemId) {
        this.stockItemId = stockItemId;
    }

    public Long getStockBatchId() {
        return stockBatchId;
    }

    public void setStockBatchId(Long stockBatchId) {
        this.stockBatchId = stockBatchId;
    }

    public String getStockBatchSn() {
        return stockBatchSn;
    }

    public void setStockBatchSn(String stockBatchSn) {
        this.stockBatchSn = stockBatchSn;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSn() {
        return productSn;
    }

    public void setProductSn(String productSn) {
        this.productSn = productSn;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(Double costPrice) {
        this.costPrice = costPrice;
    }

    @Override
    public String toString() {
        return "ChangeFormProductDTO{" +
                "stockSn='" + stockSn + '\'' +
                ", stockBatchId=" + stockBatchId +
                ", stockBatchSn='" + stockBatchSn + '\'' +
                ", goodsId=" + goodsId +
                ", productId=" + productId +
                ", productName='" + productName + '\'' +
                ", productSn='" + productSn + '\'' +
                ", specification='" + specification + '\'' +
                ", categoryId=" + categoryId +
                ", categoryName='" + categoryName + '\'' +
                ", unit='" + unit + '\'' +
                ", num=" + num +
                ", amount=" + amount +
                ", costPrice=" + costPrice +
                '}';
    }
}
