package com.hys.app.model.erp.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hys.app.model.goods.dos.GoodsGalleryDO;
import com.hys.app.model.goods.dos.GoodsParamsDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 商品新增/编辑DTO
 *
 * @author zs
 * 2023-12-26 15:56:58
 */
@Data
public class GoodsDTO {

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "sn", value = "商品编号")
    @NotBlank(message = "商品编号不能为空")
    private String sn;

    @ApiModelProperty(name = "name", value = "商品名称")
    @NotBlank(message = "商品名称不能为空")
    private String name;

    @ApiModelProperty(name = "category_id", value = "商品分类id")
    @NotNull(message = "商品分类id不能为空")
    private Long categoryId;

    @ApiModelProperty(name = "brand_id", value = "品牌id")
    private Long brandId;

    @ApiModelProperty(name = "market_enable", value = "是否上架 true上架")
    @NotNull(message = "状态不能为空")
    private Boolean marketEnable;

    @ApiModelProperty(name = "description", value = "商品详情")
    private String description;

    @ApiModelProperty(name = "freight_template_id", value = "运费模板id（0代表包邮）")
    @NotNull(message = "运费模板id不能为空")
    private Long freightTemplateId;

    @ApiModelProperty(name = "video", value = "商品视频")
    private String video;

    @ApiModelProperty(name = "have_spec", value = "是否有规格")
    @NotNull(message = "是否有规格不能为空")
    private Boolean haveSpec;

    @ApiModelProperty(name = "image_list", value = "商品相册")
    @NotEmpty(message = "商品相册不能为空")
    private List<GoodsGalleryDO> imageList;

    @ApiModelProperty(name = "param_list", value = "商品参数列表")
    @Valid
    private List<GoodsParamsDO> paramList;

    @ApiModelProperty(name = "product_list", value = "产品列表")
    @NotEmpty(message = "产品列表不能为空")
    @Valid
    private List<ProductDTO> productList;
}

