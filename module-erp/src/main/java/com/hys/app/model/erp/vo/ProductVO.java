package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.ProductDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 产品详情
 *
 * @author zs
 * 2023-11-30 16:06:44
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ProductVO extends ProductDO {

    @ApiModelProperty(name = "category_name", value = "分类名称")
    private String categoryName;

    @ApiModelProperty(name = "stock_num", value = "库存数量")
    private Integer stockNum;

}

