package com.hys.app.model.erp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 编号生成的规则项类型
 *
 * @author zs
 * @since 2023-11-30
 */
@Getter
@AllArgsConstructor
public enum NoGenerateItemTypeEnum {

    /**
     * 常量
     */
    Const,
    /**
     * 当前年
     */
    Year,
    /**
     * 当前年月
     */
    YearMonth,
    /**
     * 当前年月日
     */
    YearMonthDay,
    /**
     * 顺序号
     */
    Seq,
    /**
     * 分部编码
     */
    Dept


}
