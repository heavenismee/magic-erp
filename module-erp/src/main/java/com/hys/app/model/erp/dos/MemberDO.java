package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import com.hys.app.model.erp.enums.SexEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;

/**
 * 会员DO
 *
 * @author zsong
 * 2024-01-23 09:28:16
 */
@TableName("erp_member")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class MemberDO extends BaseDO {

    private static final long serialVersionUID = 1L;
    
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(name = "sn", value = "编号")
    private String sn;
    
    @ApiModelProperty(name = "name", value = "姓名")
    private String name;
    
    @ApiModelProperty(name = "sex", value = "性别")
    private SexEnum sex;
    
    @ApiModelProperty(name = "mobile", value = "手机号")
    private String mobile;
    
    @ApiModelProperty(name = "email", value = "邮箱")
    private String email;
    
    @ApiModelProperty(name = "birthday", value = "出生日期")
    private Long birthday;
    
    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;
    
    @ApiModelProperty(name = "disable_flag", value = "是否禁用")
    private Boolean disableFlag;
    
}
