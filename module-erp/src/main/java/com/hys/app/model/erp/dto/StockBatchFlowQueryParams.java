package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 库存批次流水分页查询参数
 *
 * @author zsong
 * 2024-01-16 11:39:01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "库存批次流水分页查询参数")
public class StockBatchFlowQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "warehouse_entry_id", value = "入库单id")
    private Long warehouseEntryId;
    
    @ApiModelProperty(name = "warehouse_entry_sn", value = "入库单编号")
    private String warehouseEntrySn;
    
    @ApiModelProperty(name = "batch_id", value = "批次id")
    private Long batchId;
    
    @ApiModelProperty(name = "batch_sn", value = "批次编号")
    private String batchSn;
    
    @ApiModelProperty(name = "warehouse_id", value = "仓库id")
    private Long warehouseId;
    
    @ApiModelProperty(name = "goods_id", value = "商品id")
    private Long goodsId;
    
    @ApiModelProperty(name = "product_id", value = "产品id")
    private Long productId;
    
    @ApiModelProperty(name = "product_sn", value = "产品编号")
    private String productSn;
    
    @ApiModelProperty(name = "product_name", value = "产品名称")
    private String productName;
    
    @ApiModelProperty(name = "product_specification", value = "产品规格")
    private String productSpecification;
    
    @ApiModelProperty(name = "product_unit", value = "产品单位")
    private String productUnit;
    
    @ApiModelProperty(name = "product_barcode", value = "产品条形码")
    private String productBarcode;
    
    @ApiModelProperty(name = "category_id", value = "产品分类id")
    private Long categoryId;
    
    @ApiModelProperty(name = "category_name", value = "产品分类名称")
    private String categoryName;
    
    @ApiModelProperty(name = "source_type", value = "库存变更来源")
    private String sourceType;
    
    @ApiModelProperty(name = "source_sn", value = "库存变更来源单号")
    private String sourceSn;
    
    @ApiModelProperty(name = "create_time", value = "创建时间")
    private Long[] createTime;
    
}

