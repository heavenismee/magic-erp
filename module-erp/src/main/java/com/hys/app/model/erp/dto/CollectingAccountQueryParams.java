package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 收款账户分页查询参数
 *
 * @author zsong
 * 2024-01-24 14:43:18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "收款账户分页查询参数")
public class CollectingAccountQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "name", value = "账户名称")
    private String name;
    
    @ApiModelProperty(name = "sn", value = "编号")
    private String sn;
    
    @ApiModelProperty(name = "default_flag", value = "是否默认")
    private Boolean defaultFlag;
    
    @ApiModelProperty(name = "enable_flag", value = "是否启用")
    private Boolean enableFlag;
    
    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;
    
    @ApiModelProperty(name = "create_time", value = "创建时间")
    private Long[] createTime;
    
}

