package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 供应商结算单明细新增/编辑DTO
 *
 * @author zs
 * @since 2023-12-15 14:09:20
 */
@Data
public class SupplierSettlementItemDTO {

    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "supplier_settlement_id", value = "供应商结算单id")
    @NotNull(message = "供应商结算单id不能为空")
    private Long supplierSettlementId;
    
    @ApiModelProperty(name = "warehouse_entry_id", value = "入库单id")
    @NotNull(message = "入库单id不能为空")
    private Long warehouseEntryId;
    
    @ApiModelProperty(name = "warehouse_entry_sn", value = "入库单编号")
    @NotBlank(message = "入库单编号不能为空")
    private String warehouseEntrySn;
    
    @ApiModelProperty(name = "product_id", value = "商品id")
    @NotNull(message = "商品id不能为空")
    private Long productId;
    
    @ApiModelProperty(name = "product_name", value = "商品名称")
    @NotBlank(message = "商品名称不能为空")
    private String productName;
    
    @ApiModelProperty(name = "product_specification", value = "商品规格")
    @NotBlank(message = "商品规格不能为空")
    private String productSpecification;
    
    @ApiModelProperty(name = "product_unit", value = "商品单位")
    @NotBlank(message = "商品单位不能为空")
    private String productUnit;
    
    @ApiModelProperty(name = "product_price", value = "商品单价")
    @NotNull(message = "商品单价不能为空")
    private Double productPrice;
    
    @ApiModelProperty(name = "product_num", value = "商品数量")
    @NotNull(message = "商品数量不能为空")
    private Integer productNum;
    
    @ApiModelProperty(name = "total_price", value = "总价格")
    @NotNull(message = "总价格不能为空")
    private Double totalPrice;
    
}

