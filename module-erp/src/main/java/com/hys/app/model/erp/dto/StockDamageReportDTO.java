package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 库存报损单实体DTO
 *
 * @author dmy
 * 2023-12-05
 */
public class StockDamageReportDTO implements Serializable {

    private static final long serialVersionUID = -2146175467517339422L;

    /**
     * 部门ID
     */
    @ApiModelProperty(name = "dept_id", value = "部门ID", required = true)
    @NotNull(message = "部门ID不能为空")
    private Long deptId;
    /**
     * 部门名称
     */
    @ApiModelProperty(name = "dept_name", value = "部门名称", required = true)
    @NotEmpty(message = "部门名称不能为空")
    private String deptName;
    /**
     * 仓库ID
     */
    @ApiModelProperty(name = "warehouse_id", value = "仓库ID", required = true)
    @NotNull(message = "仓库ID不能为空")
    private Long warehouseId;
    /**
     * 仓库名称
     */
    @ApiModelProperty(name = "warehouse_name", value = "仓库名称", required = true)
    @NotEmpty(message = "仓库名称不能为空")
    private String warehouseName;
    /**
     * 制单人ID
     */
    @ApiModelProperty(name = "creator_id", value = "制单人ID", required = true)
    @NotNull(message = "制单人ID不能为空")
    private Long creatorId;
    /**
     * 制单人
     */
    @ApiModelProperty(name = "creator", value = "制单人", required = true)
    @NotEmpty(message = "制单人不能为空")
    private String creator;
    /**
     * 报损时间
     */
    @ApiModelProperty(name = "report_time", value = "报损时间", required = true)
    @NotNull(message = "报损时间不能为空")
    private Long reportTime;
    /**
     * 报损类型（键）
     */
    @ApiModelProperty(name = "damage_type_key", value = "报损类型（键）")
    @NotEmpty(message = "报损类型（键）不能为空")
    private String damageTypeKey;
    /**
     * 报损类型（值）
     */
    @ApiModelProperty(name = "damage_type_value", value = "报损类型（值）")
    @NotEmpty(message = "报损类型（值）不能为空")
    private String damageTypeValue;
    /**
     * 报损说明
     */
    @ApiModelProperty(name = "report_desc", value = "报损说明", required = true)
    @NotEmpty(message = "调整说明不能为空")
    private String reportDesc;
    /**
     * 库存报损单产品集合
     */
    @ApiModelProperty(name = "product_list", value = "库存报损单产品集合", required = true)
    @NotNull(message = "库存报损单产品不能为空")
    @Valid
    private List<StockDamageReportProductDTO> productList;

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Long getReportTime() {
        return reportTime;
    }

    public void setReportTime(Long reportTime) {
        this.reportTime = reportTime;
    }

    public String getDamageTypeKey() {
        return damageTypeKey;
    }

    public void setDamageTypeKey(String damageTypeKey) {
        this.damageTypeKey = damageTypeKey;
    }

    public String getDamageTypeValue() {
        return damageTypeValue;
    }

    public void setDamageTypeValue(String damageTypeValue) {
        this.damageTypeValue = damageTypeValue;
    }

    public String getReportDesc() {
        return reportDesc;
    }

    public void setReportDesc(String reportDesc) {
        this.reportDesc = reportDesc;
    }

    public List<StockDamageReportProductDTO> getProductList() {
        return productList;
    }

    public void setProductList(List<StockDamageReportProductDTO> productList) {
        this.productList = productList;
    }

    @Override
    public String toString() {
        return "StockDamageReportDTO{" +
                "deptId=" + deptId +
                ", deptName='" + deptName + '\'' +
                ", warehouseId=" + warehouseId +
                ", warehouseName='" + warehouseName + '\'' +
                ", creatorId=" + creatorId +
                ", creator='" + creator + '\'' +
                ", reportTime=" + reportTime +
                ", damageTypeKey='" + damageTypeKey + '\'' +
                ", damageTypeValue='" + damageTypeValue + '\'' +
                ", reportDesc='" + reportDesc + '\'' +
                ", productList=" + productList +
                '}';
    }
}
