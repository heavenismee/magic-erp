package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.WarehouseDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 仓库详情
 *
 * @author zs
 * 2023-11-30 16:35:16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WarehouseVO extends WarehouseDO {

    @ApiModelProperty(name = "dept_name", value = "部门名称")
    private String deptName;

    @ApiModelProperty(name = "admin_name", value = "管理员名称")
    private String adminName;

}

