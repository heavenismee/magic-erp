package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 * 入库单产品明细实体类
 *
 * @author zs
 * @since 2023-12-05 15:30:10
 */
@TableName("erp_warehouse_entry_product")
@Data
public class WarehouseEntryProductDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "warehouse_entry_id", value = "入库单id")
    private Long warehouseEntryId;

    @ApiModelProperty(name = "goods_id", value = "商品id")
    private Long goodsId;

    @ApiModelProperty(name = "product_id", value = "产品id")
    private Long productId;

    @ApiModelProperty(name = "sn", value = "产品编号")
    private String sn;

    @ApiModelProperty(name = "name", value = "产品名称")
    private String name;

    @ApiModelProperty(name = "specification", value = "产品规格型号")
    private String specification;

    @ApiModelProperty(name = "brand", value = "产品品牌")
    private String brand;

    @ApiModelProperty(name = "unit", value = "产品单位")
    private String unit;

    @ApiModelProperty(name = "product_barcode", value = "产品条形码")
    private String productBarcode;

    @ApiModelProperty(name = "category_id", value = "产品分类id")
    private Long categoryId;

    @ApiModelProperty(name = "category_name", value = "产品分类名称")
    private String categoryName;

    @ApiModelProperty(name = "contract_num", value = "合同数量")
    private Integer contractNum;

    @ApiModelProperty(name = "num", value = "本次入库数量")
    private Integer num;

    @ApiModelProperty(name = "contract_price", value = "合同单价")
    private Double contractPrice;

    @ApiModelProperty(name = "total_price", value = "入库总价")
    private Double totalPrice;

    @ApiModelProperty(name = "cost_price", value = "进货单价")
    private Double costPrice;

    @ApiModelProperty(name = "tax_rate", value = "税率")
    private Double taxRate;

    @ApiModelProperty(name = "return_num", value = "退货数量")
    private Integer returnNum;

}
