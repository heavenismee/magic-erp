package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 仓库新增/编辑DTO
 *
 * @author zs
 * 2023-11-30 16:35:16
 */
@Data
public class WarehouseDTO {

    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "name", value = "仓库名称")
    @NotBlank(message = "仓库名称不能为空")
    private String name;
    
    @ApiModelProperty(name = "sn", value = "仓库编号")
    @NotBlank(message = "仓库编号不能为空")
    private String sn;
    
    @ApiModelProperty(name = "admin_id", value = "管理员id")
    @NotNull(message = "管理员id不能为空")
    private Long adminId;
    
    @ApiModelProperty(name = "description", value = "描述")
    private String description;
    
    @ApiModelProperty(name = "dept_id", value = "部门id")
    @NotNull(message = "请选择部门")
    private Long deptId;

}

