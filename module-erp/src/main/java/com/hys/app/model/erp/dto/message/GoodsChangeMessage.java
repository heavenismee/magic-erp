package com.hys.app.model.erp.dto.message;

import com.hys.app.model.erp.dto.GoodsDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 商品变更消息
 *
 * @author zs
 * @since 2024-01-19
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoodsChangeMessage implements Serializable {

    private static final long serialVersionUID = 524607318299159659L;

    /**
     * 变更类型
     */
    private GoodsChangeType goodsChangeType;

    /**
     * 商品id
     */
    private Long id;

    public enum GoodsChangeType {
        /**
         * 添加
         */
        Add,
        /**
         * 编辑
         */
        Edit
    }
}
