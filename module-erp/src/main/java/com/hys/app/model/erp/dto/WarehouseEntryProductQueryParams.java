package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 入库单商品明细查询参数
 *
 * @author zs
 * @since 2023-12-05 15:30:10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WarehouseEntryProductQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "warehouse_entry_id", value = "入库单id")
    private Long warehouseEntryId;
    
    @ApiModelProperty(name = "sn", value = "商品编号")
    private String sn;
    
    @ApiModelProperty(name = "name", value = "商品名称")
    private String name;
    
    @ApiModelProperty(name = "specification", value = "商品规格型号")
    private String specification;
    
    @ApiModelProperty(name = "brand", value = "商品品牌")
    private String brand;
    
    @ApiModelProperty(name = "unit", value = "商品单位")
    private String unit;
    
    @ApiModelProperty(name = "contract_num", value = "合同数量")
    private Integer contractNum;
    
    @ApiModelProperty(name = "already_num", value = "已入库数量")
    private Integer alreadyNum;
    
    @ApiModelProperty(name = "num", value = "本次入库数量")
    private Integer num;
    
}

