package com.hys.app.model.base.rabbitmq;

/**
 * 延迟加载任务执行器
 *
 * @author zh
 * @version v7.0
 * @date 19/3/1 下午2:13
 * @since v7.0
 */
public class TimeExecute {

    /**
     * 促销延迟加载执行器
     */
    public final static String PROMOTION_EXECUTER = "promotionTimeTriggerExecuter";
    /**
     * 优惠券脚本创建延迟加载执行器
     */
    public final static String COUPON_SCRIPT_EXECUTER = "couponScriptTimeTriggerExecuter";
    /**
     * 作废膨胀卷延时消息
     */
    public static final String INVALID_SWELL_EXECUTER = "invalidSwellTimeTriggerExecuter";
    /**
     * 群sop消息推送提醒延迟加载执行器
     */
    public final static String GROUP_SOP_EXECUTER = "groupSopTimeTriggerExecuter";
    /**
     * 个人sop消息推送提醒延迟加载执行器
     */
    public final static String PERSONAL_SOP_EXECUTER = "personalSopTimeTriggerExecuter";
    /**
     * 好友裂变活动失效延时消息
     */
    public static final String INVALID_FISSION_EXECUTER = "invalidFissionTimeTriggerExecuter";
    /**
     * 促销活动脚本创建延迟加载执行器
     */
    public final static String PROMOTION_SCRIPT_EXECUTER = "promotionScriptTimeTriggerExecuter";
    /**
     * 拼团延迟加载执行器
     */
    public final static String PINTUAN_EXECUTER = "pintuanTimeTriggerExecuter";
    /**
     * 拼团订单虚拟成团延迟加载执行器
     */
    public final static String PINTUAN_ORDER_EXECUTER = "pintuanOrderTimeTriggerExecuter";
    /**
     * 限时抢购商品促销标签生成延迟加载执行器
     */
    public final static String SECKILL_GOODS_PROMOTION_TAGS_EXECUTER = "seckillGoodsPromotionTagsTimeTriggerExecuter";
}
