package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * 商品库存实体类
 *
 * @author zs
 * @since 2023-12-07 10:08:44
 */
@TableName("erp_product_stock")
@Data
@NoArgsConstructor
public class ProductStockDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "warehouse_id", value = "仓库id")
    private Long warehouseId;

    @ApiModelProperty(name = "goods_id", value = "商品id")
    private Long goodsId;

    @ApiModelProperty(name = "product_id", value = "产品id")
    private Long productId;

    @ApiModelProperty(name = "stock", value = "库存数量")
    private Integer num;

    public ProductStockDO(Long warehouseId, Long goodsId, Long productId, Integer num) {
        this.setWarehouseId(warehouseId);
        this.setGoodsId(goodsId);
        this.setProductId(productId);
        this.setNum(num);
    }

}
