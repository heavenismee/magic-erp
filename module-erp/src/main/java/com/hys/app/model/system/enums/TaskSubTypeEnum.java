package com.hys.app.model.system.enums;

/**
 * 任务子类型
 *
 * @author zs
 * @since 2024-01-23
 **/
public enum TaskSubTypeEnum {

    // ============ 导入相关 ============
    /**
     * 导入
     */
    MemberImport,

}
