package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * 门店新增/编辑DTO
 *
 * @author zs
 * @since 2023-12-11 17:47:09
 */
@Data
public class StoreDTO {

    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "store_name", value = "门店名称")
    @NotEmpty(message = "门店名称不能为空")
    private String storeName;

    @ApiModelProperty(name = "address", value = "门店地址")
    @NotEmpty(message = "门店地址不能为空")
    private String address;

    @ApiModelProperty(name = "contact_person", value = "联系人")
    @NotEmpty(message = "联系人不能为空")
    private String contactPerson;

    @ApiModelProperty(name = "telephone", value = "门店联系电话")
    @NotEmpty(message = "门店联系电话不能为空")
    private String telephone;

}

