package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 订单退货项新增/编辑DTO
 *
 * @author zs
 * @since 2023-12-14 15:42:07
 */
@Data
public class OrderReturnItemDTO {

    @ApiModelProperty(name = "warehouse_out_item_id", value = "出库单项的id")
    @NotNull(message = "商品id不能为空")
    private Long warehouseOutItemId;

    @ApiModelProperty(name = "return_num", value = "退货数量")
    @NotNull(message = "退货数量不能为空")
    private Integer returnNum;

}

