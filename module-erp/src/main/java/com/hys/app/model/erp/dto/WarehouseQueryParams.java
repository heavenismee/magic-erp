package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 仓库查询参数
 *
 * @author zs
 * 2023-11-30 16:35:16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WarehouseQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "keyword", value = "仓库名称")
    private String keyword;

    @ApiModelProperty(name = "name", value = "仓库名称")
    private String name;

    @ApiModelProperty(name = "sn", value = "仓库编号")
    private String sn;
    
    @ApiModelProperty(name = "admin_id", value = "管理员id")
    private Long adminId;
    
    @ApiModelProperty(name = "description", value = "描述")
    private String description;
    
    @ApiModelProperty(name = "dept_id", value = "部门id")
    private Long deptId;
    
}

