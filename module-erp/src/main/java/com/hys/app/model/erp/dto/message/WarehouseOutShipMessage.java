package com.hys.app.model.erp.dto.message;

import com.hys.app.model.erp.dos.WarehouseOutItemDO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 出库单发货消息
 *
 * @author zs
 * @since 2024-01-09
 */
@Data
public class WarehouseOutShipMessage implements Serializable {

    private static final long serialVersionUID = 524607318299159659L;

    private Long id;

    private List<WarehouseOutItemDO> itemList;
}
