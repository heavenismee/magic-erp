package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 财务明细分页查询参数
 *
 * @author zsong
 * 2024-03-15 17:40:23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "财务明细分页查询参数")
public class FinanceItemQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "sn", value = "财务明细编号")
    private String sn;
    
    @ApiModelProperty(name = "source_type", value = "来源")
    private String sourceType;
    
    @ApiModelProperty(name = "source_sn", value = "来源编号")
    private String sourceSn;
    
    @ApiModelProperty(name = "amount_type", value = "收入/支出")
    private String amountType;

    @ApiModelProperty(name = "settle_flag", value = "是否已结算")
    private Boolean settleFlag;

    @ApiModelProperty(name = "create_time", value = "创建时间")
    private Long[] createTime;
    
}

