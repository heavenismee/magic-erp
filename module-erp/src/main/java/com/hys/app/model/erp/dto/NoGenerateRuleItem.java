package com.hys.app.model.erp.dto;

import com.hys.app.framework.util.StringUtil;
import com.hys.app.model.erp.enums.NoGenerateItemTypeEnum;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.AssertFalse;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 编号生成规则项
 *
 * @author zs
 * @since 2023-12-01
 */
@Data
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class NoGenerateRuleItem {

    @ApiModelProperty(name = "type", value = "规则类型")
    @NotNull(message = "规则类型不能为空")
    private NoGenerateItemTypeEnum type;

    @ApiModelProperty(name = "const_value", value = "常量值")
    private String constValue;

    @ApiModelProperty(name = "seq_length", value = "顺序号的位数")
    @Min(value = 2, message = "顺序号的位数最小为2")
    @Max(value = 20, message = "顺序号的位数最大为20")
    private Integer seqLength;

    @ApiModelProperty(name = "split", value = "分隔符")
    @NotNull(message = "分隔符不能为空")
    private String split;

    @AssertFalse(message = "常量值不能为空")
    public boolean isConstValue() {
        return type == NoGenerateItemTypeEnum.Const && StringUtil.isEmpty(constValue);
    }

    @AssertFalse(message = "顺序号的位数 不能为空")
    public boolean isSeqLength() {
        return type == NoGenerateItemTypeEnum.Seq && seqLength == null;
    }

}

