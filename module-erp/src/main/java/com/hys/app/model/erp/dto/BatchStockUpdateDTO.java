package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 更新批次库存DTO
 *
 * @author zs
 * @since 2023-12-11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BatchStockUpdateDTO {

    @ApiModelProperty(value = "批次id")
    private Long id;

    @ApiModelProperty(name = "change_num", value = "变更的库存")
    private Integer changeNum;

}

