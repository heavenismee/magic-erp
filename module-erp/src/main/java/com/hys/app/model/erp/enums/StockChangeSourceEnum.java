package com.hys.app.model.erp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 库存变更来源枚举
 *
 * @author zs
 * @since 2024-01-16
 */
@Getter
@AllArgsConstructor
public enum StockChangeSourceEnum {

    /**
     * 入库单
     */
    WAREHOUSE_ENTRY,

    /**
     * 出库单
     */
    WAREHOUSE_OUT,

    /**
     * 供应商退货
     */
    SUPPLIER_RETURN,

    /**
     * 订单退货
     */
    ORDER_RETURN,

    /**
     * 库存调拨
     */
    STOCK_TRANSFER,

    /**
     * 库存报损
     */
    STOCK_DAMAGE,

    /**
     * 换货
     */
    CHANGE_FORM,
}
