package com.hys.app.model.system.enums;

/**
 * 菜单类型 枚举
 *
 * @author zs
 * @since 2023-11-27
 */
public enum MenuTypeEnum {

    /**
     * 菜单
     */
    MENU,

    /**
     * 按钮
     */
    BUTTON,


}
