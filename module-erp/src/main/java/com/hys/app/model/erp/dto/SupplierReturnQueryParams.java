package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 供应商退货查询参数
 *
 * @author zs
 * @since 2023-12-14 11:12:46
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SupplierReturnQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "sn", value = "编号")
    private String sn;
    
    @ApiModelProperty(name = "dept_id", value = "部门id")
    private Long deptId;
    
    @ApiModelProperty(name = "warehouse_entry_id", value = "入库单id")
    private Long warehouseEntryId;
    
    @ApiModelProperty(name = "warehouse_entry_sn", value = "入库单编号")
    private String warehouseEntrySn;
    
    @ApiModelProperty(name = "warehouse_id", value = "仓库id")
    private Long warehouseId;
    
    @ApiModelProperty(name = "supplier_id", value = "供应商id")
    private Long supplierId;
    
    @ApiModelProperty(name = "contract_id", value = "合同id")
    private Long contractId;
    
    @ApiModelProperty(name = "handle_by", value = "经手人")
    private String handleBy;
    
    @ApiModelProperty(name = "attachment", value = "附件")
    private String attachment;
    
    @ApiModelProperty(name = "return_time", value = "退货时间")
    private Long returnTime;
    
    @ApiModelProperty(name = "status", value = "状态")
    private String status;
    
    @ApiModelProperty(name = "audit_remark", value = "审核备注")
    private String auditRemark;

    @ApiModelProperty(name = "start_time", value = "开始时间")
    private Long startTime;

    @ApiModelProperty(name = "end_time", value = "结束时间")
    private Long endTime;

}

