package com.hys.app.model.system.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * 数据导入结果
 * @author kingapex
 * @version 1.0
 * @data 2022/8/1 17:04
 **/
public class ImportResult {

    public ImportResult() {
        errorList = new ArrayList<>();
    }


    private boolean result;

    private String filePath;

    public void putError(String error) {
        errorList.add(error);
    }
    private List<String> errorList;

    public List<String> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<String> errorList) {
        this.errorList = errorList;
    }

    public boolean getResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
