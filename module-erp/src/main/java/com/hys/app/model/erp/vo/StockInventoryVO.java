package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.StockInventory;
import com.hys.app.model.erp.dos.StockInventoryProduct;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * 库存盘点单实体VO
 *
 * @author dmy
 * 2023-12-05
 */
@ApiModel
public class StockInventoryVO extends StockInventory implements Serializable {

    private static final long serialVersionUID = 5362636527081818965L;

    /**
     * 库存盘点单商品信息集合
     */
    @ApiModelProperty(name = "product_list", value = "库存盘点单商品信息集合")
    private List<StockInventoryProduct> productList;

    public List<StockInventoryProduct> getProductList() {
        return productList;
    }

    public void setProductList(List<StockInventoryProduct> productList) {
        this.productList = productList;
    }

    @Override
    public String toString() {
        return "StockInventoryVO{" +
                "productList=" + productList +
                '}';
    }
}
