package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 会员分页查询参数
 *
 * @author zsong
 * 2024-01-23 09:28:16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "会员分页查询参数")
public class MemberQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "sn", value = "编号")
    private String sn;
    
    @ApiModelProperty(name = "name", value = "姓名")
    private String name;
    
    @ApiModelProperty(name = "sex", value = "性别")
    private String sex;
    
    @ApiModelProperty(name = "mobile", value = "手机号")
    private String mobile;
    
    @ApiModelProperty(name = "email", value = "邮箱")
    private String email;
    
    @ApiModelProperty(name = "birthday", value = "出生日期")
    private Long birthday;
    
    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;
    
    @ApiModelProperty(name = "disable_flag", value = "是否禁用")
    private Boolean disableFlag;
    
    @ApiModelProperty(name = "create_time", value = "创建时间")
    private Long[] createTime;
    
}

