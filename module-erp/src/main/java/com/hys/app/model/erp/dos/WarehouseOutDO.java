package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;
import com.hys.app.framework.database.mybatisplus.type.LongListTypeHandler;
import com.hys.app.framework.database.mybatisplus.type.StringListTypeHandler;
import com.hys.app.model.erp.enums.WarehouseOutStatusEnum;
import com.hys.app.model.erp.enums.OrderDeliveryType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;


/**
 * 出库单实体类
 *
 * @author zs
 * @since 2023-12-07 16:50:20
 */
@TableName(value = "erp_warehouse_out", autoResultMap = true)
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WarehouseOutDO extends BaseDO {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "sn", value = "出库单编号")
    private String sn;

    @ApiModelProperty(name = "out_time", value = "出库日期")
    private Long outTime;

    @ApiModelProperty(name = "dept_id", value = "部门id")
    private Long deptId;

    @ApiModelProperty(name = "warehouse_id", value = "仓库id")
    private Long warehouseId;

    @ApiModelProperty(name = "dept_name", value = "部门名称")
    private String deptName;

    @ApiModelProperty(name = "warehouse_name", value = "仓库名称")
    private String warehouseName;

    @ApiModelProperty(name = "member_real_name", value = "用户姓名")
    private String memberRealName;

    @ApiModelProperty(name = "member_mobile", value = "用户电话")
    private String memberMobile;

    @ApiModelProperty(name = "ship_region_ids", value = "收货地区id集合")
    @TableField(typeHandler = LongListTypeHandler.class)
    private List<Long> shipRegionIds;

    @ApiModelProperty(name = "ship_region_names", value = "收货地区名称集合")
    @TableField(typeHandler = StringListTypeHandler.class)
    private List<String> shipRegionNames;

    @ApiModelProperty(name = "ship_addr", value = "收货地址")
    private String shipAddr;

    @ApiModelProperty(name = "consignee", value = "提货人")
    private String consignee;

    @ApiModelProperty(name = "order_id_list", value = "订单id集合")
    @TableField(typeHandler = LongListTypeHandler.class)
    private List<Long> orderIdList;

    @ApiModelProperty(name = "order_sn_list", value = "订单编号集合")
    @TableField(typeHandler = StringListTypeHandler.class)
    private List<String> orderSnList;

    @ApiModelProperty(name = "delivery_type", value = "订单配送方式")
    private OrderDeliveryType deliveryType;

    @ApiModelProperty(name = "status", value = "状态")
    private WarehouseOutStatusEnum status;

    @ApiModelProperty(name = "logistics_company_id", value = "物流公司id")
    private Long logisticsCompanyId;

    @ApiModelProperty(name = "logistics_company_name", value = "物流公司名称")
    private String logisticsCompanyName;

    @ApiModelProperty(name = "tracking_number", value = "物流单号")
    private String trackingNumber;

    @ApiModelProperty(name = "freight_price", value = "物流费用")
    private Double freightPrice;

    @ApiModelProperty(name = "store_id", value = "自提门店id")
    private Long storeId;

    @ApiModelProperty(name = "store_name", value = "自提门店名称")
    private String storeName;

    @ApiModelProperty(name = "ship_time", value = "发货时间")
    private Long shipTime;

    @ApiModelProperty(name = "create_by", value = "制单人")
    private String createBy;

    @ApiModelProperty(name = "audit_by_id", value = "审核人id")
    private Long auditById;

    @ApiModelProperty(name = "audit_by_name", value = "审核人名称")
    private String auditByName;

    @ApiModelProperty(name = "warehouse_consignee_id", value = "仓库提货人id")
    private Long warehouseConsigneeId;

    @ApiModelProperty(name = "warehouse_consignee_name", value = "仓库提货人名称")
    private String warehouseConsigneeName;

}
