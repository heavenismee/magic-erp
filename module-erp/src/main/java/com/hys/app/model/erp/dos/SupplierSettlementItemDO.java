package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 * 供应商结算单明细实体类
 *
 * @author zs
 * @since 2023-12-15 14:09:20
 */
@TableName("erp_supplier_settlement_item")
@Data
public class SupplierSettlementItemDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "supplier_settlement_id", value = "供应商结算单id")
    private Long supplierSettlementId;

    @ApiModelProperty(name = "warehouse_entry_id", value = "入库单id")
    private Long warehouseEntryId;

    @ApiModelProperty(name = "warehouse_entry_sn", value = "入库单编号")
    private String warehouseEntrySn;

    @ApiModelProperty(name = "warehouse_entry_item_id", value = "入库单明细id")
    private Long warehouseEntryItemId;

    @ApiModelProperty(name = "goods_id", value = "商品id")
    private Long goodsId;

    @ApiModelProperty(name = "product_id", value = "产品id")
    private Long productId;

    @ApiModelProperty(name = "product_sn", value = "产品编号")
    private String productSn;

    @ApiModelProperty(name = "product_name", value = "产品名称")
    private String productName;

    @ApiModelProperty(name = "product_specification", value = "产品规格")
    private String productSpecification;

    @ApiModelProperty(name = "product_unit", value = "产品单位")
    private String productUnit;

    @ApiModelProperty(name = "product_barcode", value = "产品条形码")
    private String productBarcode;

    @ApiModelProperty(name = "category_id", value = "产品分类id")
    private Long categoryId;

    @ApiModelProperty(name = "category_name", value = "产品分类名称")
    private String categoryName;

    @ApiModelProperty(name = "product_price", value = "产品单价")
    private Double productPrice;

    @ApiModelProperty(name = "product_cost_price", value = "进货单价")
    private Double productCostPrice;

    @ApiModelProperty(name = "tax_rate", value = "税率")
    private Double taxRate;

    @ApiModelProperty(name = "product_num", value = "结算数量")
    private Integer productNum;

    @ApiModelProperty(name = "total_price", value = "总价格")
    private Double totalPrice;

}
