package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.ProductDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 商品库存预警信息VO
 *
 * @author dmy
 * 2023-12-15
 */
@ApiModel
public class ProductStockWarningVO extends ProductDO implements Serializable {

    private static final long serialVersionUID = 742062772065939324L;

    @ApiModelProperty(name = "warehouse_id", value = "仓库ID")
    private Long warehouseId;

    @ApiModelProperty(name = "warehouse_name", value = "仓库名称")
    private String warehouseName;

    @ApiModelProperty(name = "dept_id", value = "部门ID")
    private Long deptId;

    @ApiModelProperty(name = "dept_name", value = "部门名称")
    private String deptName;

    @ApiModelProperty(name = "stock_num", value = "库存数量")
    private Integer stockNum;

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    @Override
    public String toString() {
        return "ProductStockWarningVO{" +
                "warehouseId=" + warehouseId +
                ", warehouseName='" + warehouseName + '\'' +
                ", deptId=" + deptId +
                ", deptName='" + deptName + '\'' +
                ", stockNum=" + stockNum +
                '}';
    }
}
