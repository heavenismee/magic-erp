package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 出库单查询参数
 *
 * @author zs
 * @since 2023-12-07 16:50:20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WarehouseOutQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "sn", value = "出库单编号")
    private String sn;
    
    @ApiModelProperty(name = "out_time", value = "出库日期")
    private Long outTime;
    
    @ApiModelProperty(name = "dept_id", value = "部门id")
    private Long deptId;
    
    @ApiModelProperty(name = "warehouse_id", value = "仓库id")
    private Long warehouseId;
    
    @ApiModelProperty(name = "consignee", value = "提货人")
    private String consignee;
    
    @ApiModelProperty(name = "audit_by", value = "审核人")
    private String auditBy;
    
    @ApiModelProperty(name = "status", value = "状态")
    private String status;
    
}

