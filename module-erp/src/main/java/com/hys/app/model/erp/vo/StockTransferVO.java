package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.StockTransferDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 库存调拨VO
 *
 * @author zs
 * @since 2023-12-12 11:59:06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class StockTransferVO extends StockTransferDO {

    @ApiModelProperty(name = "allowable", value = "可进行的操作")
    private StockTransferAllowable allowable;

    @ApiModelProperty(name = "product_list", value = "商品列表")
    private List<StockTransferProductVO> productList;

}

