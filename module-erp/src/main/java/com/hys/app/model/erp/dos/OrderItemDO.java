package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 订单明细 DO
 *
 * @author zsong
 * 2024-01-24 16:39:38
 */
@TableName("erp_order_item")
@Data
public class OrderItemDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(name = "order_id", value = "订单id")
    private Long orderId;

    @ApiModelProperty(name = "order_sn", value = "订单编号")
    private String orderSn;

    // ================== 商品信息 ==================

    @ApiModelProperty(name = "goods_id", value = "商品id")
    private Long goodsId;

    @ApiModelProperty(name = "product_id", value = "产品id")
    private Long productId;

    @ApiModelProperty(name = "product_sn", value = "产品编号")
    private String productSn;

    @ApiModelProperty(name = "product_name", value = "产品名称")
    private String productName;

    @ApiModelProperty(name = "product_specification", value = "产品规格")
    private String productSpecification;

    @ApiModelProperty(name = "product_unit", value = "产品单位")
    private String productUnit;

    @ApiModelProperty(name = "product_image", value = "产品图片")
    private String productImage;

    @ApiModelProperty(name = "product_weight", value = "产品重量")
    private Double productWeight;

    @ApiModelProperty(name = "product_tax_rate", value = "产品税率")
    private Double productTaxRate;

    @ApiModelProperty(name = "product_cost_price", value = "产品成本价（单）")
    private Double productCostPrice;

    @ApiModelProperty(name = "product_mkt_price", value = "产品市场价（单）")
    private Double productMktPrice;

    @ApiModelProperty(name = "product_origin_price", value = "产品原价（单）")
    private Double productOriginPrice;

    @ApiModelProperty(name = "brand_id", value = "品牌id")
    private Long brandId;

    @ApiModelProperty(name = "brand_name", value = "品牌名称")
    private String brandName;

    @ApiModelProperty(name = "category_id", value = "分类id")
    private Long categoryId;

    @ApiModelProperty(name = "category_name", value = "分类名称")
    private String categoryName;

    // ================ 销售信息 ================

    @ApiModelProperty(name = "num", value = "销售数量")
    private Integer num;

    @ApiModelProperty(name = "price", value = "销售价（单）")
    private Double price;

    @ApiModelProperty(name = "total_price", value = "销售价（总）")
    private Double totalPrice;

    @ApiModelProperty(name = "tax_rate", value = "税率")
    private Double taxRate;

    @ApiModelProperty(name = "tax_price", value = "税额（总）")
    private Double taxPrice;

    @ApiModelProperty(name = "discount_price", value = "优惠金额（总）")
    private Double discountPrice;

    /**
     * 应付金额（总） = {@link #totalPrice} + {@link #taxPrice} - {@link #discountPrice}
     */
    @ApiModelProperty(name = "pay_price", value = "应付金额（总）")
    private Double payPrice;

    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;

}
