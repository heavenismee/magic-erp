package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;


/**
 * 岗位实体类
 *
 * @author zs
 * 2023-11-29 17:15:22
 */
@TableName("erp_post")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class PostDO extends BaseDO {

    private static final long serialVersionUID = 1L;
    
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "name", value = "岗位名称")
    private String name;
    
    @ApiModelProperty(name = "sn", value = "岗位编号")
    private String sn;
    
    @ApiModelProperty(name = "sort", value = "顺序")
    private Integer sort;
    
    @ApiModelProperty(name = "description", value = "岗位描述")
    private String description;
    
    @ApiModelProperty(name = "members", value = "岗位成员")
    private String members;
    
}
