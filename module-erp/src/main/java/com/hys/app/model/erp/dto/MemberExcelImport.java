package com.hys.app.model.erp.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import com.hys.app.model.erp.enums.SexEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 会员 Excel 导入
 * @author zs
 * @since 2024-01-23
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MemberExcelImport {

    @ExcelProperty("会员编号*")
    private String sn;

    @ExcelProperty("姓名")
    private String name;

    @ExcelProperty("性别")
    private String sexStr;

    @ExcelProperty("手机号")
    private String mobile;

    @ExcelProperty("邮箱")
    private String email;

    @ExcelProperty("出生日期")
    private String birthdayStr;

    @ExcelProperty("备注")
    private String remark;

    @ExcelProperty("状态*")
    private String disableFlagStr;

    // ========== 非excel中数据 ===========

    private Long birthday;
    private SexEnum sex;
    private Boolean disableFlag;
}
