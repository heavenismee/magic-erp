package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.MarketingManagerDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 营销经理VO
 *
 * @author zs
 * @since 2023-12-11 11:25:31
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class MarketingManagerVO extends MarketingManagerDO {

    @ApiModelProperty(name = "dept_name", value = "部门名称")
    private String deptName;

}

