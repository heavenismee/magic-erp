package com.hys.app.model.erp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 订单支付状态枚举
 *
 * @author zs
 * @since 2024-02-07
 */
@Getter
@AllArgsConstructor
public enum OrderPaymentStatusEnum {

    /**
     * 未支付
     */
    NOT_PAY("未支付"),
    /**
     * 已支付
     */
    PAY("已支付"),
    ;

    private final String text;

}
