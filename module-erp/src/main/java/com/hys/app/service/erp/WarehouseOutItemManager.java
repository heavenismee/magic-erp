package com.hys.app.service.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.WarehouseOutItemDO;
import com.hys.app.model.erp.dto.WarehouseOutItemQueryParams;
import com.hys.app.model.erp.vo.WarehouseOutItemVO;

import java.util.List;

/**
 * 出库单商品明细业务层接口
 *
 * @author zs
 * @since 2023-12-07 17:06:32
 */
public interface WarehouseOutItemManager extends BaseService<WarehouseOutItemDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<WarehouseOutItemVO> list(WarehouseOutItemQueryParams queryParams);

    /**
     * 根据出库单id查询
     *
     * @param warehouseOutId
     * @return
     */
    List<WarehouseOutItemDO> listByWarehouseOutId(Long warehouseOutId);

    /**
     * 根据出库单id删除
     *
     * @param warehouseOutIds
     */
    void deleteByWarehouseOutId(List<Long> warehouseOutIds);
}

