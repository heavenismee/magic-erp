package com.hys.app.service.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.StockBatchFlowDO;
import com.hys.app.model.erp.vo.StockBatchFlowVO;
import com.hys.app.model.erp.dto.StockBatchFlowDTO;
import com.hys.app.model.erp.dto.StockBatchFlowQueryParams;
import com.hys.app.framework.database.WebPage;
import java.util.List;

/**
 * 库存批次流水业务层接口
 *
 * @author zsong
 * 2024-01-16 11:39:01
 */
public interface StockBatchFlowManager extends BaseService<StockBatchFlowDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<StockBatchFlowVO> list(StockBatchFlowQueryParams queryParams);

    /**
     * 添加
     * @param stockBatchFlowDTO 新增数据
     */
    void add(StockBatchFlowDTO stockBatchFlowDTO);

    /**
     * 编辑
     * @param stockBatchFlowDTO 更新数据
     */
    void edit(StockBatchFlowDTO stockBatchFlowDTO);

    /**
     * 查询详情
     * @param id 主键
     * @return 详情数据
     */
    StockBatchFlowVO getDetail(Long id);

    /**
     * 删除
     * @param ids 主键集合
     */
    void delete(List<Long> ids);
}

