package com.hys.app.service.erp.impl;

import com.hys.app.converter.erp.WarehouseEntryProductConverter;
import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseServiceImpl;
import com.hys.app.mapper.erp.WarehouseEntryProductMapper;
import com.hys.app.model.erp.dos.WarehouseEntryProductDO;
import com.hys.app.model.erp.dto.WarehouseEntryProductDTO;
import com.hys.app.model.erp.dto.WarehouseEntryProductQueryParams;
import com.hys.app.model.erp.vo.WarehouseEntryProductVO;
import com.hys.app.service.erp.WarehouseEntryProductManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 入库单商品明细业务层实现
 *
 * @author zs
 * @since 2023-12-05 15:30:10
 */
@Service
public class WarehouseEntryProductManagerImpl extends BaseServiceImpl<WarehouseEntryProductMapper, WarehouseEntryProductDO> implements WarehouseEntryProductManager {

    @Autowired
    private WarehouseEntryProductConverter converter;

    @Override
    public WebPage<WarehouseEntryProductVO> list(WarehouseEntryProductQueryParams queryParams) {
        WebPage<WarehouseEntryProductDO> webPage = baseMapper.selectPage(queryParams);
        return converter.convert(webPage);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void add(WarehouseEntryProductDTO warehouseEntryProductDTO) {
        save(converter.convert(warehouseEntryProductDTO));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void edit(WarehouseEntryProductDTO warehouseEntryProductDTO) {
        updateById(converter.convert(warehouseEntryProductDTO));
    }

    @Override
    public WarehouseEntryProductVO getDetail(Long id) {
        return converter.convert(getById(id));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Long id) {
        removeById(id);
    }

    @Override
    public void deleteByWarehouseEntryId(Long warehouseEntryId) {
        lambdaUpdate().eq(WarehouseEntryProductDO::getWarehouseEntryId, warehouseEntryId).remove();
    }

    @Override
    public void deleteByWarehouseEntryIds(List<Long> warehouseEntryIds) {
        lambdaUpdate().in(WarehouseEntryProductDO::getWarehouseEntryId, warehouseEntryIds).remove();
    }

    @Override
    public List<WarehouseEntryProductDO> listByWarehouseEntryId(Long warehouseEntryId) {
        return lambdaQuery().eq(WarehouseEntryProductDO::getWarehouseEntryId, warehouseEntryId).list();
    }

    @Override
    public List<WarehouseEntryProductDO> listByWarehouseEntryIds(List<Long> warehouseEntryIds) {
        return lambdaQuery()
                .in(WarehouseEntryProductDO::getWarehouseEntryId, warehouseEntryIds)
                .list();
    }

    @Override
    public void updateReturnNum(Long id, Integer returnNum) {
        lambdaUpdate()
                .setSql("return_num = return_num + " + returnNum)
                .eq(WarehouseEntryProductDO::getId, id)
                .update();
    }

}

