package com.hys.app.service.erp.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.exception.ServiceException;
import com.hys.app.framework.util.BeanUtil;
import com.hys.app.framework.util.PageConvert;
import com.hys.app.mapper.erp.LendFormProductMapper;
import com.hys.app.model.erp.dos.LendFormProduct;
import com.hys.app.model.erp.dto.LendFormProductDTO;
import com.hys.app.model.erp.dto.LendFormProductReturnDTO;
import com.hys.app.model.erp.dto.ProductLendNumDTO;
import com.hys.app.service.erp.LendFormProductManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品借出单业务接口实现
 * @author dmy
 * 2023-12-05
 */
@Service
public class LendFormProductManagerImpl extends ServiceImpl<LendFormProductMapper, LendFormProduct> implements LendFormProductManager {

    @Autowired
    private LendFormProductMapper lendFormProductMapper;

    /**
     * 新增采购计划产品信息
     *
     * @param lendId 借出单ID
     * @param productList 产品信息集合
     */
    @Override
    @Transactional(value = "goodsTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void saveProduct(Long lendId, List<LendFormProductDTO> productList) {
        //先删除旧数据
        List<Long> lendIds = new ArrayList<>();
        lendIds.add(lendId);
        this.deleteProduct(lendIds);

        //再循环将商品信息入库
        for (LendFormProductDTO productDTO : productList) {
            LendFormProduct product = new LendFormProduct();
            BeanUtil.copyProperties(productDTO, product);
            product.setLendId(lendId);
            this.save(product);
        }
    }

    /**
     * 删除借出单产品信息
     *
     * @param lendIds 借出单ID集合
     */
    @Override
    @Transactional(value = "goodsTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void deleteProduct(List<Long> lendIds) {
        LambdaQueryWrapper<LendFormProduct> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(LendFormProduct::getLendId, lendIds);
        this.lendFormProductMapper.delete(wrapper);
    }

    /**
     * 根据借出单ID获取借出单产品信息集合
     * @param lendId 借出单ID
     * @return
     */
    @Override
    public List<LendFormProduct> list(Long lendId) {
        return this.lambdaQuery()
                .eq(LendFormProduct::getLendId, lendId)
                .list();
    }

    /**
     * 修改借出单产品归还数量
     *
     * @param productList
     */
    @Override
    @Transactional(value = "goodsTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void updateProductReturnNum(List<LendFormProductReturnDTO> productList) {
        for (LendFormProductReturnDTO returnDTO : productList) {
            //根据主键ID获取借出单产品信息
            LendFormProduct product = this.getById(returnDTO.getId());
            if (product.getLendNum() < returnDTO.getReturnNum()) {
                throw new ServiceException("归还数量不能大于借出数量");
            }
            product.setReturnNum(returnDTO.getReturnNum());
            this.updateById(product);
        }
    }

    /**
     * 查询待归还的借出单商品分页列表数据
     *
     * @param pageNo 页数
     * @param pageSize 每页数量
     * @return
     */
    @Override
    public WebPage listWaitReturnProductPage(Long pageNo, Long pageSize) {
        IPage<LendFormProduct> iPage = this.lendFormProductMapper.selectWaitReturnProductPage(new Page(pageNo, pageSize));
        return PageConvert.convert(iPage);
    }

    /**
     * 查询产品借出数量信息
     *
     * @param lendList 借出数量信息
     * @return
     */
    @Override
    public List<ProductLendNumDTO> listProductLendNum(List<ProductLendNumDTO> lendList) {
        for (ProductLendNumDTO productLendNumDTO : lendList) {
            //根据入库单号和产品ID查询借出商品信息
            List<LendFormProduct> products = this.lendFormProductMapper.selectByConfirm(productLendNumDTO.getStockSn(), productLendNumDTO.getProductId());
            int lendNum = 0;
            if (products != null && products.size() != 0) {
                lendNum = products.stream().mapToInt(LendFormProduct::getLendNum).sum();
                productLendNumDTO.setLendNum(lendNum);
                productLendNumDTO.setRemark("借出" + lendNum + products.get(0).getUnit());
            }
        }
        return lendList;
    }
}
