package com.hys.app.service.erp;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dos.ProcurementPlan;
import com.hys.app.model.erp.dto.ProcurementPlanDTO;
import com.hys.app.model.erp.dto.ProcurementPlanQueryParam;
import com.hys.app.model.erp.vo.ProcurementPlanVO;

import java.util.List;

/**
 * 采购计划业务接口
 * @author dmy
 * 2023-12-05
 */
public interface ProcurementPlanManager extends IService<ProcurementPlan> {

    /**
     * 查询采购计划分页列表数据
     *
     * @param params 查询参数
     * @return 分页数据
     */
    WebPage list(ProcurementPlanQueryParam params);

    /**
     * 新增采购计划
     *
     * @param procurementPlanDTO 采购计划信息
     */
    void add(ProcurementPlanDTO procurementPlanDTO);

    /**
     * 编辑采购计划
     *
     * @param id 主键ID
     * @param procurementPlanDTO 采购计划信息
     */
    void edit(Long id, ProcurementPlanDTO procurementPlanDTO);

    /**
     * 删除采购计划
     * @param ids 采购计划主键ID集合
     */
    void delete(List<Long> ids);

    /**
     * 根据编号获取采购计划信息
     *
     * @param id 主键ID
     * @return
     */
    ProcurementPlanVO getDetail(Long id);

    /**
     * 获取所有采购计划信息
     *
     * @return
     */
    List<ProcurementPlan> listAll();
}
