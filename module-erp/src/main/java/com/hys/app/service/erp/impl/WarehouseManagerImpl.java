package com.hys.app.service.erp.impl;

import com.hys.app.converter.erp.WarehouseConverter;
import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.exception.ServiceException;
import com.hys.app.framework.database.mybatisplus.base.BaseServiceImpl;
import com.hys.app.mapper.erp.WarehouseMapper;
import com.hys.app.model.erp.dos.WarehouseDO;
import com.hys.app.model.erp.dto.WarehouseDTO;
import com.hys.app.model.erp.dto.WarehouseQueryParams;
import com.hys.app.model.erp.vo.WarehouseVO;
import com.hys.app.model.system.dos.DeptDO;
import com.hys.app.service.erp.*;
import com.hys.app.service.system.AdminUserManager;
import com.hys.app.service.system.DeptManager;
import com.hys.app.util.ValidateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

import static com.hys.app.framework.util.CollectionUtils.convertList;

/**
 * 仓库业务层实现
 *
 * @author zs
 * 2023-11-30 16:35:16
 */
@Service
public class WarehouseManagerImpl extends BaseServiceImpl<WarehouseMapper, WarehouseDO> implements WarehouseManager {

    @Autowired
    private WarehouseConverter converter;

    @Autowired
    private DeptManager deptManager;

    @Autowired
    private ProductStockManager productStockManager;

    @Autowired
    private WarehouseEntryManager warehouseEntryManager;

    @Autowired
    private WarehouseOutManager warehouseOutManager;

    @Autowired
    private StockTransferManager stockTransferManager;

    @Autowired
    private AdminUserManager adminUserManager;

    @Override
    public WebPage<WarehouseVO> list(WarehouseQueryParams queryParams) {
        WebPage<WarehouseDO> webPage = baseMapper.selectPage(queryParams);

        List<WarehouseDO> data = webPage.getData();
        Map<Long, String> deptNameMap = deptManager.listAndConvertMap(convertList(data, WarehouseDO::getDeptId), DeptDO::getId, DeptDO::getName);
        Map<Long, String> adminNameMap = adminUserManager.getNameMapByIds(convertList(data, WarehouseDO::getAdminId));

        return converter.combination(webPage, deptNameMap, adminNameMap);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void add(WarehouseDTO warehouseDTO) {
        check(warehouseDTO);
        WarehouseDO warehouseDO = converter.convert(warehouseDTO);
        save(warehouseDO);
        productStockManager.initStockByWarehouse(warehouseDO.getId());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void edit(WarehouseDTO warehouseDTO) {
        check(warehouseDTO);
        updateById(converter.convert(warehouseDTO));
    }

    @Override
    public WarehouseVO getDetail(Long id) {
        return converter.convert(getById(id));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Long id) {
        if (warehouseEntryManager.countByWarehouseId(id) > 0) {
            throw new ServiceException("该仓库已关联入库单，不能进行删除操作");
        }
        if (warehouseOutManager.countByWarehouseId(id) > 0) {
            throw new ServiceException("该仓库已关联出库单，不能进行删除操作");
        }
        if (stockTransferManager.countByWarehouseId(id) > 0) {
            throw new ServiceException("该仓库已关联调拨单，不能进行删除操作");
        }

        removeById(id);
        productStockManager.deleteStockByWarehouse(id);
    }

    @Override
    public List<WarehouseVO> listAll(Long deptId) {
        List<WarehouseDO> list = baseMapper.listAll(deptId);
        return converter.convert(list);
    }

    @Override
    public long countByDeptId(Long deptId) {
        return lambdaQuery().eq(WarehouseDO::getDeptId, deptId).count();
    }

    private void check(WarehouseDTO warehouseDTO) {
        if (ValidateUtil.checkRepeat(baseMapper, WarehouseDO::getSn, warehouseDTO.getSn(), WarehouseDO::getId, warehouseDTO.getId())) {
            throw new ServiceException("仓库编号已存在");
        }
    }

}

