package com.hys.app.service.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.WarehouseEntryDO;
import com.hys.app.model.erp.dto.WarehouseEntryDTO;
import com.hys.app.model.erp.dto.WarehouseEntryQueryParams;
import com.hys.app.model.erp.dto.WarehouseEntryStatisticsParam;
import com.hys.app.model.erp.enums.WarehouseEntryStatusEnum;
import com.hys.app.model.erp.vo.WarehouseEntryVO;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 入库单业务层接口
 *
 * @author zs
 * @since 2023-12-05 11:25:07
 */
public interface WarehouseEntryManager extends BaseService<WarehouseEntryDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<WarehouseEntryVO> list(WarehouseEntryQueryParams queryParams);

    /**
     * 添加
     *
     * @param warehouseEntryDTO
     */
    void add(WarehouseEntryDTO warehouseEntryDTO);

    /**
     * 编辑
     *
     * @param warehouseEntryDTO
     */
    void edit(WarehouseEntryDTO warehouseEntryDTO);

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    WarehouseEntryVO getDetail(Long id);

    /**
     * 删除
     *
     * @param id
     */
    void delete(List<Long> ids);

    /**
     * 提交入库单
     *
     * @param ids
     */
    void submit(List<Long> ids);

    /**
     * 撤销提交入库单
     *
     * @param id
     */
    void withdraw(Long id);

    /**
     * 审核入库单
     *
     * @param ids
     * @param status
     * @param remark
     */
    void audit(List<Long> ids, WarehouseEntryStatusEnum status, String remark);

    /**
     * 结算完成
     *
     * @param supplierSettlementId 供应商结算单id
     * @param ids
     */
    void settlementComplete(Long supplierSettlementId, List<Long> ids);

    /**
     * 取消结算
     * @param supplierSettlementId
     */
    void settlementCancel(Long supplierSettlementId);

    /**
     * 查询入库单统计分页列表数据
     *
     * @param params 查询参数
     * @return
     */
    WebPage statistics(WarehouseEntryStatisticsParam params);

    /**
     * 导出入库单统计列表数据
     *
     * @param response
     * @param params 查询参数
     */
    void export(HttpServletResponse response, WarehouseEntryStatisticsParam params);

    /**
     * 判断合同是否已被入库单占用
     *
     * @param contractIds 合同ID集合
     * @return
     */
    boolean checkContractIsUsed(List<Long> contractIds);

    /**
     * 根据仓库id查询数量
     * @param warehouseId
     * @return
     */
    long countByWarehouseId(Long warehouseId);

    /**
     * 判断采购计划是否已被入库单占用
     *
     * @param planIds 采购计划ID集合
     * @return
     */
    boolean checkPlanIsUsed(List<Long> planIds);
}

