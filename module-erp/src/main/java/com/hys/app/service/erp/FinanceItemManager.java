package com.hys.app.service.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.FinanceItemDO;
import com.hys.app.model.erp.dto.FinanceItemDTO;
import com.hys.app.model.erp.enums.FinanceExpandTypeEnum;
import com.hys.app.model.erp.enums.FinanceIncomeTypeEnum;
import com.hys.app.model.erp.vo.FinanceItemVO;
import com.hys.app.model.erp.dto.FinanceItemQueryParams;
import com.hys.app.framework.database.WebPage;
import java.util.List;

/**
 * 财务明细业务层接口
 *
 * @author zsong
 * 2024-03-15 17:40:23
 */
public interface FinanceItemManager extends BaseService<FinanceItemDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<FinanceItemVO> list(FinanceItemQueryParams queryParams);

    /**
     * 查询详情
     * @param id 主键
     * @return 详情数据
     */
    FinanceItemVO getDetail(Long id);

    /**
     * 删除
     * @param ids 主键集合
     */
    void delete(List<Long> ids);

    /**
     * 新增收入明细
     *
     * @param type
     * @param sourceSn
     * @param price
     */
    void addIncome(FinanceIncomeTypeEnum type, String sourceSn, Double price);

    /**
     * 新增支出明细
     *
     * @param type
     * @param sourceSn
     * @param price
     */
    void addExpand(FinanceExpandTypeEnum type, String sourceSn, Double price);

    /**
     * 后台添加明细
     * @param financeItemDTO
     */
    void add(FinanceItemDTO financeItemDTO);
}

