package com.hys.app.service.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.StockTransferProductDO;
import com.hys.app.model.erp.vo.StockTransferProductVO;
import com.hys.app.model.erp.dto.StockTransferProductDTO;
import com.hys.app.model.erp.dto.StockTransferProductQueryParams;
import com.hys.app.framework.database.WebPage;

import java.util.List;

/**
 * 调拨单商品明细业务层接口
 *
 * @author zs
 * @since 2023-12-12 16:34:15
 */
public interface StockTransferProductManager extends BaseService<StockTransferProductDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<StockTransferProductVO> list(StockTransferProductQueryParams queryParams);

    /**
     * 添加
     * @param stockTransferProductDTO
     */
    void add(StockTransferProductDTO stockTransferProductDTO);

    /**
     * 编辑
     * @param stockTransferProductDTO
     */
    void edit(StockTransferProductDTO stockTransferProductDTO);

    /**
     * 查询详情
     * @param id
     * @return 
     */
    StockTransferProductVO getDetail(Long id);

    /**
     * 删除
     * @param id
     */
    void delete(Long id);

    /**
     * 根据调拨单id删除
     * @param stockTransferIds
     */
    void deleteByStockTransferId(List<Long> stockTransferIds);

    /**
     * 根据调拨单id查询
     * @param stockTransferId
     * @return
     */
    List<StockTransferProductDO> listByStockTransferId(Long stockTransferId);
}

