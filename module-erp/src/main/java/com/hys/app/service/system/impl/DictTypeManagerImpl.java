package com.hys.app.service.system.impl;

import cn.hutool.core.util.StrUtil;
import com.hys.app.model.system.dto.DictTypeCreateDTO;
import com.hys.app.model.system.dto.DictTypeQueryParams;
import com.hys.app.model.system.dto.DictTypeUpdateDTO;
import com.hys.app.converter.system.DictTypeConverter;
import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.exception.ServiceException;
import com.hys.app.framework.util.DateUtil;
import com.hys.app.mapper.system.DictTypeMapper;
import com.hys.app.model.system.dos.DictTypeDO;
import com.hys.app.service.system.DictDataManager;
import com.hys.app.service.system.DictTypeManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 字典类型 Service 实现类
 *
 * @author zs
 * @since 2023-11-28
 */
@Service
public class DictTypeManagerImpl implements DictTypeManager {

    @Resource
    private DictDataManager dictDataManager;

    @Resource
    private DictTypeMapper dictTypeMapper;

    @Autowired
    private DictTypeConverter converter;

    @Override
    public WebPage<DictTypeDO> getDictTypePage(DictTypeQueryParams reqVO) {
        return dictTypeMapper.selectPage(reqVO);
    }

    @Override
    public DictTypeDO getDictType(Long id) {
        return dictTypeMapper.selectById(id);
    }

    @Override
    public DictTypeDO getDictType(String type) {
        return dictTypeMapper.selectByType(type);
    }

    @Override
    public Long createDictType(DictTypeCreateDTO reqVO) {
        // 校验正确性
        validateDictTypeForCreateOrUpdate(null, reqVO.getName(), reqVO.getType());

        // 插入字典类型
        DictTypeDO dictType = converter.convert(reqVO)
                .setDeletedTime(DateUtil.getDateline());
        dictTypeMapper.insert(dictType);
        return dictType.getId();
    }

    @Override
    public void updateDictType(DictTypeUpdateDTO reqVO) {
        // 校验正确性
        validateDictTypeForCreateOrUpdate(reqVO.getId(), reqVO.getName(), null);

        // 更新字典类型
        DictTypeDO updateObj = converter.convert(reqVO);
        dictTypeMapper.updateById(updateObj);
    }

    @Override
    public void deleteDictType(Long id) {
        // 校验是否存在
        DictTypeDO dictType = validateDictTypeExists(id);
        // 校验是否有字典数据
        if (dictDataManager.countByDictType(dictType.getType()) > 0) {
            throw new ServiceException("无法删除，该字典类型还有字典数据");
        }
        // 删除字典类型
        dictTypeMapper.updateToDelete(id, DateUtil.getDateline());
    }

    @Override
    public List<DictTypeDO> getDictTypeList() {
        return dictTypeMapper.selectList();
    }

    private void validateDictTypeForCreateOrUpdate(Long id, String name, String type) {
        // 校验自己存在
        validateDictTypeExists(id);
        // 校验字典类型的名字的唯一性
        validateDictTypeNameUnique(id, name);
        // 校验字典类型的类型的唯一性
        validateDictTypeUnique(id, type);
    }

    private void validateDictTypeNameUnique(Long id, String name) {
        DictTypeDO dictType = dictTypeMapper.selectByName(name);
        if (dictType == null) {
            return;
        }
        // 如果 id 为空，说明不用比较是否为相同 id 的字典类型
        if (id == null) {
            throw new ServiceException("已经存在该名字的字典类型");
        }
        if (!dictType.getId().equals(id)) {
            throw new ServiceException("已经存在该名字的字典类型");
        }
    }

    private void validateDictTypeUnique(Long id, String type) {
        if (StrUtil.isEmpty(type)) {
            return;
        }
        DictTypeDO dictType = dictTypeMapper.selectByType(type);
        if (dictType == null) {
            return;
        }
        // 如果 id 为空，说明不用比较是否为相同 id 的字典类型
        if (id == null) {
            throw new ServiceException("已经存在该类型的字典类型");
        }
        if (!dictType.getId().equals(id)) {
            throw new ServiceException("已经存在该类型的字典类型");
        }
    }

    private DictTypeDO validateDictTypeExists(Long id) {
        if (id == null) {
            return null;
        }
        DictTypeDO dictType = dictTypeMapper.selectById(id);
        if (dictType == null) {
            throw new ServiceException("当前字典类型不存在");
        }
        return dictType;
    }

}
