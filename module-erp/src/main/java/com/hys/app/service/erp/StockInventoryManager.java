package com.hys.app.service.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.StockInventoryDTO;
import com.hys.app.model.erp.dto.StockInventoryParam;
import com.hys.app.model.erp.vo.StockInventoryVO;

import java.util.List;

/**
 * 库存盘点单业务接口
 *
 * @author dmy
 * 2023-12-05
 */
public interface StockInventoryManager {

    /**
     * 查询库存盘点单分页列表数据
     *
     * @param params 查询参数
     * @return
     */
    WebPage list(StockInventoryParam params);

    /**
     * 新增库存盘点单
     *
     * @param stockInventoryDTO 库存盘点单信息
     */
    void add(StockInventoryDTO stockInventoryDTO);

    /**
     * 编辑库存盘点单
     *
     * @param id 主键ID
     * @param stockInventoryDTO 库存盘点单信息
     */
    void edit(Long id, StockInventoryDTO stockInventoryDTO);

    /**
     * 删除库存盘点单
     *
     * @param ids 库存盘点单主键ID集合
     */
    void delete(List<Long> ids);

    /**
     * 获取库存盘点单详情
     *
     * @param id 主键ID
     * @return
     */
    StockInventoryVO getDetail(Long id);

    /**
     * 库存盘点单提交审核
     *
     * @param ids 库存盘点单主键ID集合
     */
    void submit(List<Long> ids);

    /**
     * 库存盘点单撤销提交审核
     *
     * @param ids 库存盘点单主键ID集合
     */
    void cancel(List<Long> ids);

    /**
     * 审核库存盘点单
     *
     * @param ids 库存盘点单ID集合
     * @param status 审核状态 PASS：审核通过，REJECT：审核驳回
     * @param rejectReason 驳回原因
     */
    void audit(List<Long> ids, String status, String rejectReason);
}
