package com.hys.app.service.system;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.system.dos.AdminUser;
import com.hys.app.model.system.vo.AdminLoginVO;
import com.hys.app.model.system.vo.AdminUserVO;

import java.util.List;
import java.util.Map;

/**
 * 平台管理员业务层
 *
 * @author zh
 * @version v7.0
 * @since v7.0.0
 * 2018-06-20 20:38:26
 */
public interface AdminUserManager {

    /**
     * 查询平台管理员列表
     *
     * @param page      页码
     * @param pageSize  每页数量
     * @param userState
     * @return WebPage
     */
    WebPage list(long page, long pageSize, String keyword, Integer userState);

    /**
     * 添加平台管理员
     *
     * @param adminUserVO 平台管理员
     * @return AdminUser 平台管理员
     */
    AdminUser add(AdminUserVO adminUserVO);

    /**
     * 修改平台管理员
     *
     * @param adminUserVO 平台管理员
     * @param id          平台管理员主键
     * @return AdminUser 平台管理员
     */
    AdminUser edit(AdminUserVO adminUserVO, Long id);

    /**
     * 删除平台管理员
     *
     * @param id 平台管理员主键
     */
    void delete(Long id);

    /**
     * 获取平台管理员
     *
     * @param id 平台管理员主键
     * @return AdminUser  平台管理员
     */
    AdminUser getModel(Long id);

    /**
     * 获取未删除的平台管理员
     *
     * @param id 平台管理员主键
     * @return AdminUser  平台管理员
     */
    AdminUser getModelNormalStatus(Long id);

    /**
     * 管理员登录
     *
     * @param name     管理员名称
     * @param password 管理员密码
     * @return
     */
    AdminLoginVO login(String name, String password);

    /**
     * 通过refreshToken重新获取accessToken
     *
     * @param refreshToken
     * @return
     */
    String exchangeToken(String refreshToken);

    /**
     * 管理员注销登录
     *
     * @param uid 会员id
     */
    void logout(Long uid);


    /**
     * 根据部门id查询数量
     * @param deptId
     * @return
     */
    Long countByDeptId(Long deptId);

    /**
     * 获取当前登录用户
     * @return
     */
    AdminUser getCurrUser();

    /**
     * 恢复平台管理员
     * @param id
     */
    void recover(Long id);

    /**
     * 根据岗位id查询数量
     * @param postId
     * @return
     */
    long countByPostId(Long postId);

    /**
     * 查询当前用户名称
     * @return
     */
    String getCurrUserName();

    /**
     * 批量查询管理员名称
     * @param ids
     * @return
     */
    Map<Long, String> getNameMapByIds(List<Long> ids);
}
