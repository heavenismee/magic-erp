package com.hys.app.service.erp.impl;

import com.hys.app.converter.erp.OrderPaymentConverter;
import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseServiceImpl;
import com.hys.app.mapper.erp.OrderPaymentMapper;
import com.hys.app.model.erp.dos.OrderPaymentDO;
import com.hys.app.model.erp.dto.OrderPaymentQueryParams;
import com.hys.app.model.erp.vo.OrderPaymentVO;
import com.hys.app.service.erp.OrderPaymentManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 订单支付明细业务层实现
 *
 * @author zsong
 * 2024-01-24 17:00:32
 */
@Service
public class OrderPaymentManagerImpl extends BaseServiceImpl<OrderPaymentMapper, OrderPaymentDO> implements OrderPaymentManager {

    @Autowired
    private OrderPaymentConverter converter;

    @Override
    public WebPage<OrderPaymentVO> list(OrderPaymentQueryParams queryParams) {
        WebPage<OrderPaymentDO> page = baseMapper.selectPage(queryParams);
        return converter.convertPage(page);
    }

    @Override
    public OrderPaymentVO getDetail(Long id) {
        OrderPaymentDO orderPaymentDO = getById(id);
        return converter.convert(orderPaymentDO);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(List<Long> ids) {
        removeBatchByIds(ids);
    }

    @Override
    public void deleteByOrderId(Long orderId) {
        lambdaUpdate().eq(OrderPaymentDO::getOrderId, orderId).remove();
    }

    @Override
    public List<OrderPaymentDO> listByOrderId(Long orderId) {
        return lambdaQuery().eq(OrderPaymentDO::getOrderId, orderId).list();
    }

}

