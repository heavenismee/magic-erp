package com.hys.app.service.system.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.util.PageConvert;
import com.hys.app.mapper.system.SmsSendRecordMapper;
import com.hys.app.model.system.dos.SmsSendRecord;
import com.hys.app.model.system.enums.SmsRecordStatusEnum;
import com.hys.app.service.system.SmsSendRecordManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 短信发送记录业务类
 * @author zhangsong
 * @version v1.0
 * @since v1.0
 * 2021-02-05 17:17:30
 */
@Service
public class SmsSendRecordManagerImpl implements SmsSendRecordManager {

	@Autowired
	private SmsSendRecordMapper smsSendRecordMapper;

	@Override
	public WebPage list(Long pageNo, Long pageSize){

		//新建查询条件包装器
		QueryWrapper<SmsSendRecord> wrapper = new QueryWrapper<>();

        IPage<SmsSendRecord> iPage = smsSendRecordMapper.selectPage(new Page<>(pageNo, pageSize), wrapper);

		return PageConvert.convert(iPage);
	}

	@Override
	public SmsSendRecord add(SmsSendRecord smsSendRecord)	{
		this.smsSendRecordMapper.insert(smsSendRecord);

		return smsSendRecord;
	}

	@Override
	public SmsSendRecord getModel(Long id)	{
		return this.smsSendRecordMapper.selectById(id);
	}

	@Override
	public void smsSendCallback(String bizId, String isSuccess) {

		SmsSendRecord smsSendRecord = this.getModel(bizId);

		if(smsSendRecord == null){
			return;
		}

		if("true".equals(isSuccess)){
			smsSendRecord.setSendStatus(SmsRecordStatusEnum.SUCCESS.value());
		}else{
			smsSendRecord.setSendStatus(SmsRecordStatusEnum.FAIL.value());
		}

		smsSendRecordMapper.updateById(smsSendRecord);

	}

	private SmsSendRecord getModel(String bizId) {
		return new QueryChainWrapper<>(smsSendRecordMapper).eq("biz_id", bizId).one();
	}
}
