package com.hys.app.service.erp.impl;

import com.hys.app.framework.database.mybatisplus.base.BaseServiceImpl;
import com.hys.app.framework.database.WebPage;
import com.hys.app.mapper.erp.StockBatchFlowMapper;
import com.hys.app.model.erp.dos.StockBatchFlowDO;
import com.hys.app.model.erp.vo.StockBatchFlowVO;
import com.hys.app.model.erp.dto.StockBatchFlowDTO;
import com.hys.app.converter.erp.StockBatchFlowConverter;
import com.hys.app.service.erp.StockBatchFlowManager;
import com.hys.app.model.erp.dto.StockBatchFlowQueryParams;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

/**
 * 库存批次流水业务层实现
 *
 * @author zsong
 * 2024-01-16 11:39:01
 */
@Service
public class StockBatchFlowManagerImpl extends BaseServiceImpl<StockBatchFlowMapper, StockBatchFlowDO> implements StockBatchFlowManager {
    
    @Autowired
    private StockBatchFlowConverter converter;
    
    @Override
    public WebPage<StockBatchFlowVO> list(StockBatchFlowQueryParams queryParams) {
        WebPage<StockBatchFlowDO> page = baseMapper.selectPage(queryParams);
        return converter.convertPage(page);
    }
    
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void add(StockBatchFlowDTO stockBatchFlowDTO) {
        // 校验参数
        checkParams(stockBatchFlowDTO);
        
        // 保存
        StockBatchFlowDO stockBatchFlowDO = converter.convert(stockBatchFlowDTO);
        save(stockBatchFlowDO);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void edit(StockBatchFlowDTO stockBatchFlowDTO) {
        // 校验参数
        checkParams(stockBatchFlowDTO);
        
        // 更新
        StockBatchFlowDO stockBatchFlowDO = converter.convert(stockBatchFlowDTO);
        updateById(stockBatchFlowDO);
    }

    @Override
    public StockBatchFlowVO getDetail(Long id) {
        StockBatchFlowDO stockBatchFlowDO = getById(id);
        return converter.convert(stockBatchFlowDO);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(List<Long> ids) {
        removeBatchByIds(ids);
    }
    
    private void checkParams(StockBatchFlowDTO stockBatchFlowDTO) {
        
    }
}

