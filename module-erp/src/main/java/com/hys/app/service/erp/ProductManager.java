package com.hys.app.service.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.GoodsDO;
import com.hys.app.model.erp.dos.ProductDO;
import com.hys.app.model.erp.dto.ProductDTO;
import com.hys.app.model.erp.dto.ProductQueryParams;
import com.hys.app.model.erp.vo.ProductVO;

import java.util.List;

/**
 * 产品业务层接口
 *
 * @author zs
 * 2023-11-30 16:06:44
 */
public interface ProductManager extends BaseService<ProductDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<ProductVO> list(ProductQueryParams queryParams);

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    ProductVO getDetail(Long id);

    /**
     * 添加
     *
     * @param goodsDO
     * @param productList
     */
    void add(GoodsDO goodsDO, List<ProductDTO> productList);

    /**
     * 编辑
     *
     * @param goodsDO
     * @param productList
     */
    void edit(GoodsDO goodsDO, List<ProductDTO> productList);

    /**
     * 根据商品id查询
     *
     * @param goodsId
     * @return
     */
    List<ProductDO> listByGoodsId(Long goodsId);

    /**
     * 根据商品id删除
     *
     * @param goodsIds
     */
    void deleteByGoodsId(List<Long> goodsIds);
}

