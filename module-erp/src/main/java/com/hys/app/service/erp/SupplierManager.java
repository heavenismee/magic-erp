package com.hys.app.service.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.SupplierDO;
import com.hys.app.model.erp.dto.SupplierDTO;
import com.hys.app.model.erp.dto.SupplierQueryParams;
import com.hys.app.model.erp.vo.SupplierVO;

import java.util.List;

/**
 * 供应商业务层接口
 *
 * @author zs
 * 2023-11-29 14:20:18
 */
public interface SupplierManager extends BaseService<SupplierDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<SupplierVO> list(SupplierQueryParams queryParams);

    /**
     * 添加
     *
     * @param supplierDTO
     */
    void add(SupplierDTO supplierDTO);

    /**
     * 编辑
     *
     * @param supplierDTO
     */
    void edit(SupplierDTO supplierDTO);

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    SupplierVO getDetail(Long id);

    /**
     * 删除
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 所有正常状态的供应商
     *
     * @return
     */
    List<SupplierVO> normalStatusList();

    /**
     * 禁用/恢复
     *
     * @param id
     * @param disable
     */
    void updateDisable(Long id, Boolean disable);

}

