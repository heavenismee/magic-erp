package com.hys.app.service.base.service;

import com.hys.app.model.base.SceneType;
import com.hys.app.model.system.enums.SmsServiceCodeEnum;

import java.util.Map;

/**
 * 手机短信接口
 *
 * @author zh
 * @version v2.0
 * @since v7.0.0
 * 2018年3月19日 下午4:02:40
 */
public interface SmsManager {
    /**
     * 发送短信
     * @param phoneNumber 手机号
     * @param serviceCode 业务类型编号
     * @param valuesMap   模板变量
     */
    void send(String phoneNumber, SmsServiceCodeEnum serviceCode, Map<String, Object> valuesMap);

    /**
     * 验证手机验证码
     *
     * @param scene  业务场景
     * @param mobile 手机号码
     * @param code   手机验证码
     * @return 是否通过校验 true通过，false不通过
     */
    boolean valid(String scene, String mobile, String code);

    /**
     * 获取验证通过的手机号
     * @param scene
     * @param mobile
     * @return
     */
    String validMobile(String scene,String mobile);

    /**
     * 在缓存中记录验证码
     *
     * @param scene  业务场景
     * @param mobile 手机号码
     * @param code   手机验证码
     */
    void record(String scene, String mobile, String code);

    /**
     * 发送(发送手机短信)消息
     *
     * @param byName    操作，替换内容
     * @param mobile    手机号码
     * @param sceneType 操作类型
     */
    void sendSmsMessage(String byName, String mobile, SceneType sceneType);

}
