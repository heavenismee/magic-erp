package com.hys.app.service.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.WarehouseEntryBatchDO;
import com.hys.app.model.erp.dto.StockUpdateDTO;
import com.hys.app.model.erp.dto.WarehouseEntryBatchQueryParams;
import com.hys.app.model.erp.enums.StockChangeSourceEnum;
import com.hys.app.model.erp.vo.WarehouseEntryBatchVO;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 入库批次业务层接口
 *
 * @author zs
 * @since 2023-12-08 11:49:52
 */
public interface WarehouseEntryBatchManager extends BaseService<WarehouseEntryBatchDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<WarehouseEntryBatchVO> list(WarehouseEntryBatchQueryParams queryParams);

    /**
     * 查询商品的可出库批次
     *
     * @param warehouseId
     * @param productIds
     * @return
     */
    Map<Long, List<WarehouseEntryBatchDO>> listAvailableBatch(Long warehouseId, List<Long> productIds);

    /**
     * 根据入库单id查询
     *
     * @param warehouseEntryIds
     * @return
     */
    List<WarehouseEntryBatchDO> listByWarehouseEntryIds(Collection<Long> warehouseEntryIds);

    /**
     * 查询批次列表
     *
     * @param queryParams
     * @return
     */
    List<WarehouseEntryBatchVO> listAll(WarehouseEntryBatchQueryParams queryParams);

    /**
     * 创建入库批次
     *
     * @param sourceType 库存变更来源
     * @param sourceSn   来源单号
     * @param batchList  批次信息
     */
    void create(StockChangeSourceEnum sourceType, String sourceSn, List<WarehouseEntryBatchDO> batchList);

    /**
     * 更新批次库存
     *
     * @param sourceType 库存变更来源
     * @param sourceSn   来源单号
     * @param updateList 批次更新信息
     */
    void updateStock(StockChangeSourceEnum sourceType, String sourceSn, List<StockUpdateDTO> updateList);
}

