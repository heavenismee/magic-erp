package com.hys.app.service.erp.impl;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.hys.app.converter.erp.OrderConverter;
import com.hys.app.converter.erp.OrderItemConverter;
import com.hys.app.converter.erp.OrderPaymentConverter;
import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseServiceImpl;
import com.hys.app.framework.exception.ServiceException;
import com.hys.app.framework.util.CurrencyUtil;
import com.hys.app.framework.util.DateUtil;
import com.hys.app.mapper.erp.OrderMapper;
import com.hys.app.model.erp.dos.*;
import com.hys.app.model.erp.dto.*;
import com.hys.app.model.erp.enums.*;
import com.hys.app.model.erp.vo.OrderAllowable;
import com.hys.app.model.erp.vo.OrderVO;
import com.hys.app.model.goods.dos.BrandDO;
import com.hys.app.model.goods.dos.CategoryDO;
import com.hys.app.model.system.dos.DeptDO;
import com.hys.app.service.erp.*;
import com.hys.app.service.goods.BrandManager;
import com.hys.app.service.goods.CategoryManager;
import com.hys.app.service.system.DeptManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static com.hys.app.framework.util.CollectionUtils.convertList;
import static com.hys.app.framework.util.CollectionUtils.convertMap;

/**
 * 订单业务层实现
 *
 * @author zsong
 * 2024-01-24 15:58:31
 */
@Service
public class OrderManagerImpl extends BaseServiceImpl<OrderMapper, OrderDO> implements OrderManager {

    @Autowired
    private OrderConverter converter;

    @Autowired
    private OrderItemConverter itemConverter;

    @Autowired
    private MemberManager memberManager;

    @Autowired
    private WarehouseManager warehouseManager;

    @Autowired
    private MarketingManagerManager marketingManagerManager;

    @Autowired
    private StoreManager storeManager;

    @Autowired
    private ProductManager productManager;

    @Autowired
    private CollectingAccountManager collectingAccountManager;

    @Autowired
    private NoGenerateManager noGenerateManager;

    @Autowired
    private OrderItemManager orderItemManager;

    @Autowired
    private OrderPaymentConverter orderPaymentConverter;

    @Autowired
    private OrderPaymentManager orderPaymentManager;

    @Autowired
    private DeptManager deptManager;

    @Autowired
    private CategoryManager categoryManager;

    @Autowired
    private BrandManager brandManager;

    @Autowired
    private ProductStockManager productStockManager;

    @Autowired
    private FinanceItemManager financeItemManager;

    @Override
    public WebPage<OrderVO> list(OrderQueryParams queryParams) {
        WebPage<OrderDO> page = baseMapper.selectPage(queryParams);
        return converter.convertPage(page);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void add(OrderDTO orderDTO) {
        // 校验参数
        checkParams(orderDTO);

        // 保存订单
        OrderDO orderDO = converter.combination(orderDTO);
        orderDO.setSn(noGenerateManager.generate(NoBusinessTypeEnum.Order));
        orderDO.setStatus(OrderStatusEnum.WAIT_AUDIT);
        save(orderDO);

        // 保存订单项
        List<OrderItemDO> itemList = itemConverter.convert(orderDTO, orderDO);
        orderItemManager.saveBatch(itemList);

        // 保存支付信息
        List<OrderPaymentDO> collectingAccountList = orderPaymentConverter.convert(orderDO.getId(), orderDTO.getPaymentList(), orderDTO.getCollectingAccountMap());
        orderPaymentManager.saveBatch(collectingAccountList);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void edit(OrderDTO orderDTO) {
        // 校验是否可以编辑
        OrderDO old = getById(orderDTO.getId());
        if (!new OrderAllowable(old).getEdit()) {
            throw new ServiceException("当前订单状态不允许进行编辑操作");
        }

        // 校验参数
        checkParams(orderDTO);

        // 更新订单
        OrderDO orderDO = converter.combination(orderDTO);
        orderDO.setSn(noGenerateManager.generate(NoBusinessTypeEnum.Order));
        updateById(orderDO);
        if (orderDO.getPaymentStatus() == OrderPaymentStatusEnum.NOT_PAY) {
            lambdaUpdate().set(OrderDO::getPaymentTime, null).eq(OrderDO::getId, orderDO.getId()).update();
        }

        // 更新订单项
        List<OrderItemDO> itemList = itemConverter.convert(orderDTO, orderDO);
        orderItemManager.deleteByOrderId(orderDO.getId());
        orderItemManager.saveBatch(itemList);

        // 保存支付信息
        List<OrderPaymentDO> paymentList = orderPaymentConverter.convert(orderDO.getId(), orderDTO.getPaymentList(), orderDTO.getCollectingAccountMap());
        orderPaymentManager.deleteByOrderId(orderDO.getId());
        orderPaymentManager.saveBatch(paymentList);
    }

    @Override
    public OrderVO getDetail(Long id) {
        OrderDO orderDO = getById(id);

        // 查询订单项
        List<OrderItemDO> itemList = orderItemManager.listByOrderIds(Collections.singletonList(id));
        // 查询库存（编辑时回显剩余库存）
        List<Long> warehouseId = Collections.singletonList(orderDO.getWarehouseId());
        List<Long> productIds = convertList(itemList, OrderItemDO::getProductId);
        Map<Long, Integer> stockNumMap = productStockManager.queryStockNum(warehouseId, productIds);
        // 查询支付信息
        List<OrderPaymentDO> paymentList = orderPaymentManager.listByOrderId(id);
        // 查询门店信息
        StoreDO storeDO = orderDO.getDeliveryType() == OrderDeliveryType.self_pick ? storeManager.getById(orderDO.getStoreId()) : null;

        return converter.convert(orderDO, itemList, paymentList, storeDO, stockNumMap);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(List<Long> ids) {
        removeBatchByIds(ids);
    }

    @Override
    public void warehouseOut(List<Long> ids, Long warehouseOutId) {
        lambdaUpdate()
                .set(OrderDO::getWarehouseOutFlag, true)
                .set(OrderDO::getStatus, OrderStatusEnum.WAIT_SHIP)
                .set(OrderDO::getWarehouseOutId, warehouseOutId)
                .in(OrderDO::getId, ids)
                .update();
    }

    @Override
    public void warehouseOutDelete(List<Long> warehouseOutIds) {
        lambdaUpdate()
                .set(OrderDO::getWarehouseOutFlag, false)
                .set(OrderDO::getStatus, OrderStatusEnum.WAIT_WAREHOUSE_OUT)
                .set(OrderDO::getWarehouseOutId, null)
                .in(OrderDO::getWarehouseOutId, warehouseOutIds)
                .update();
    }

    @Override
    public void warehouseOutShip(WarehouseOutShipDTO shipDTO) {
        lambdaUpdate()
                .set(OrderDO::getStatus, OrderStatusEnum.SHIPPED)
                .set(OrderDO::getShipFlag, true)
                .set(OrderDO::getShipTime, DateUtil.getDateline())
                .set(OrderDO::getLogisticsCompanyId, shipDTO.getLogisticsCompanyId())
                .set(OrderDO::getLogisticsCompanyName, shipDTO.getLogisticsCompanyName())
                .set(OrderDO::getLogisticsTrackingNumber, shipDTO.getTrackingNumber())
                .eq(OrderDO::getWarehouseOutId, shipDTO.getId())
                .update();
    }

    @Override
    public void submit(List<Long> ids) {
        List<OrderDO> orderList = listByIds(ids);
        for (OrderDO orderDO : orderList) {
            if (!new OrderAllowable(orderDO).getSubmit()) {
                throw new ServiceException(StrUtil.format("订单【{}】不允许进行提交操作", orderDO.getSn()));
            }
        }

        lambdaUpdate().set(OrderDO::getStatus, OrderStatusEnum.WAIT_AUDIT).in(OrderDO::getId, ids).update();
    }

    @Override
    public void withdraw(List<Long> ids) {
        List<OrderDO> orderList = listByIds(ids);
        for (OrderDO orderDO : orderList) {
            if (!new OrderAllowable(orderDO).getWithdraw()) {
                throw new ServiceException(StrUtil.format("订单【{}】不允许进行撤回操作", orderDO.getSn()));
            }
        }

        lambdaUpdate().set(OrderDO::getStatus, OrderStatusEnum.WAIT_SUBMIT).in(OrderDO::getId, ids).update();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void audit(List<Long> ids, OrderStatusEnum status, String remark) {
        Assert.isTrue(status == OrderStatusEnum.WAIT_WAREHOUSE_OUT
                || status == OrderStatusEnum.AUDIT_REJECT, "审核参数错误");

        List<OrderDO> orderList = listByIds(ids);
        for (OrderDO orderDO : orderList) {
            if (!new OrderAllowable(orderDO).getAudit()) {
                throw new ServiceException(StrUtil.format("订单【{}】不允许进行审核操作", orderDO.getSn()));
            }
        }

        lambdaUpdate().set(OrderDO::getStatus, status).in(OrderDO::getId, ids).update();

        // 审核通过后，生成财务明细
        if (status == OrderStatusEnum.WAIT_WAREHOUSE_OUT) {
            for (OrderDO orderDO : orderList) {
                financeItemManager.addIncome(FinanceIncomeTypeEnum.OrderSale, orderDO.getSn(), CurrencyUtil.add(orderDO.getPayPrice(), orderDO.getDiscountPrice()));
                if (orderDO.getDiscountPrice() > 0) {
                    financeItemManager.addExpand(FinanceExpandTypeEnum.Promotion, orderDO.getSn(), orderDO.getDiscountPrice());
                }
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void payment(List<Long> ids) {
        List<OrderDO> orderList = listByIds(ids);
        for (OrderDO orderDO : orderList) {
            if (!new OrderAllowable(orderDO).getPayment()) {
                throw new ServiceException(StrUtil.format("订单【{}】不允许进行支付操作", orderDO.getSn()));
            }
        }

        lambdaUpdate()
                .set(OrderDO::getPaymentStatus, OrderPaymentStatusEnum.PAY)
                .set(OrderDO::getPaymentTime, DateUtil.getDateline())
                .in(OrderDO::getId, ids)
                .update();
    }

    private void checkParams(OrderDTO orderDTO) {
        // 校验会员
        orderDTO.setMemberDO(checkMember(orderDTO.getMemberId()));

        // 校验仓库
        orderDTO.setWarehouseDO(checkWarehouse(orderDTO.getWarehouseId()));

        // 校验部门
        orderDTO.setDeptDO(checkDept(orderDTO.getWarehouseDO().getDeptId()));

        // 校验销售经理
        orderDTO.setMarketingManagerDO(checkMarketing(orderDTO.getMarketingId()));

        // 校验门店
        orderDTO.setStoreDO(checkStore(orderDTO.getDeliveryType(), orderDTO.getStoreId()));

        // 校验商品
        orderDTO.setProductMap(checkProduct(orderDTO));

        // 校验收款账户
        orderDTO.setCollectingAccountMap(checkPayment(orderDTO.getPaymentList()));
    }

    private DeptDO checkDept(Long deptId) {
        DeptDO deptDO = deptManager.getById(deptId);
        if (deptDO == null) {
            throw new ServiceException("仓库未关联部门，请先进行关联部门操作");
        }
        return deptDO;
    }

    private Map<Long, CollectingAccountDO> checkPayment(List<OrderPaymentDTO> paymentList) {
        List<Long> collectingAccountIds = convertList(paymentList, OrderPaymentDTO::getCollectingAccountId);
        Map<Long, CollectingAccountDO> collectingAccountMap = collectingAccountManager.listAndConvertMap(collectingAccountIds, CollectingAccountDO::getId);
        for (OrderPaymentDTO orderPaymentDTO : paymentList) {
            CollectingAccountDO collectingAccountDO = collectingAccountMap.get(orderPaymentDTO.getCollectingAccountId());
            if (collectingAccountDO == null) {
                throw new ServiceException(StrUtil.format("收款账户【{}】不存在", orderPaymentDTO.getCollectingAccountId()));
            }
            if (!collectingAccountDO.getEnableFlag()) {
                throw new ServiceException(StrUtil.format("收款账户【{}】未启用", collectingAccountDO.getName()));
            }
        }
        return collectingAccountMap;
    }

    private Map<Long, ProductDO> checkProduct(OrderDTO orderDTO) {
        // 批量查询商品
        List<Long> productIds = convertList(orderDTO.getItemList(), OrderItemDTO::getProductId);
        Map<Long, ProductDO> productMap = productManager.listAndConvertMap(productIds, ProductDO::getId);
        // 循环校验
        for (OrderItemDTO orderItemDTO : orderDTO.getItemList()) {
            ProductDO productDO = productMap.get(orderItemDTO.getProductId());
            if (productDO == null) {
                throw new ServiceException(StrUtil.format("商品【{}】不存在", orderItemDTO.getProductId()));
            }
        }

        // 查询分类，暂时不做数据存在校验
        List<Long> categoryIds = convertList(productMap.values(), ProductDO::getCategoryId);
        List<CategoryDO> categoryList = categoryManager.listByIds(categoryIds);
        orderDTO.setCategoryMap(convertMap(categoryList, CategoryDO::getCategoryId, Function.identity()));

        // 查询品牌，暂时不做数据存在校验
        List<Long> brandIds = convertList(productMap.values(), ProductDO::getBrandId);
        List<BrandDO> brandList = brandManager.listByIds(brandIds);
        orderDTO.setBrandMap(convertMap(brandList, BrandDO::getBrandId, Function.identity()));

        return productMap;
    }

    private StoreDO checkStore(OrderDeliveryType deliveryType, Long storeId) {
        if (deliveryType != OrderDeliveryType.self_pick) {
            return null;
        }

        StoreDO storeDO = storeManager.getById(storeId);
        if (storeDO == null) {
            throw new ServiceException("自提门店不存在");
        }

        return storeDO;
    }

    private MarketingManagerDO checkMarketing(Long marketingId) {
        if (marketingId == null) {
            return null;
        }

        MarketingManagerDO marketingManagerDO = marketingManagerManager.getById(marketingId);
        if (marketingManagerDO == null) {
            throw new SecurityException("销售经理不存在");
        }
        if (marketingManagerDO.getDisableFlag() == 1) {
            throw new SecurityException("销售经理已禁用");
        }

        return marketingManagerDO;
    }

    private WarehouseDO checkWarehouse(Long warehouseId) {
        WarehouseDO warehouseDO = warehouseManager.getById(warehouseId);
        if (warehouseDO == null) {
            throw new SecurityException("仓库不存在");
        }
        return warehouseDO;
    }

    private MemberDO checkMember(Long memberId) {
        MemberDO memberDO = memberManager.getById(memberId);
        if (memberDO == null) {
            throw new SecurityException("会员不存在");
        }
        if (memberDO.getDisableFlag()) {
            throw new SecurityException("会员已禁用");
        }
        return memberDO;
    }
}

