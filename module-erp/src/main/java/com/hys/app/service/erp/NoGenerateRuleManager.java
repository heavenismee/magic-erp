package com.hys.app.service.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.NoGenerateRuleDO;
import com.hys.app.model.erp.dto.NoGenerateRuleDTO;
import com.hys.app.model.erp.dto.NoGenerateRuleQueryParams;
import com.hys.app.model.erp.enums.NoBusinessTypeEnum;
import com.hys.app.model.erp.vo.NoGenerateRuleVO;

import java.util.List;

/**
 * 编号生成规则业务层接口
 *
 * @author zs
 * 2023-12-01 11:37:56
 */
public interface NoGenerateRuleManager extends BaseService<NoGenerateRuleDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<NoGenerateRuleVO> list(NoGenerateRuleQueryParams queryParams);

    /**
     * 添加
     *
     * @param noGenerateRuleDTO
     */
    void add(NoGenerateRuleDTO noGenerateRuleDTO);

    /**
     * 编辑
     *
     * @param noGenerateRuleDTO
     */
    void edit(NoGenerateRuleDTO noGenerateRuleDTO);

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    NoGenerateRuleVO getDetail(Long id);

    /**
     * 删除
     *
     * @param ids
     */
    void delete(List<Long> ids);

    /**
     * 查询开启的规则
     *
     * @param noBusinessTypeEnum
     * @return
     */
    NoGenerateRuleDO getOpenRule(NoBusinessTypeEnum noBusinessTypeEnum);

    Long getSeq(Long id, String formatDate);
}

