package com.hys.app.service.erp.impl;

import com.hys.app.framework.database.mybatisplus.base.BaseServiceImpl;
import com.hys.app.framework.database.WebPage;
import com.hys.app.mapper.erp.StockTransferProductMapper;
import com.hys.app.model.erp.dos.StockTransferProductDO;
import com.hys.app.model.erp.vo.StockTransferProductVO;
import com.hys.app.model.erp.dto.StockTransferProductDTO;
import com.hys.app.converter.erp.StockTransferProductConverter;
import com.hys.app.service.erp.StockTransferProductManager;
import com.hys.app.model.erp.dto.StockTransferProductQueryParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 调拨单商品明细业务层实现
 *
 * @author zs
 * @since 2023-12-12 16:34:15
 */
@Service
public class StockTransferProductManagerImpl extends BaseServiceImpl<StockTransferProductMapper, StockTransferProductDO> implements StockTransferProductManager {

    @Autowired
    private StockTransferProductConverter converter;

    @Override
    public WebPage<StockTransferProductVO> list(StockTransferProductQueryParams queryParams) {
        WebPage<StockTransferProductDO> webPage = baseMapper.selectPage(queryParams);
        return converter.convert(webPage);
    }
    
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void add(StockTransferProductDTO stockTransferProductDTO) {
        save(converter.convert(stockTransferProductDTO));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void edit(StockTransferProductDTO stockTransferProductDTO) {
        updateById(converter.convert(stockTransferProductDTO));
    }

    @Override
    public StockTransferProductVO getDetail(Long id) {
        return converter.convert(getById(id));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Long id) {
        removeById(id);
    }

    @Override
    public void deleteByStockTransferId(List<Long> stockTransferIds) {
        lambdaUpdate().in(StockTransferProductDO::getStockTransferId, stockTransferIds).remove();
    }

    @Override
    public List<StockTransferProductDO> listByStockTransferId(Long stockTransferId) {
        return lambdaQuery().eq(StockTransferProductDO::getStockTransferId, stockTransferId).list();
    }

}

