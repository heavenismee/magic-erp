package com.hys.app.service.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.SupplierReturnDO;
import com.hys.app.model.erp.dto.SupplierReturnStatisticsParam;
import com.hys.app.model.erp.enums.SupplierReturnStatusEnum;
import com.hys.app.model.erp.vo.SupplierReturnVO;
import com.hys.app.model.erp.dto.SupplierReturnDTO;
import com.hys.app.model.erp.dto.SupplierReturnQueryParams;
import com.hys.app.framework.database.WebPage;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 供应商退货业务层接口
 *
 * @author zs
 * @since 2023-12-14 11:12:46
 */
public interface SupplierReturnManager extends BaseService<SupplierReturnDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<SupplierReturnVO> list(SupplierReturnQueryParams queryParams);

    /**
     * 添加
     * @param supplierReturnDTO
     */
    void add(SupplierReturnDTO supplierReturnDTO);

    /**
     * 编辑
     * @param supplierReturnDTO
     */
    void edit(SupplierReturnDTO supplierReturnDTO);

    /**
     * 查询详情
     * @param id
     * @return 
     */
    SupplierReturnVO getDetail(Long id);

    /**
     * 删除
     * @param ids
     */
    void delete(List<Long> ids);

    /**
     * 提交
     * @param id
     */
    void submit(Long id);

    /**
     * 撤销提交
     * @param id
     */
    void withdraw(Long id);

    /**
     * 审核
     * @param ids
     * @param status
     * @param remark
     */
    void audit(List<Long> ids, SupplierReturnStatusEnum status, String remark);

    /**
     * 查询供应商退货统计分页列表数据
     *
     * @param params 查询参数
     * @return
     */
    WebPage statistics(SupplierReturnStatisticsParam params);

    /**
     * 导出供应商退货统计列表
     *
     * @param response
     * @param params 查询参数
     */
    void export(HttpServletResponse response, SupplierReturnStatisticsParam params);
}

