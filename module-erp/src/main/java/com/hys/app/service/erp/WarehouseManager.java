package com.hys.app.service.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.WarehouseDO;
import com.hys.app.model.erp.dto.WarehouseDTO;
import com.hys.app.model.erp.dto.WarehouseQueryParams;
import com.hys.app.model.erp.vo.WarehouseVO;

import java.util.List;

/**
 * 仓库业务层接口
 *
 * @author zs
 * 2023-11-30 16:35:16
 */
public interface WarehouseManager extends BaseService<WarehouseDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<WarehouseVO> list(WarehouseQueryParams queryParams);

    /**
     * 添加
     *
     * @param warehouseDTO
     */
    void add(WarehouseDTO warehouseDTO);

    /**
     * 编辑
     *
     * @param warehouseDTO
     */
    void edit(WarehouseDTO warehouseDTO);

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    WarehouseVO getDetail(Long id);

    /**
     * 删除
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 查询所有仓库
     *
     * @return
     */
    List<WarehouseVO> listAll(Long deptId);

    long countByDeptId(Long deptId);
}

