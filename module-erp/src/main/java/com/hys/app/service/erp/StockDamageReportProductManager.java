package com.hys.app.service.erp;

import com.hys.app.model.erp.dos.StockDamageReportProduct;
import com.hys.app.model.erp.dto.StockDamageReportProductDTO;

import java.util.List;

/**
 * 库存报损单产品业务接口
 *
 * @author dmy
 * 2023-12-05
 */
public interface StockDamageReportProductManager {

    /**
     * 新增库存报损单产品信息
     *
     * @param reportId 库存报损单ID
     * @param productList 产品信息集合
     */
    void saveProduct(Long reportId, List<StockDamageReportProductDTO> productList);

    /**
     * 删除库存报损单产品信息
     *
     * @param reportIds 库存报损单ID集合
     */
    void deleteProduct(List<Long> reportIds);

    /**
     * 根据库存报损单ID获取库存报损单产品信息集合
     * @param reportId 库存报损单ID
     * @return
     */
    List<StockDamageReportProduct> list(Long reportId);
}
