package com.hys.app.service.erp.impl;

import com.hys.app.framework.database.mybatisplus.base.BaseServiceImpl;
import com.hys.app.framework.database.WebPage;
import com.hys.app.mapper.erp.SupplierReturnItemMapper;
import com.hys.app.model.erp.dos.SupplierReturnItemDO;
import com.hys.app.model.erp.vo.SupplierReturnItemVO;
import com.hys.app.model.erp.dto.SupplierReturnItemDTO;
import com.hys.app.converter.erp.SupplierReturnItemConverter;
import com.hys.app.service.erp.SupplierReturnItemManager;
import com.hys.app.model.erp.dto.SupplierReturnItemQueryParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * 供应商退货项业务层实现
 *
 * @author zs
 * @since 2023-12-14 11:12:46
 */
@Service
public class SupplierReturnItemManagerImpl extends BaseServiceImpl<SupplierReturnItemMapper, SupplierReturnItemDO> implements SupplierReturnItemManager {

    @Autowired
    private SupplierReturnItemConverter converter;

    @Override
    public WebPage<SupplierReturnItemVO> list(SupplierReturnItemQueryParams queryParams) {
        WebPage<SupplierReturnItemDO> webPage = baseMapper.selectPage(queryParams);
        return converter.convert(webPage);
    }
    
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void add(SupplierReturnItemDTO supplierReturnItemDTO) {
        save(converter.convert(supplierReturnItemDTO));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void edit(SupplierReturnItemDTO supplierReturnItemDTO) {
        updateById(converter.convert(supplierReturnItemDTO));
    }

    @Override
    public SupplierReturnItemVO getDetail(Long id) {
        return converter.convert(getById(id));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(List<Long> ids) {
        removeBatchByIds(ids);
    }

    @Override
    public void deleteBySupplierReturnId(List<Long> supplierReturnIds) {
        lambdaUpdate().in(SupplierReturnItemDO::getSupplierReturnId, supplierReturnIds).remove();
    }

    @Override
    public List<SupplierReturnItemDO> listBySupplierReturnId(Long supplierReturnId) {
        return lambdaQuery().eq(SupplierReturnItemDO::getSupplierReturnId, supplierReturnId).list();
    }

}

