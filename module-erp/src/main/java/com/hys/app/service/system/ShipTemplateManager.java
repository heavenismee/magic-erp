package com.hys.app.service.system;

import java.util.List;

import com.hys.app.model.system.dos.ShipTemplateDO;
import com.hys.app.model.system.vo.ShipTemplateSellerVO;

/**
 * 运费模版业务层
 * @author zjp
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-28 21:44:49
 */
public interface ShipTemplateManager{


	/**
	 * 新增店铺运费模板
	 * @param tamplate 运费模板信息
	 * @return
	 */
	ShipTemplateDO save(ShipTemplateSellerVO tamplate);

	/**
	 * 修改店铺运费模板
	 * @param template 运费模板信息
	 * @return
	 */
	ShipTemplateDO edit(ShipTemplateSellerVO template);


	/**
	 * 查询运费模版列表数据集合
	 * @return
	 */
	List<ShipTemplateSellerVO> getStoreTemplate();

	/**
	 * 删除店铺运费模板
     * @param templateId 运费模板ID
     */
	void delete(Long templateId);

	/**
	 * 数据库中查询一个运费模板VO
	 * @param templateId 运费模板ID
	 * @return
	 */
	ShipTemplateSellerVO getFromDB(Long templateId);

}
