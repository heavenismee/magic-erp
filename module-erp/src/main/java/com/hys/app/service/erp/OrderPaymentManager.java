package com.hys.app.service.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.OrderPaymentDO;
import com.hys.app.model.erp.dto.OrderPaymentQueryParams;
import com.hys.app.model.erp.vo.OrderPaymentVO;

import java.util.List;

/**
 * 订单支付明细业务层接口
 *
 * @author zsong
 * 2024-01-24 17:00:32
 */
public interface OrderPaymentManager extends BaseService<OrderPaymentDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<OrderPaymentVO> list(OrderPaymentQueryParams queryParams);

    /**
     * 查询详情
     *
     * @param id 主键
     * @return 详情数据
     */
    OrderPaymentVO getDetail(Long id);

    /**
     * 删除
     *
     * @param ids 主键集合
     */
    void delete(List<Long> ids);

    /**
     * 根据订单id删除
     *
     * @param orderId
     */
    void deleteByOrderId(Long orderId);

    /**
     * 根据订单id查询
     * @param orderId
     * @return
     */
    List<OrderPaymentDO> listByOrderId(Long orderId);
}

