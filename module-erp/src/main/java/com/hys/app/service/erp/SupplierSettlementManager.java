package com.hys.app.service.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.SupplierSettlementDO;
import com.hys.app.model.erp.vo.SupplierSettlementVO;
import com.hys.app.model.erp.dto.SupplierSettlementDTO;
import com.hys.app.model.erp.dto.SupplierSettlementQueryParams;
import com.hys.app.framework.database.WebPage;
import java.util.List;

/**
 * 供应商结算单业务层接口
 *
 * @author zs
 * @since 2023-12-15 14:09:09
 */
public interface SupplierSettlementManager extends BaseService<SupplierSettlementDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<SupplierSettlementVO> list(SupplierSettlementQueryParams queryParams);

    /**
     * 添加
     * @param supplierSettlementDTO
     */
    void add(SupplierSettlementDTO supplierSettlementDTO);

    /**
     * 编辑
     * @param supplierSettlementDTO
     */
    void edit(SupplierSettlementDTO supplierSettlementDTO);

    /**
     * 查询详情
     * @param id
     * @return 
     */
    SupplierSettlementVO getDetail(Long id);

    /**
     * 删除
     * @param ids
     */
    void delete(List<Long> ids);

    /**
     * 取消结算单
     * @param id
     */
    void cancel(Long id);
}

