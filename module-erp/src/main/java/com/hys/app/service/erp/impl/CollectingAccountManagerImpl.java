package com.hys.app.service.erp.impl;

import com.hys.app.framework.database.mybatisplus.base.BaseServiceImpl;
import com.hys.app.framework.database.WebPage;
import com.hys.app.mapper.erp.CollectingAccountMapper;
import com.hys.app.model.erp.dos.CollectingAccountDO;
import com.hys.app.model.erp.enums.NoBusinessTypeEnum;
import com.hys.app.model.erp.vo.CollectingAccountVO;
import com.hys.app.model.erp.dto.CollectingAccountDTO;
import com.hys.app.converter.erp.CollectingAccountConverter;
import com.hys.app.service.erp.CollectingAccountManager;
import com.hys.app.model.erp.dto.CollectingAccountQueryParams;
import com.hys.app.service.erp.NoGenerateManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

/**
 * 收款账户业务层实现
 *
 * @author zsong
 * 2024-01-24 14:43:18
 */
@Service
public class CollectingAccountManagerImpl extends BaseServiceImpl<CollectingAccountMapper, CollectingAccountDO> implements CollectingAccountManager {
    
    @Autowired
    private CollectingAccountConverter converter;

    @Autowired
    private NoGenerateManager noGenerateManager;

    @Override
    public WebPage<CollectingAccountVO> list(CollectingAccountQueryParams queryParams) {
        WebPage<CollectingAccountDO> page = baseMapper.selectPage(queryParams);
        return converter.convertPage(page);
    }
    
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void add(CollectingAccountDTO collectingAccountDTO) {
        // 校验参数
        checkParams(collectingAccountDTO);

        // 如果是默认账户，将其他账户改为非默认
        if(collectingAccountDTO.getDefaultFlag()){
            baseMapper.updateAllToNotDefault();
        }

        // 保存
        CollectingAccountDO collectingAccountDO = converter.convert(collectingAccountDTO);
        collectingAccountDO.setSn(noGenerateManager.generate(NoBusinessTypeEnum.CollectingAccount));
        save(collectingAccountDO);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void edit(CollectingAccountDTO collectingAccountDTO) {
        // 校验参数
        checkParams(collectingAccountDTO);

        // 如果是默认账户，将其他账户改为非默认
        if(collectingAccountDTO.getDefaultFlag()){
            baseMapper.updateAllToNotDefault();
        }

        // 更新
        CollectingAccountDO collectingAccountDO = converter.convert(collectingAccountDTO);
        updateById(collectingAccountDO);
    }

    @Override
    public CollectingAccountVO getDetail(Long id) {
        CollectingAccountDO collectingAccountDO = getById(id);
        return converter.convert(collectingAccountDO);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(List<Long> ids) {
        removeBatchByIds(ids);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void setDefault(Long id) {
        // 先将所有账户改为非默认
        baseMapper.updateAllToNotDefault();
        // 再将该账户改为默认
        lambdaUpdate().set(CollectingAccountDO::getDefaultFlag, true).eq(CollectingAccountDO::getId, id).update();
    }

    @Override
    public void updateEnableFlag(Long id, Boolean enableFlag) {
        lambdaUpdate().set(CollectingAccountDO::getEnableFlag, enableFlag).eq(CollectingAccountDO::getId, id).update();
    }

    private void checkParams(CollectingAccountDTO collectingAccountDTO) {
        
    }
}

