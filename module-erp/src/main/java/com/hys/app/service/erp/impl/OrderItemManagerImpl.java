package com.hys.app.service.erp.impl;

import com.hys.app.converter.erp.OrderItemConverter;
import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseServiceImpl;
import com.hys.app.mapper.erp.OrderItemMapper;
import com.hys.app.model.erp.dos.OrderItemDO;
import com.hys.app.model.erp.dto.OrderItemQueryParams;
import com.hys.app.model.erp.vo.OrderItemVO;
import com.hys.app.service.erp.OrderItemManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
 * 订单明细业务层实现
 *
 * @author zsong
 * 2024-01-24 16:39:39
 */
@Service
public class OrderItemManagerImpl extends BaseServiceImpl<OrderItemMapper, OrderItemDO> implements OrderItemManager {

    @Autowired
    private OrderItemConverter converter;

    @Override
    public WebPage<OrderItemVO> list(OrderItemQueryParams queryParams) {
        WebPage<OrderItemDO> page = baseMapper.selectPage(queryParams);
        return converter.convertPage(page);
    }

    @Override
    public OrderItemVO getDetail(Long id) {
        OrderItemDO orderItemDO = getById(id);
        return converter.convert(orderItemDO);
    }

    @Override
    public void deleteByOrderId(Long orderId) {
        lambdaUpdate().eq(OrderItemDO::getOrderId, orderId).remove();
    }

    @Override
    public List<OrderItemDO> listByOrderIds(Collection<Long> orderIds) {
        return lambdaQuery().in(OrderItemDO::getOrderId, orderIds).list();
    }

}

