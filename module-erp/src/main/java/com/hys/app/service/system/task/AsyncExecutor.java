package com.hys.app.service.system.task;

/**
 * 异步执行器
 *
 * @author zs
 * 2023-05-12
 */
public interface AsyncExecutor {

    /**
     * 执行
     *
     * @param onStart
     * @param runnable
     * @param onSuccess
     * @param onError
     */
    void execute(ThreadHandler onStart, TaskRunnable runnable, Runnable onSuccess, TaskError onError);
}
