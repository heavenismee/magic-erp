package com.hys.app.service.system;


import com.hys.app.model.system.dto.DictTypeCreateDTO;
import com.hys.app.model.system.dto.DictTypeQueryParams;
import com.hys.app.model.system.dto.DictTypeUpdateDTO;
import com.hys.app.framework.database.WebPage;
import com.hys.app.model.system.dos.DictTypeDO;

import java.util.List;

/**
 * 字典类型 Service 接口
 * @author zs
 * @since 2023-11-28
 */
public interface DictTypeManager {

    /**
     * 创建字典类型
     *
     * @param reqVO 字典类型信息
     * @return 字典类型编号
     */
    Long createDictType(DictTypeCreateDTO reqVO);

    /**
     * 更新字典类型
     *
     * @param reqVO 字典类型信息
     */
    void updateDictType(DictTypeUpdateDTO reqVO);

    /**
     * 删除字典类型
     *
     * @param id 字典类型编号
     */
    void deleteDictType(Long id);

    /**
     * 获得字典类型分页列表
     *
     * @param reqVO 分页请求
     * @return 字典类型分页列表
     */
    WebPage<DictTypeDO> getDictTypePage(DictTypeQueryParams reqVO);

    /**
     * 获得字典类型详情
     *
     * @param id 字典类型编号
     * @return 字典类型
     */
    DictTypeDO getDictType(Long id);

    /**
     * 获得字典类型详情
     *
     * @param type 字典类型
     * @return 字典类型详情
     */
    DictTypeDO getDictType(String type);

    /**
     * 获得全部字典类型列表
     *
     * @return 字典类型列表
     */
    List<DictTypeDO> getDictTypeList();

}
