package com.hys.app.service.system.task;


import com.hys.app.model.base.Result;

@FunctionalInterface
public interface TaskRunnable {

    Result run();
}
