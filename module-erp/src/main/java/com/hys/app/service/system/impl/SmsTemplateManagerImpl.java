package com.hys.app.service.system.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.UpdateChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.exception.ServiceException;
import com.hys.app.model.system.vo.SmsPlatformVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hys.app.framework.util.PageConvert;
import com.hys.app.framework.util.StringUtil;
import com.hys.app.mapper.system.SmsTemplateMapper;
import com.hys.app.model.errorcode.SystemErrorCode;
import com.hys.app.model.system.dos.SmsTemplate;
import com.hys.app.model.system.enums.AuditStatusEnum;
import com.hys.app.model.system.enums.EnableStatusEnum;
import com.hys.app.model.system.enums.SmsServiceCodeEnum;
import com.hys.app.service.base.plugin.sms.SmsPlatform;
import com.hys.app.service.system.SmsPlatformManager;
import com.hys.app.service.system.SmsTemplateManager;
import com.hys.app.service.system.factory.SmsFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 短信模板业务类
 *
 * @author zhangsong
 * @version v1.0
 * @since v7.3.0
 * 2021-01-26 16:04:23
 */
@Service
public class SmsTemplateManagerImpl implements SmsTemplateManager {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private SmsTemplateMapper smsTemplateMapper;

	@Autowired
	private SmsPlatformManager smsPlatformManager;

	@Autowired
	private SmsFactory smsFactory;

	/**
	 * 查询短信模板列表
	 *
	 * @param pageNo      页码
	 * @param pageSize    每页数量
	 * @param serviceType 模板业务类型
	 * @param beanId      短信平台beanId
	 * @return WebPage
	 */
	@Override
	public WebPage list(Long pageNo, Long pageSize, String serviceType, String beanId) {

		//新建查询条件包装器
		QueryWrapper<SmsTemplate> wrapper = new QueryWrapper<>();
		wrapper.eq("service_type", serviceType);
		wrapper.eq("bean_id", beanId);

		IPage<SmsTemplate> iPage = smsTemplateMapper.selectPage(new Page<>(pageNo, pageSize), wrapper);

		return PageConvert.convert(iPage);
	}

	@Override
	@Transactional(value = "systemTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public SmsTemplate edit(SmsTemplate smsTemplate, Long id) {
		smsTemplate.setId(id);
		SmsTemplate oldModel = this.getModel(id);

		//找到所属短信平台插件
		SmsPlatform smsPlatform = smsFactory.findByPluginId(oldModel.getBeanId());

		//如果模板未发生变化，无需修改
		if (!isChange(oldModel, smsTemplate)) {
			return oldModel;
		}
		//封装模板编码
		smsTemplate.setTemplateCode(oldModel.getTemplateCode());
		//校验当前状态是否可以修改
		smsPlatform.templateUpdateCheck(oldModel);

		//修改模板
		smsPlatform.updateTemplate(oldModel, smsTemplate);
		smsTemplateMapper.updateById(smsTemplate);

		return smsTemplate;
	}

	/**
	 * 获取短信模板
	 *
	 * @param id 短信模板主键
	 * @return SmsTemplate  短信模板
	 */
	@Override
	public SmsTemplate getModel(Long id) {
		return this.smsTemplateMapper.selectById(id);
	}

	/**
	 * 获取短信模板
	 *
	 * @param templateCode 短信模板编号
	 * @return SmsTemplate  短信模板
	 */
	@Override
	public SmsTemplate getModel(String templateCode) {
		return new QueryChainWrapper<>(smsTemplateMapper).eq("template_code", templateCode).one();
	}

	/**
	 * 修改短信模板开启状态
	 *
	 * @param id           短信模板id
	 * @param enableStatus 要修改的状态 {@link EnableStatusEnum}
	 */
	@Override
	public void updateEnableStatus(Long id, String enableStatus) {
		//验证状态值是否正确
		EnableStatusEnum.valueOf(enableStatus);

		SmsTemplate model = this.getModel(id);
		if (model == null) {
			throw new ServiceException(SystemErrorCode.E931.code(), "短信模板不存在");
		}
		//审核通过后才可以开启
		if (!AuditStatusEnum.PASS.value().equals(model.getAuditStatus())) {
			throw new ServiceException(SystemErrorCode.E931.code(), "审核未通过，无法开启");
		}

		//修改模板启用状态
		new UpdateChainWrapper<>(smsTemplateMapper)
				.set("enable_status", enableStatus)
				.eq("id", id)
				.update();
	}

	/**
	 * 提交审核
	 *
	 * @param id     短信模板id
	 * @param remark 申请说明(描述业务使用场景)
	 */
	@Override
	public void audit(Long id, String remark) {
		SmsTemplate model = this.getModel(id);
		if (model == null) {
			throw new ServiceException(SystemErrorCode.E931.code(), "短信模板不存在");
		}

		//找到所属短信平台插件
		SmsPlatform smsPlatform = smsFactory.findByPluginId(model.getBeanId());

		//判断模板是否需要审核
		if (!smsPlatform.isNeedAudit()) {
			return;
		}
		//只有待审核状态和审核不通过状态可以提交审核
		String auditStatus = model.getAuditStatus();
		if (!(AuditStatusEnum.WAITING.value().equals(auditStatus) || AuditStatusEnum.NOT_PASS.value().equals(auditStatus))) {
			throw new ServiceException(SystemErrorCode.E931.code(), "当前状态无法提交审核");
		}

		//提交审核
		model.setRemark(remark);
		smsPlatform.templateSubmitAudit(model);

		//状态设置为审核中
		model.setAuditStatus(AuditStatusEnum.AUDITING.value());
		smsTemplateMapper.updateById(model);
	}

	/**
	 * 短信模板审核回调
	 *
	 * @param json
	 * @param type
	 * @return
	 */
	@Override
	public Map templateAuditCallback(String json, String type) {
		//  获取短信插件
		SmsPlatform smsPlatform = smsFactory.findByPluginId(type);
		return smsPlatform.templateAuditCallback(json);


	}

	/**
	 * 查询当前开启的短信平台的模板
	 *
	 * @param serviceCode 业务类型编号
	 * @return 短信模板
	 */
	@Override
	public SmsTemplate getOpenTemplate(SmsServiceCodeEnum serviceCode) {
		if (serviceCode == null) {
			return null;
		}

		//获取当前开启的短信平台
		SmsPlatformVO openSmsPlatform = smsPlatformManager.getOpen();

		if (openSmsPlatform == null) {
			return null;
		}

		return new QueryChainWrapper<>(smsTemplateMapper)
				//根据业务编号查询
				.eq("service_code", serviceCode.value())
				//根据短信平台查询
				.eq("bean_id", openSmsPlatform.getBean())
				.one();
	}

	@Override
	public Map smsSendCallback(String json, String type) {
		//  获取短信插件
		SmsPlatform smsPlatform = smsFactory.findByPluginId(type);
		return smsPlatform.smsSendCallback(json);
	}

	/**
	 * 短信模板是否发生变化
	 *
	 * @param oldModel    修改之前的短信模板
	 * @param smsTemplate 修改后的短信模板
	 * @return
	 */
	private boolean isChange(SmsTemplate oldModel, SmsTemplate smsTemplate) {
		if (oldModel == null) {
			throw new ServiceException(SystemErrorCode.E931.code(), "模板不存在");
		}

		//模板名称
		String templateName = smsTemplate.getTemplateName();
		if (StringUtil.notEmpty(templateName) && !templateName.equals(oldModel.getTemplateName())) {
			return true;
		}

		//模板内容
		String templateContent = smsTemplate.getTemplateContent();
		if (StringUtil.notEmpty(templateContent) && !templateContent.equals(oldModel.getTemplateContent())) {
			return true;
		}

		//模板类型
		String templateType = smsTemplate.getTemplateType();
		if (StringUtil.notEmpty(templateType) && !templateType.equals(oldModel.getTemplateType())) {
			return true;
		}

		return false;
	}
}