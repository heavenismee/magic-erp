package com.hys.app.service.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.CollectingAccountDO;
import com.hys.app.model.erp.dto.CollectingAccountDTO;
import com.hys.app.model.erp.dto.CollectingAccountQueryParams;
import com.hys.app.model.erp.vo.CollectingAccountVO;

import java.util.List;

/**
 * 收款账户业务层接口
 *
 * @author zsong
 * 2024-01-24 14:43:18
 */
public interface CollectingAccountManager extends BaseService<CollectingAccountDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<CollectingAccountVO> list(CollectingAccountQueryParams queryParams);

    /**
     * 添加
     *
     * @param collectingAccountDTO 新增数据
     */
    void add(CollectingAccountDTO collectingAccountDTO);

    /**
     * 编辑
     *
     * @param collectingAccountDTO 更新数据
     */
    void edit(CollectingAccountDTO collectingAccountDTO);

    /**
     * 查询详情
     *
     * @param id 主键
     * @return 详情数据
     */
    CollectingAccountVO getDetail(Long id);

    /**
     * 删除
     *
     * @param ids 主键集合
     */
    void delete(List<Long> ids);

    /**
     * 设为默认
     *
     * @param id 主键
     */
    void setDefault(Long id);

    /**
     * 更新状态
     *
     * @param id         主键
     * @param enableFlag 启用/禁用
     */
    void updateEnableFlag(Long id, Boolean enableFlag);
}

