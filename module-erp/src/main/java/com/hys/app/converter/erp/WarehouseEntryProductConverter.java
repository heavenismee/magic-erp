package com.hys.app.converter.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.util.CurrencyUtil;
import com.hys.app.model.erp.dos.ProcurementContractProduct;
import com.hys.app.model.erp.dos.WarehouseEntryProductDO;
import com.hys.app.model.erp.dto.WarehouseEntryProductDTO;
import com.hys.app.model.erp.vo.WarehouseEntryProductVO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 入库单商品明细 Convert
 *
 * @author zs
 * @since 2023-12-05 15:30:10
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface WarehouseEntryProductConverter {

    WarehouseEntryProductDO convert(WarehouseEntryProductDTO warehouseEntryProductDTO);

    WarehouseEntryProductVO convert(WarehouseEntryProductDO warehouseEntryProductDO);

    List<WarehouseEntryProductVO> convert(List<WarehouseEntryProductDO> list);

    WebPage<WarehouseEntryProductVO> convert(WebPage<WarehouseEntryProductDO> webPage);

    default List<WarehouseEntryProductDO> combination(List<WarehouseEntryProductDTO> dtolist, Long warehouseEntryId, Map<Long, ProcurementContractProduct> contractProductMap, Double priceRatio) {
        List<WarehouseEntryProductDO> list = new ArrayList<>();

        for (WarehouseEntryProductDTO warehouseEntryProductDTO : dtolist) {
            // 合同商品信息
            ProcurementContractProduct contractProduct = contractProductMap.get(warehouseEntryProductDTO.getProductId());

            WarehouseEntryProductDO entryProductDO = convert(warehouseEntryProductDTO);
            entryProductDO.setWarehouseEntryId(warehouseEntryId);
            entryProductDO.setGoodsId(contractProduct.getGoodsId());
            entryProductDO.setSn(contractProduct.getProductSn());
            entryProductDO.setName(contractProduct.getProductName());
            entryProductDO.setSpecification(contractProduct.getSpecification());
            entryProductDO.setUnit(contractProduct.getUnit());
            entryProductDO.setProductBarcode(contractProduct.getBarcode());
            entryProductDO.setCategoryId(contractProduct.getCategoryId());
            entryProductDO.setCategoryName(contractProduct.getCategoryName());
            entryProductDO.setContractNum(contractProduct.getNum());
            entryProductDO.setContractPrice(contractProduct.getPrice());
            entryProductDO.setCostPrice(CurrencyUtil.mul(contractProduct.getPrice(), priceRatio));
            entryProductDO.setTotalPrice(CurrencyUtil.mul(entryProductDO.getCostPrice(), entryProductDO.getNum()));
            entryProductDO.setTaxRate(contractProduct.getTaxRate());

            list.add(entryProductDO);
        }

        return list;
    }
}

