package com.hys.app.converter.erp;

import com.hys.app.model.erp.vo.NoGenerateRuleVO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import com.hys.app.model.erp.dos.NoGenerateRuleDO;
import com.hys.app.model.erp.dto.NoGenerateRuleDTO;
import com.hys.app.framework.database.WebPage;

/**
 * 编号生成规则 Convert
 *
 * @author zs
 * 2023-12-01 11:37:56
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface NoGenerateRuleConverter {

    NoGenerateRuleDO convert(NoGenerateRuleDTO noGenerateRuleDTO);
    
    NoGenerateRuleVO convert(NoGenerateRuleDO noGenerateRuleDO);
    
    WebPage<NoGenerateRuleVO> convert(WebPage<NoGenerateRuleDO> webPage);
    
}

