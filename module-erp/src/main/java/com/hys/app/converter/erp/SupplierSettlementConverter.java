package com.hys.app.converter.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dos.SupplierSettlementDO;
import com.hys.app.model.erp.dto.SupplierSettlementDTO;
import com.hys.app.model.erp.vo.SupplierSettlementVO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.List;
import java.util.Map;

/**
 * 供应商结算单 Convert
 *
 * @author zs
 * @since 2023-12-15 14:09:09
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface SupplierSettlementConverter {

    SupplierSettlementDO convert(SupplierSettlementDTO supplierSettlementDTO);

    SupplierSettlementVO convert(SupplierSettlementDO supplierSettlementDO);

    List<SupplierSettlementVO> convert(List<SupplierSettlementDO> list);

    WebPage<SupplierSettlementVO> convert(WebPage<SupplierSettlementDO> webPage);

    default WebPage<SupplierSettlementVO> combination(WebPage<SupplierSettlementDO> webPage, Map<Long, String> supplierNameMap) {
        WebPage<SupplierSettlementVO> page = convert(webPage);

        for (SupplierSettlementVO supplierSettlementVO : page.getData()) {
            supplierSettlementVO.setSupplierName(supplierNameMap.get(supplierSettlementVO.getSupplierId()));
        }

        return page;
    }
}

