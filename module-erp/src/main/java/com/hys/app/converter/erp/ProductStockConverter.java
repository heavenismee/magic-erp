package com.hys.app.converter.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dos.ProductStockDO;
import com.hys.app.model.erp.dto.ProductStockIncreaseDTO;
import com.hys.app.model.erp.dto.ProductStockReduceDTO;
import com.hys.app.model.erp.vo.ProductStockVO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * 商品库存 Convert
 *
 * @author zs
 * @since 2023-12-07 10:08:44
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface ProductStockConverter {
    ProductStockVO convert(ProductStockDO productStockDO);

    WebPage<ProductStockVO> convert(WebPage<ProductStockDO> webPage);

    ProductStockIncreaseDTO convert(ProductStockReduceDTO stockReduceDTO);
}

