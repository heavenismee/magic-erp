package com.hys.app.converter.erp;

import com.hys.app.framework.util.DateUtil;
import com.hys.app.model.erp.dos.OrderReturnDO;
import com.hys.app.model.erp.dos.WarehouseEntryBatchDO;
import com.hys.app.model.erp.dos.WarehouseOutItemDO;
import com.hys.app.model.erp.enums.NoBusinessTypeEnum;
import com.hys.app.service.erp.NoGenerateManager;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import com.hys.app.model.erp.dos.OrderReturnItemDO;
import com.hys.app.model.erp.vo.OrderReturnItemVO;
import com.hys.app.model.erp.dto.OrderReturnItemDTO;
import com.hys.app.framework.database.WebPage;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 订单退货项 Convert
 *
 * @author zs
 * @since 2023-12-14 15:42:07
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface OrderReturnItemConverter {
    OrderReturnItemDO convert(OrderReturnItemDTO orderReturnItemDTO);
    
    OrderReturnItemVO convert(OrderReturnItemDO orderReturnItemDO);
    
    List<OrderReturnItemVO> convert(List<OrderReturnItemDO> list);
    
    WebPage<OrderReturnItemVO> convert(WebPage<OrderReturnItemDO> webPage);

    default List<OrderReturnItemDO> combination(Long orderReturnId, List<OrderReturnItemDTO> itemList, Map<Long, WarehouseOutItemDO> warehouseOutItemMap){
        return itemList.stream().map(orderReturnItemDTO -> {
            WarehouseOutItemDO warehouseOutItemDO = warehouseOutItemMap.get(orderReturnItemDTO.getWarehouseOutItemId());

            OrderReturnItemDO orderReturnItemDO = new OrderReturnItemDO();

            orderReturnItemDO.setOrderReturnId(orderReturnId);
            orderReturnItemDO.setWarehouseEntryId(warehouseOutItemDO.getWarehouseEntryId());
            orderReturnItemDO.setWarehouseEntrySn(warehouseOutItemDO.getWarehouseEntrySn());
            orderReturnItemDO.setWarehouseEntryItemId(warehouseOutItemDO.getWarehouseEntryItemId());
            orderReturnItemDO.setWarehouseEntryBatchId(warehouseOutItemDO.getWarehouseEntryBatchId());
            orderReturnItemDO.setWarehouseEntryBatchSn(warehouseOutItemDO.getWarehouseEntryBatchSn());
            orderReturnItemDO.setWarehouseOutItemId(warehouseOutItemDO.getId());
            orderReturnItemDO.setGoodsId(warehouseOutItemDO.getGoodsId());
            orderReturnItemDO.setProductId(warehouseOutItemDO.getProductId());
            orderReturnItemDO.setProductSn(warehouseOutItemDO.getProductSn());
            orderReturnItemDO.setProductName(warehouseOutItemDO.getProductName());
            orderReturnItemDO.setProductSpecification(warehouseOutItemDO.getProductSpecification());
            orderReturnItemDO.setProductUnit(warehouseOutItemDO.getProductUnit());
            orderReturnItemDO.setProductBarcode(warehouseOutItemDO.getProductBarcode());
            orderReturnItemDO.setCategoryId(warehouseOutItemDO.getCategoryId());
            orderReturnItemDO.setCategoryName(warehouseOutItemDO.getCategoryName());
            orderReturnItemDO.setProductPrice(warehouseOutItemDO.getProductPrice());
            orderReturnItemDO.setProductCostPrice(warehouseOutItemDO.getProductCostPrice());
            orderReturnItemDO.setTaxRate(warehouseOutItemDO.getTaxRate());
            orderReturnItemDO.setOrderNum(warehouseOutItemDO.getOutNum());
            orderReturnItemDO.setReturnNum(orderReturnItemDTO.getReturnNum());

            return orderReturnItemDO;
        }).collect(Collectors.toList());
    }

    default List<WarehouseEntryBatchDO> combination(List<OrderReturnItemDO> returnItemList, OrderReturnDO orderReturnDO, NoGenerateManager noGenerateManager){
        long dateline = DateUtil.getDateline();

        return returnItemList.stream().map(orderReturnItemDO -> {
            WarehouseEntryBatchDO batchDO = new WarehouseEntryBatchDO();

            batchDO.setWarehouseEntryId(orderReturnItemDO.getWarehouseEntryId());
            batchDO.setWarehouseEntrySn(orderReturnItemDO.getWarehouseEntrySn());
            batchDO.setWarehouseEntryItemId(orderReturnItemDO.getWarehouseEntryItemId());
            batchDO.setSn(noGenerateManager.generate(NoBusinessTypeEnum.WarehouseEntryBatch, orderReturnDO.getDeptId()));
            batchDO.setWarehouseId(orderReturnDO.getWarehouseId());
            batchDO.setGoodsId(orderReturnItemDO.getGoodsId());
            batchDO.setProductId(orderReturnItemDO.getProductId());
            batchDO.setProductSn(orderReturnItemDO.getProductSn());
            batchDO.setProductName(orderReturnItemDO.getProductName());
            batchDO.setProductSpecification(orderReturnItemDO.getProductSpecification());
            batchDO.setProductUnit(orderReturnItemDO.getProductUnit());
            batchDO.setProductBarcode(orderReturnItemDO.getProductBarcode());
            batchDO.setCategoryId(orderReturnItemDO.getCategoryId());
            batchDO.setCategoryName(orderReturnItemDO.getCategoryName());
            batchDO.setEntryNum(orderReturnItemDO.getReturnNum());
            batchDO.setEntryPrice(orderReturnItemDO.getProductPrice());
            batchDO.setProductCostPrice(orderReturnItemDO.getProductCostPrice());
            batchDO.setTaxRate(orderReturnItemDO.getTaxRate());
            batchDO.setRemainNum(orderReturnItemDO.getReturnNum());
            batchDO.setEntryTime(dateline);

            return batchDO;
        }).collect(Collectors.toList());
    }
}

