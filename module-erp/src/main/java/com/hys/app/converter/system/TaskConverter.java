package com.hys.app.converter.system;

import org.mapstruct.Mapper;
import com.hys.app.model.system.dos.TaskDO;
import com.hys.app.model.system.vo.TaskVO;
import com.hys.app.model.system.dto.TaskDTO;
import org.mapstruct.MappingConstants;
import com.hys.app.framework.database.WebPage;
import java.util.List;

/**
 * 任务 Converter
 *
 * @author zsong
 * 2024-01-23 10:43:35
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface TaskConverter {
    
    TaskDO convert(TaskDTO taskDTO);
    
    TaskVO convert(TaskDO taskDO);
    
    List<TaskVO> convertList(List<TaskDO> list);
    
    WebPage<TaskVO> convertPage(WebPage<TaskDO> webPage);
    
}

