package com.hys.app.converter.erp;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import com.hys.app.model.erp.dos.MarketingManagerDO;
import com.hys.app.model.erp.vo.MarketingManagerVO;
import com.hys.app.model.erp.dto.MarketingManagerDTO;
import com.hys.app.framework.database.WebPage;
import java.util.List;
import java.util.Map;

/**
 * 营销经理 Convert
 *
 * @author zs
 * @since 2023-12-11 11:25:31
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface MarketingManagerConverter {

    MarketingManagerDO convert(MarketingManagerDTO marketingManagerDTO);
    
    MarketingManagerVO convert(MarketingManagerDO marketingManagerDO);
    
    List<MarketingManagerVO> convert(List<MarketingManagerDO> list);
    
    WebPage<MarketingManagerVO> convert(WebPage<MarketingManagerDO> webPage);

    default WebPage<MarketingManagerVO> combination(WebPage<MarketingManagerDO> webPage, Map<Long, String> deptNameMap){
        WebPage<MarketingManagerVO> page = convert(webPage);

        for (MarketingManagerVO marketingManagerVO : page.getData()) {
            marketingManagerVO.setDeptName(deptNameMap.get(marketingManagerVO.getDeptId()));
        }

        return page;
    }
}

