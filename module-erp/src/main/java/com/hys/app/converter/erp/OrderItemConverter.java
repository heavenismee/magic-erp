package com.hys.app.converter.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dos.OrderDO;
import com.hys.app.model.erp.dos.OrderItemDO;
import com.hys.app.model.erp.dos.ProductDO;
import com.hys.app.model.erp.dto.OrderDTO;
import com.hys.app.model.erp.dto.OrderItemDTO;
import com.hys.app.model.erp.vo.OrderItemExcelVO;
import com.hys.app.model.erp.vo.OrderItemVO;
import com.hys.app.model.goods.dos.BrandDO;
import com.hys.app.model.goods.dos.CategoryDO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 订单明细 Converter
 *
 * @author zsong
 * 2024-01-24 16:39:38
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface OrderItemConverter {

    OrderItemDO convert(OrderItemDTO orderItemDTO);

    OrderItemVO convert(OrderItemDO orderItemDO);

    List<OrderItemVO> convertList(List<OrderItemDO> list);

    List<OrderItemExcelVO> convertExcel(List<OrderItemVO> list);

    WebPage<OrderItemVO> convertPage(WebPage<OrderItemDO> webPage);

    default List<OrderItemDO> convert(OrderDTO orderDTO, OrderDO orderDO) {
        List<OrderItemDTO> itemList = orderDTO.getItemList();
        Map<Long, ProductDO> productMap = orderDTO.getProductMap();
        Map<Long, CategoryDO> categoryMap = orderDTO.getCategoryMap();
        Map<Long, BrandDO> brandMap = orderDTO.getBrandMap();

        return itemList.stream().map(orderItemDTO -> {
            ProductDO productDO = productMap.get(orderItemDTO.getProductId());
            CategoryDO categoryDO = categoryMap.get(productDO.getCategoryId());
            BrandDO brandDO = brandMap.get(productDO.getBrandId());

            OrderItemDO orderItemDO = new OrderItemDO();
            // 订单信息
            orderItemDO.setOrderId(orderDO.getId());
            orderItemDO.setOrderSn(orderDO.getSn());

            // 商品信息
            orderItemDO.setGoodsId(productDO.getGoodsId());
            orderItemDO.setProductId(productDO.getId());
            orderItemDO.setProductSn(productDO.getSn());
            orderItemDO.setProductName(productDO.getName());
            orderItemDO.setProductSpecification(productDO.getSpecification());
            orderItemDO.setProductUnit(productDO.getUnit());
            orderItemDO.setProductImage(productDO.getImage());
            orderItemDO.setProductWeight(productDO.getWeight());
            orderItemDO.setProductTaxRate(productDO.getTaxRate());
            orderItemDO.setProductCostPrice(productDO.getCostPrice());
            orderItemDO.setProductMktPrice(productDO.getMktPrice());
            orderItemDO.setProductOriginPrice(productDO.getPrice());
            orderItemDO.setBrandId(productDO.getBrandId());
            orderItemDO.setBrandName(brandDO == null ? "" : brandDO.getName());
            orderItemDO.setCategoryId(productDO.getCategoryId());
            orderItemDO.setCategoryName(categoryDO == null ? "" : categoryDO.getName());
            // 销售相关信息（由于金额都是在前端动态计算并展示的，所以后端就不再计算了，直接使用前端传递的）
            orderItemDO.setNum(orderItemDTO.getNum());
            orderItemDO.setPrice(orderItemDTO.getPrice());
            orderItemDO.setTaxRate(orderItemDTO.getTaxRate());
            orderItemDO.setTotalPrice(orderItemDTO.getTotalPrice());
            orderItemDO.setTaxPrice(orderItemDTO.getTaxPrice());
            orderItemDO.setDiscountPrice(orderItemDTO.getDiscountPrice());
            orderItemDO.setPayPrice(orderItemDTO.getPayPrice());
            orderItemDO.setRemark(orderItemDTO.getRemark());

            return orderItemDO;
        }).collect(Collectors.toList());
    }
}

