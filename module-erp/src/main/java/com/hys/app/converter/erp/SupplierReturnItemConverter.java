package com.hys.app.converter.erp;

import com.hys.app.model.erp.dos.SupplierReturnDO;
import com.hys.app.model.erp.dos.WarehouseEntryBatchDO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import com.hys.app.model.erp.dos.SupplierReturnItemDO;
import com.hys.app.model.erp.vo.SupplierReturnItemVO;
import com.hys.app.model.erp.dto.SupplierReturnItemDTO;
import com.hys.app.framework.database.WebPage;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 供应商退货项 Convert
 *
 * @author zs
 * @since 2023-12-14 11:12:46
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface SupplierReturnItemConverter {

    SupplierReturnItemDO convert(SupplierReturnItemDTO supplierReturnItemDTO);
    
    SupplierReturnItemVO convert(SupplierReturnItemDO supplierReturnItemDO);
    
    List<SupplierReturnItemVO> convert(List<SupplierReturnItemDO> list);
    
    WebPage<SupplierReturnItemVO> convert(WebPage<SupplierReturnItemDO> webPage);

    default List<SupplierReturnItemDO> combination(SupplierReturnDO supplierReturnDO, List<SupplierReturnItemDTO> itemList, Map<Long, WarehouseEntryBatchDO> batchMap){
        return itemList.stream().map(supplierReturnItemDTO -> {
            WarehouseEntryBatchDO batchDO = batchMap.get(supplierReturnItemDTO.getWarehouseEntryBatchId());

            SupplierReturnItemDO supplierReturnItemDO = new SupplierReturnItemDO();

            supplierReturnItemDO.setSupplierReturnId(supplierReturnDO.getId());
            supplierReturnItemDO.setWarehouseEntryItemId(batchDO.getWarehouseEntryItemId());
            supplierReturnItemDO.setWarehouseEntryBatchId(batchDO.getId());
            supplierReturnItemDO.setWarehouseEntryBatchSn(batchDO.getSn());
            supplierReturnItemDO.setGoodsId(batchDO.getGoodsId());
            supplierReturnItemDO.setProductId(batchDO.getProductId());
            supplierReturnItemDO.setProductSn(batchDO.getProductSn());
            supplierReturnItemDO.setProductName(batchDO.getProductName());
            supplierReturnItemDO.setProductSpecification(batchDO.getProductSpecification());
            supplierReturnItemDO.setProductUnit(batchDO.getProductUnit());
            supplierReturnItemDO.setProductBarcode(batchDO.getProductBarcode());
            supplierReturnItemDO.setCategoryId(batchDO.getCategoryId());
            supplierReturnItemDO.setCategoryName(batchDO.getCategoryName());
            supplierReturnItemDO.setProductPrice(batchDO.getEntryPrice());
            supplierReturnItemDO.setProductCostPrice(batchDO.getProductCostPrice());
            supplierReturnItemDO.setTaxRate(batchDO.getTaxRate());
            supplierReturnItemDO.setEntryNum(batchDO.getEntryNum());
            supplierReturnItemDO.setReturnNum(supplierReturnItemDTO.getReturnNum());

            return supplierReturnItemDO;
        }).collect(Collectors.toList());
    }
}

