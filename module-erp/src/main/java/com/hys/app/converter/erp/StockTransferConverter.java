package com.hys.app.converter.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.util.DateUtil;
import com.hys.app.model.erp.dos.StockTransferDO;
import com.hys.app.model.erp.dos.StockTransferProductDO;
import com.hys.app.model.erp.dos.WarehouseEntryBatchDO;
import com.hys.app.model.erp.dto.StockTransferDTO;
import com.hys.app.model.erp.dto.StockUpdateDTO;
import com.hys.app.model.erp.enums.NoBusinessTypeEnum;
import com.hys.app.model.erp.enums.StockOperateEnum;
import com.hys.app.model.erp.vo.StockTransferAllowable;
import com.hys.app.model.erp.vo.StockTransferVO;
import com.hys.app.model.system.dto.DataPermissionDTO;
import com.hys.app.service.erp.NoGenerateManager;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 库存调拨 Convert
 *
 * @author zs
 * @since 2023-12-12 11:59:06
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface StockTransferConverter {

    StockTransferDO convert(StockTransferDTO stockTransferDTO);

    @Mapping(target = "allowable", expression = "java(new com.hys.app.model.erp.vo.StockTransferAllowable(stockTransferDO))")
    StockTransferVO convert(StockTransferDO stockTransferDO);

    List<StockTransferVO> convert(List<StockTransferDO> list);

    WebPage<StockTransferVO> convert(WebPage<StockTransferDO> webPage);

    default List<WarehouseEntryBatchDO> combination(StockTransferDO stockTransferDO, List<StockTransferProductDO> transferProductList, NoGenerateManager noGenerateManager) {
        long dateline = DateUtil.getDateline();
        return transferProductList.stream().map(stockTransferProductDO -> {
            WarehouseEntryBatchDO batchDO = new WarehouseEntryBatchDO();

            batchDO.setWarehouseEntryId(stockTransferProductDO.getWarehouseEntryId());
            batchDO.setWarehouseEntrySn(stockTransferProductDO.getWarehouseEntrySn());
            batchDO.setWarehouseEntryItemId(stockTransferProductDO.getWarehouseEntryItemId());
            batchDO.setSn(noGenerateManager.generate(NoBusinessTypeEnum.WarehouseEntryBatch, stockTransferDO.getInDeptId()));
            batchDO.setWarehouseId(stockTransferDO.getInWarehouseId());
            batchDO.setGoodsId(stockTransferProductDO.getGoodsId());
            batchDO.setProductId(stockTransferProductDO.getProductId());
            batchDO.setProductSn(stockTransferProductDO.getProductSn());
            batchDO.setProductName(stockTransferProductDO.getProductName());
            batchDO.setProductSpecification(stockTransferProductDO.getProductSpecification());
            batchDO.setProductUnit(stockTransferProductDO.getProductUnit());
            batchDO.setProductBarcode(stockTransferProductDO.getProductBarcode());
            batchDO.setCategoryId(stockTransferProductDO.getCategoryId());
            batchDO.setCategoryName(stockTransferProductDO.getCategoryName());
            batchDO.setEntryNum(stockTransferProductDO.getNum());
            batchDO.setRemainNum(stockTransferProductDO.getNum());
            batchDO.setEntryPrice(stockTransferProductDO.getProductPrice());
            batchDO.setProductCostPrice(stockTransferProductDO.getProductCostPrice());
            batchDO.setTaxRate(stockTransferProductDO.getTaxRate());
            batchDO.setEntryTime(dateline);

            return batchDO;
        }).collect(Collectors.toList());
    }

    default List<StockUpdateDTO> convertStockUpdateList(List<StockTransferProductDO> transferProductList) {
        return transferProductList.stream().map(transferProductDO -> {
            StockUpdateDTO stockUpdateDTO = new StockUpdateDTO();

            stockUpdateDTO.setBatchId(transferProductDO.getWarehouseEntryBatchId());
            stockUpdateDTO.setBatchSn(transferProductDO.getWarehouseEntryBatchSn());
            stockUpdateDTO.setOperate(StockOperateEnum.Reduce);
            stockUpdateDTO.setChangeNum(transferProductDO.getNum());

            return stockUpdateDTO;
        }).collect(Collectors.toList());
    }

    default WebPage<StockTransferVO> combination(WebPage<StockTransferDO> webPage, DataPermissionDTO dataPermission) {
        WebPage<StockTransferVO> page = convert(webPage);

        for (StockTransferVO stockTransferVO : page.getData()) {
            StockTransferAllowable allowable = stockTransferVO.getAllowable();
            if (!dataPermission.getAll()) {
                if (!dataPermission.getDeptIds().contains(stockTransferVO.getOutDeptId())) {
                    allowable.setEdit(false);
                    allowable.setSubmit(false);
                    allowable.setWithdraw(false);
                    allowable.setDelete(false);
                }
                if (!dataPermission.getDeptIds().contains(stockTransferVO.getInDeptId())) {
                    allowable.setConfirm(false);
                }
            }
        }

        return page;
    }

    default StockTransferDO combination(StockTransferDTO stockTransferDTO) {
        StockTransferDO transferDO = convert(stockTransferDTO);

        transferDO.setOutDeptName(stockTransferDTO.getOutDeptDO().getName());
        transferDO.setOutWarehouseName(stockTransferDTO.getOutWarehouseDO().getName());
        transferDO.setInDeptName(stockTransferDTO.getInDeptDO().getName());
        transferDO.setInWarehouseName(stockTransferDTO.getInWarehouseDO().getName());
        transferDO.setOutHandledByName(stockTransferDTO.getOutHandleBy().getRealName());
        if (stockTransferDTO.getInHandleBy() != null) {
            transferDO.setInHandledByName(stockTransferDTO.getInHandleBy().getRealName());
        }

        return transferDO;
    }
}

