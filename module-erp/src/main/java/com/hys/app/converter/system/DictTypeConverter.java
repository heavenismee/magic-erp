package com.hys.app.converter.system;

import com.hys.app.model.system.dto.DictTypeCreateDTO;
import com.hys.app.model.system.vo.DictTypeRespVO;
import com.hys.app.model.system.vo.DictTypeSimpleRespVO;
import com.hys.app.model.system.dto.DictTypeUpdateDTO;
import com.hys.app.framework.database.WebPage;
import com.hys.app.model.system.dos.DictTypeDO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.List;


@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface DictTypeConverter {

    WebPage<DictTypeRespVO> convertPage(WebPage<DictTypeDO> bean);

    DictTypeRespVO convert(DictTypeDO bean);

    DictTypeDO convert(DictTypeCreateDTO bean);

    DictTypeDO convert(DictTypeUpdateDTO bean);

    List<DictTypeSimpleRespVO> convertList(List<DictTypeDO> list);

}
