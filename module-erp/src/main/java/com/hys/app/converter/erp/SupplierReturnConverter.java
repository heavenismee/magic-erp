package com.hys.app.converter.erp;

import com.hys.app.model.erp.dos.SupplierReturnItemDO;
import com.hys.app.model.erp.dos.WarehouseEntryDO;
import com.hys.app.model.erp.dto.StockUpdateDTO;
import com.hys.app.model.erp.enums.StockOperateEnum;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import com.hys.app.model.erp.dos.SupplierReturnDO;
import com.hys.app.model.erp.vo.SupplierReturnVO;
import com.hys.app.model.erp.dto.SupplierReturnDTO;
import com.hys.app.framework.database.WebPage;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 供应商退货 Convert
 *
 * @author zs
 * @since 2023-12-14 11:12:46
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface SupplierReturnConverter {

    SupplierReturnDO convert(SupplierReturnDTO supplierReturnDTO);

    @Mapping(target = "allowable", expression = "java(new com.hys.app.model.erp.vo.SupplierReturnAllowable(supplierReturnDO))")
    SupplierReturnVO convert(SupplierReturnDO supplierReturnDO);
    
    List<SupplierReturnVO> convert(List<SupplierReturnDO> list);
    
    WebPage<SupplierReturnVO> convert(WebPage<SupplierReturnDO> webPage);

    default WebPage<SupplierReturnVO> combination(WebPage<SupplierReturnDO> webPage, Map<Long, String> deptNameMap, Map<Long, String> warehouseNameMap, Map<Long, String> supplierNameMap){
        WebPage<SupplierReturnVO> page = convert(webPage);

        for (SupplierReturnVO supplierReturnVO : page.getData()) {
            supplierReturnVO.setDeptName(deptNameMap.get(supplierReturnVO.getDeptId()));
            supplierReturnVO.setWarehouseName(warehouseNameMap.get(supplierReturnVO.getWarehouseId()));
            supplierReturnVO.setSupplierName(supplierNameMap.get(supplierReturnVO.getSupplierId()));
        }

        return page;
    }

    default SupplierReturnDO combination(SupplierReturnDTO supplierReturnDTO){
        SupplierReturnDO supplierReturnDO = convert(supplierReturnDTO);

        WarehouseEntryDO warehouseEntryDO = supplierReturnDTO.getWarehouseEntryDO();
        supplierReturnDO.setWarehouseEntrySn(warehouseEntryDO.getSn());
        supplierReturnDO.setWarehouseId(warehouseEntryDO.getWarehouseId());
        supplierReturnDO.setSupplierId(warehouseEntryDO.getSupplierId());
        supplierReturnDO.setSupplierSn(warehouseEntryDO.getSupplierSn());
        supplierReturnDO.setContractId(warehouseEntryDO.getContractId());
        supplierReturnDO.setContractSn(warehouseEntryDO.getContractSn());
        supplierReturnDO.setDeptName(warehouseEntryDO.getDeptName());
        supplierReturnDO.setWarehouseName(warehouseEntryDO.getWarehouseName());
        supplierReturnDO.setSupplierName(warehouseEntryDO.getSupplierName());
        supplierReturnDO.setHandleByName(supplierReturnDTO.getHandleBy().getRealName());

        return supplierReturnDO;
    }

    default List<StockUpdateDTO> convertStockUpdateList(List<SupplierReturnItemDO> returnItemList){
        return returnItemList.stream().map(supplierReturnItemDO -> {
            StockUpdateDTO stockUpdateDTO = new StockUpdateDTO();

            stockUpdateDTO.setBatchId(supplierReturnItemDO.getWarehouseEntryBatchId());
            stockUpdateDTO.setBatchSn(supplierReturnItemDO.getWarehouseEntryBatchSn());
            stockUpdateDTO.setOperate(StockOperateEnum.Reduce);
            stockUpdateDTO.setChangeNum(supplierReturnItemDO.getReturnNum());

            return stockUpdateDTO;
        }).collect(Collectors.toList());
    }
}

