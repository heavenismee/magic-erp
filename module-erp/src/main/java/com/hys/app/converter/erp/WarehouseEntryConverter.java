package com.hys.app.converter.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.util.DateUtil;
import com.hys.app.model.erp.dos.ProcurementContractProduct;
import com.hys.app.model.erp.dos.WarehouseEntryBatchDO;
import com.hys.app.model.erp.dos.WarehouseEntryDO;
import com.hys.app.model.erp.dos.WarehouseEntryProductDO;
import com.hys.app.model.erp.dto.WarehouseEntryDTO;
import com.hys.app.model.erp.enums.NoBusinessTypeEnum;
import com.hys.app.model.erp.vo.WarehouseEntryBatchVO;
import com.hys.app.model.erp.vo.WarehouseEntryProductVO;
import com.hys.app.model.erp.vo.WarehouseEntryVO;
import com.hys.app.service.erp.NoGenerateManager;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.hys.app.framework.util.CollectionUtils.convertMap;

/**
 * 入库单 Convert
 *
 * @author zs
 * @since 2023-12-05 11:25:07
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface WarehouseEntryConverter {

    WarehouseEntryDO convert(WarehouseEntryDTO warehouseEntryDTO);

    @Mapping(target = "allowable", expression = "java(new com.hys.app.model.erp.vo.WarehouseEntryAllowable(warehouseEntryDO))")
    WarehouseEntryVO convert(WarehouseEntryDO warehouseEntryDO);

    WebPage<WarehouseEntryVO> convert(WebPage<WarehouseEntryDO> webPage);

    List<WarehouseEntryVO> convert(List<WarehouseEntryDO> list);

    default WarehouseEntryDO combination(WarehouseEntryDTO warehouseEntryDTO) {
        WarehouseEntryDO warehouseEntryDO = convert(warehouseEntryDTO);

        if (warehouseEntryDTO.getProcurementPlan()!=null){
            warehouseEntryDO.setProcurementPlanSn(warehouseEntryDTO.getProcurementPlan().getSn());
        }
        if (warehouseEntryDTO.getContract()!=null){
            warehouseEntryDO.setContractSn(warehouseEntryDTO.getContract().getSn());
        }
        warehouseEntryDO.setDeptName(warehouseEntryDTO.getDeptDO().getName());
        warehouseEntryDO.setWarehouseName(warehouseEntryDTO.getWarehouseDO().getName());
        warehouseEntryDO.setSupplierName(warehouseEntryDTO.getSupplierDO().getCustomName());
        warehouseEntryDO.setSupplierSn(warehouseEntryDTO.getSupplierDO().getCustomSn());
        warehouseEntryDO.setHandledByName(warehouseEntryDTO.getHandledBy().getRealName());

        return warehouseEntryDO;
    }

    default WarehouseEntryVO combination(WarehouseEntryDO warehouseEntryDO, List<WarehouseEntryProductVO> productList, List<ProcurementContractProduct> contractProductList, List<WarehouseEntryBatchVO> batchList) {
        WarehouseEntryVO warehouseEntryVO = convert(warehouseEntryDO);

        // 填充已入库数量字段
        Map<Long, Integer> stockMap = convertMap(contractProductList, ProcurementContractProduct::getProductId, ProcurementContractProduct::getStockNum);
        productList.forEach(productDetailVO -> productDetailVO.setAlreadyNum(stockMap.get(productDetailVO.getProductId())));

        warehouseEntryVO.setProductList(productList);
        warehouseEntryVO.setBatchList(batchList);
        return warehouseEntryVO;
    }

    default List<WarehouseEntryBatchDO> combination(WarehouseEntryDO warehouseEntryDO, List<WarehouseEntryProductDO> productList, NoGenerateManager noGenerateManager) {
        long dateline = DateUtil.getDateline();
        return productList.stream().map(warehouseEntryProductDO -> {

            WarehouseEntryBatchDO warehouseEntryBatchDO = new WarehouseEntryBatchDO();
            warehouseEntryBatchDO.setWarehouseEntryId(warehouseEntryDO.getId());
            warehouseEntryBatchDO.setWarehouseEntrySn(warehouseEntryDO.getSn());
            warehouseEntryBatchDO.setWarehouseEntryItemId(warehouseEntryProductDO.getId());
            warehouseEntryBatchDO.setSn(noGenerateManager.generate(NoBusinessTypeEnum.WarehouseEntryBatch, warehouseEntryDO.getDeptId()));
            warehouseEntryBatchDO.setWarehouseId(warehouseEntryDO.getWarehouseId());
            warehouseEntryBatchDO.setGoodsId(warehouseEntryProductDO.getGoodsId());
            warehouseEntryBatchDO.setProductId(warehouseEntryProductDO.getProductId());
            warehouseEntryBatchDO.setProductSn(warehouseEntryProductDO.getSn());
            warehouseEntryBatchDO.setProductName(warehouseEntryProductDO.getName());
            warehouseEntryBatchDO.setProductUnit(warehouseEntryProductDO.getUnit());
            warehouseEntryBatchDO.setProductBarcode(warehouseEntryProductDO.getProductBarcode());
            warehouseEntryBatchDO.setCategoryId(warehouseEntryProductDO.getCategoryId());
            warehouseEntryBatchDO.setCategoryName(warehouseEntryProductDO.getCategoryName());
            warehouseEntryBatchDO.setProductSpecification(warehouseEntryProductDO.getSpecification());
            warehouseEntryBatchDO.setEntryNum(warehouseEntryProductDO.getNum());
            warehouseEntryBatchDO.setEntryPrice(warehouseEntryProductDO.getContractPrice());
            warehouseEntryBatchDO.setProductCostPrice(warehouseEntryProductDO.getCostPrice());
            warehouseEntryBatchDO.setTaxRate(warehouseEntryProductDO.getTaxRate());
            warehouseEntryBatchDO.setRemainNum(warehouseEntryProductDO.getNum());
            warehouseEntryBatchDO.setEntryTime(dateline);

            return warehouseEntryBatchDO;

        }).collect(Collectors.toList());
    }
}

