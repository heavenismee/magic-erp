package com.hys.app.converter.erp;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import com.hys.app.model.erp.dos.OrderReturnDO;
import com.hys.app.model.erp.vo.OrderReturnVO;
import com.hys.app.model.erp.dto.OrderReturnDTO;
import com.hys.app.framework.database.WebPage;
import java.util.List;
import java.util.Map;

/**
 * 订单退货 Convert
 *
 * @author zs
 * @since 2023-12-14 15:42:07
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface OrderReturnConverter {
    OrderReturnDO convert(OrderReturnDTO orderReturnDTO);

    @Mapping(target = "allowable", expression = "java(new com.hys.app.model.erp.vo.OrderReturnAllowable(orderReturnDO))")
    OrderReturnVO convert(OrderReturnDO orderReturnDO);
    
    List<OrderReturnVO> convert(List<OrderReturnDO> list);
    
    WebPage<OrderReturnVO> convert(WebPage<OrderReturnDO> webPage);

    default WebPage<OrderReturnVO> combination(WebPage<OrderReturnDO> webPage, Map<Long, String> deptNameMap, Map<Long, String> warehouseNameMap){
        WebPage<OrderReturnVO> page = convert(webPage);

        for (OrderReturnVO orderReturnVO : page.getData()) {
            orderReturnVO.setDeptName(deptNameMap.get(orderReturnVO.getDeptId()));
            orderReturnVO.setWarehouseName(warehouseNameMap.get(orderReturnVO.getWarehouseId()));
        }

        return page;
    }

    default OrderReturnDO combination(OrderReturnDTO orderReturnDTO){
        OrderReturnDO orderReturnDO = convert(orderReturnDTO);

        orderReturnDO.setDistributionName(orderReturnDTO.getOrderDO().getMarketingName());
        orderReturnDO.setDeptName(orderReturnDTO.getDeptDO().getName());
        orderReturnDO.setWarehouseName(orderReturnDTO.getWarehouseDO().getName());
        orderReturnDO.setHandleByName(orderReturnDTO.getHandleBy().getRealName());

        return orderReturnDO;
    }
}

