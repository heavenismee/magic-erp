package com.hys.app.converter.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dos.WarehouseDO;
import com.hys.app.model.erp.dto.WarehouseDTO;
import com.hys.app.model.erp.vo.WarehouseVO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.List;
import java.util.Map;

/**
 * 仓库 Convert
 *
 * @author zs
 * 2023-11-30 16:35:16
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface WarehouseConverter {

    WarehouseDO convert(WarehouseDTO warehouseDTO);

    WarehouseVO convert(WarehouseDO warehouseDO);

    WebPage<WarehouseVO> convert(WebPage<WarehouseDO> webPage);

    List<WarehouseVO> convert(List<WarehouseDO> list);

    default WebPage<WarehouseVO> combination(WebPage<WarehouseDO> webPage, Map<Long, String> deptNameMap, Map<Long, String> adminNameMap) {
        WebPage<WarehouseVO> page = convert(webPage);

        for (WarehouseVO warehouseVO : page.getData()) {
            // 填充管理员名称
            warehouseVO.setAdminName(adminNameMap.get(warehouseVO.getAdminId()));
            // 填充部门名称
            warehouseVO.setDeptName(deptNameMap.get(warehouseVO.getDeptId()));
        }

        return page;
    }

}

