package com.hys.app.mapper.goods;

import com.hys.app.framework.util.StringUtil;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.erp.dos.ProductDO;
import com.hys.app.model.erp.dto.ProductQueryParams;
import com.hys.app.framework.database.WebPage;

/**
 * 产品的Mapper
 *
 * @author zs
 * 2023-11-30 16:06:44
 */
public interface ProductMapper extends BaseMapperX<ProductDO> {

    default WebPage<ProductDO> selectPage(ProductQueryParams params) {
        return lambdaQuery()
                .andX(StringUtil.notEmpty(params.getKeyword()),
                        w -> w.like(ProductDO::getName, params.getKeyword()).or().like(ProductDO::getSn, params.getKeyword()))
                .likeIfPresent(ProductDO::getSn, params.getSn())
                .likeIfPresent(ProductDO::getName, params.getName())
                .eqIfPresent(ProductDO::getCategoryId, params.getCategoryId())
                .likeIfPresent(ProductDO::getSpecification, params.getSpecification())
                .likeIfPresent(ProductDO::getUnit, params.getUnit())
                .eqIfPresent(ProductDO::getMarketEnable, params.getMarketEnable())
                .orderByDesc(ProductDO::getId)
                .page(params);
    }

}

