package com.hys.app.mapper.erp;

import org.apache.ibatis.annotations.Mapper;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;
import com.hys.app.model.erp.dos.CollectingAccountDO;
import com.hys.app.model.erp.dto.CollectingAccountQueryParams;
import com.hys.app.framework.database.WebPage;

/**
 * 收款账户 Mapper
 *
 * @author zsong
 * 2024-01-24 14:43:18
 */
@Mapper
public interface CollectingAccountMapper extends BaseMapperX<CollectingAccountDO> {

    default WebPage<CollectingAccountDO> selectPage(CollectingAccountQueryParams params) {
        return lambdaQuery()
                .likeIfPresent(CollectingAccountDO::getName, params.getName())
                .likeIfPresent(CollectingAccountDO::getSn, params.getSn())
                .eqIfPresent(CollectingAccountDO::getDefaultFlag, params.getDefaultFlag())
                .eqIfPresent(CollectingAccountDO::getEnableFlag, params.getEnableFlag())
                .likeIfPresent(CollectingAccountDO::getRemark, params.getRemark())
                .betweenIfPresent(BaseDO::getCreateTime, params.getCreateTime())
                .orderByDesc(CollectingAccountDO::getId)
                .page(params);
    }

    default void updateAllToNotDefault(){
        lambdaUpdate().set(CollectingAccountDO::getDefaultFlag, false).update();
    }
}

