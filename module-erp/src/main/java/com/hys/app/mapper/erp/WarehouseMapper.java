package com.hys.app.mapper.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.erp.dos.WarehouseDO;
import com.hys.app.model.erp.dto.WarehouseQueryParams;
import com.hys.app.framework.database.WebPage;

import java.util.List;

/**
 * 仓库的Mapper
 *
 * @author zs
 * 2023-11-30 16:35:16
 */
public interface WarehouseMapper extends BaseMapperX<WarehouseDO> {

    default WebPage<WarehouseDO> selectPage(WarehouseQueryParams params) {
        return lambdaQuery()
                .likeIfPresent(WarehouseDO::getName, params.getKeyword())
                .likeIfPresent(WarehouseDO::getName, params.getName())
                .likeIfPresent(WarehouseDO::getSn, params.getSn())
                .eqIfPresent(WarehouseDO::getAdminId, params.getAdminId())
                .likeIfPresent(WarehouseDO::getDescription, params.getDescription())
                .eqIfPresent(WarehouseDO::getDeptId, params.getDeptId())
                .orderByDesc(WarehouseDO::getId)
                .page(params);
    }

    default List<WarehouseDO> listAll(Long deptId){
        return lambdaQuery()
                .eqIfPresent(WarehouseDO::getDeptId, deptId)
                .orderByDesc(WarehouseDO::getId)
                .list();
    }
}

