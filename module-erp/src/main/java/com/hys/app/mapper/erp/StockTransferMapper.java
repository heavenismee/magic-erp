package com.hys.app.mapper.erp;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.erp.dos.StockTransferDO;
import com.hys.app.model.erp.dto.StockTransferQueryParams;
import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.StockTransferStatisticsParam;
import com.hys.app.model.erp.vo.StockTransferStatistics;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 库存调拨的Mapper
 *
 * @author zs
 * @since 2023-12-12 11:59:06
 */
public interface StockTransferMapper extends BaseMapperX<StockTransferDO> {

    default WebPage<StockTransferDO> selectPage(StockTransferQueryParams params) {
        return lambdaQuery()
                .likeIfPresent(StockTransferDO::getSn, params.getSn())
                .eqIfPresent(StockTransferDO::getOutDeptId, params.getOutDeptId())
                .eqIfPresent(StockTransferDO::getOutWarehouseId, params.getOutWarehouseId())
                .eqIfPresent(StockTransferDO::getOutHandledByName, params.getOutHandledBy())
                .eqIfPresent(StockTransferDO::getInDeptId, params.getInDeptId())
                .eqIfPresent(StockTransferDO::getInWarehouseId, params.getInWarehouseId())
                .eqIfPresent(StockTransferDO::getInHandledByName, params.getInHandledBy())
                .eqIfPresent(StockTransferDO::getStatus, params.getStatus())
                .geIfPresent(BaseDO::getCreateTime, params.getStartTime())
                .leIfPresent(BaseDO::getCreateTime, params.getEndTime())
                .andX(params.getDeptIdList() != null, deptWrapper ->
                        deptWrapper.in(StockTransferDO::getInDeptId, params.getDeptIdList()).or().in(StockTransferDO::getOutDeptId, params.getDeptIdList()))
                .orderByDesc(StockTransferDO::getTransferTime)
                .page(params);
    }

    /**
     * 查询库存调拨统计分页列表数据
     *
     * @param page 分页参数
     * @param param 查询参数
     * @return
     */
    IPage<StockTransferStatistics> selectStockTransferPage(Page page, @Param("param") StockTransferStatisticsParam param);

    /**
     * 导出库存调拨统计列表
     *
     * @param param 查询参数
     * @return
     */
    List<StockTransferStatistics> selectStockTransferList(@Param("param") StockTransferStatisticsParam param);
}

