package com.hys.app.mapper.erp;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.erp.dos.WarehouseOutDO;
import com.hys.app.model.erp.dto.WarehouseOutQueryParams;
import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.WarehouseOutStatisticsParam;
import com.hys.app.model.erp.vo.WarehouseOutStatistics;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 出库单的Mapper
 *
 * @author zs
 * @since 2023-12-07 16:50:20
 */
public interface WarehouseOutMapper extends BaseMapperX<WarehouseOutDO> {

    default WebPage<WarehouseOutDO> selectPage(WarehouseOutQueryParams params) {
        return lambdaQuery()
                .eqIfPresent(WarehouseOutDO::getSn, params.getSn())
                .eqIfPresent(WarehouseOutDO::getOutTime, params.getOutTime())
                .eqIfPresent(WarehouseOutDO::getDeptId, params.getDeptId())
                .eqIfPresent(WarehouseOutDO::getWarehouseId, params.getWarehouseId())
                .eqIfPresent(WarehouseOutDO::getConsignee, params.getConsignee())
                .eqIfPresent(WarehouseOutDO::getStatus, params.getStatus())
                .orderByDesc(WarehouseOutDO::getOutTime)
                .page(params);
    }

    /**
     * 查询出库单统计分页列表数据
     *
     * @param page 分页参数
     * @param param 查询参数
     * @return
     */
    IPage<WarehouseOutStatistics> selectWarehouseOutPage(Page page, @Param("param") WarehouseOutStatisticsParam param);

    /**
     * 查询导出出库单统计列表
     *
     * @param param 查询参数
     * @return
     */
    List<WarehouseOutStatistics> selectWarehouseOutList(@Param("param") WarehouseOutStatisticsParam param);
}

