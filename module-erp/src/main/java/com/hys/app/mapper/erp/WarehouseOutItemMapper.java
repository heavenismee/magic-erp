package com.hys.app.mapper.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.erp.dos.WarehouseOutItemDO;
import com.hys.app.model.erp.dto.WarehouseOutItemQueryParams;
import com.hys.app.framework.database.WebPage;

/**
 * 出库单商品明细的Mapper
 *
 * @author zs
 * @since 2023-12-07 17:06:32
 */
public interface WarehouseOutItemMapper extends BaseMapperX<WarehouseOutItemDO> {

    default WebPage<WarehouseOutItemDO> selectPage(WarehouseOutItemQueryParams params) {
        return lambdaQuery()
                .eqIfPresent(WarehouseOutItemDO::getWarehouseOutId, params.getWarehouseOutId())
                .eqIfPresent(WarehouseOutItemDO::getOrderSn, params.getOrderSn())
                .eqIfPresent(WarehouseOutItemDO::getWarehouseEntrySn, params.getWarehouseEntrySn())
                .eqIfPresent(WarehouseOutItemDO::getProductId, params.getProductId())
                .eqIfPresent(WarehouseOutItemDO::getProductSn, params.getProductSn())
                .eqIfPresent(WarehouseOutItemDO::getProductName, params.getProductName())
                .eqIfPresent(WarehouseOutItemDO::getProductSpecification, params.getProductSpecification())
                .eqIfPresent(WarehouseOutItemDO::getProductUnit, params.getProductUnit())
                .eqIfPresent(WarehouseOutItemDO::getProductPrice, params.getProductPrice())
                .eqIfPresent(WarehouseOutItemDO::getOutNum, params.getOutNum())
                .orderByDesc(WarehouseOutItemDO::getId)
                .page(params);
    }

}

