package com.hys.app.mapper.erp;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hys.app.framework.cache.MybatisRedisCache;
import com.hys.app.model.erp.dos.StockDamageReport;
import com.hys.app.model.erp.dto.StockDamageReportStatisticsParam;
import com.hys.app.model.erp.vo.StockDamageReportStatistics;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 库存报损单Mapper
 *
 * @author dmy
 * 2023-12-05
 */
@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface StockDamageReportMapper extends BaseMapper<StockDamageReport> {

    /**
     * 查询库存报损单商品统计分页列表数据
     *
     * @param page 分页参数
     * @param param 查询参数
     * @return
     */
    IPage<StockDamageReportStatistics> selectReportPage(Page page, @Param("param") StockDamageReportStatisticsParam param);

    /**
     * 查询库存报损单商品统计信息集合
     *
     * @param param 查询参数
     * @return
     */
    List<StockDamageReportStatistics> selectReportList(@Param("param") StockDamageReportStatisticsParam param);

}
