package com.hys.app.mapper.erp;

import com.hys.app.model.erp.enums.NoBusinessTypeEnum;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.erp.dos.NoGenerateRuleDO;
import com.hys.app.model.erp.dto.NoGenerateRuleQueryParams;
import com.hys.app.framework.database.WebPage;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;

/**
 * 编号生成规则的Mapper
 *
 * @author zs
 * 2023-12-01 11:37:56
 */
public interface NoGenerateRuleMapper extends BaseMapperX<NoGenerateRuleDO> {

    default WebPage<NoGenerateRuleDO> selectPage(NoGenerateRuleQueryParams params) {
        return lambdaQuery()
                .eqIfPresent(NoGenerateRuleDO::getType, params.getType())
                .likeIfPresent(NoGenerateRuleDO::getName, params.getName())
                .eqIfPresent(NoGenerateRuleDO::getSeqGenerateType, params.getSeqGenerateType())
                .eqIfPresent(NoGenerateRuleDO::getOpenFlag, params.getOpenFlag())
                .likeIfPresent(NoGenerateRuleDO::getItemList, params.getItemList())
                .orderByDesc(NoGenerateRuleDO::getId)
                .page(params);
    }

    default void updateCloseByType(NoBusinessTypeEnum type){
        lambdaUpdate()
                .set(NoGenerateRuleDO::getOpenFlag, false)
                .eq(NoGenerateRuleDO::getType, type)
                .update();
    }

    default NoGenerateRuleDO selectOpenRule(NoBusinessTypeEnum noBusinessTypeEnum){
        return lambdaQuery()
                .eq(NoGenerateRuleDO::getType, noBusinessTypeEnum)
                .eq(NoGenerateRuleDO::getOpenFlag, true)
                .one();
    }

    @Select("SELECT getSeq(#{id}, #{formatDate}, #{now})")
    Long getSeq(@Param("id") Long id, @Param("formatDate") String formatDate, @Param("now") Date now);

}

