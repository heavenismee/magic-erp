package com.hys.app.mapper.erp;

import org.apache.ibatis.annotations.Mapper;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.erp.dos.OrderPaymentDO;
import com.hys.app.model.erp.dto.OrderPaymentQueryParams;
import com.hys.app.framework.database.WebPage;

/**
 * 订单支付明细 Mapper
 *
 * @author zsong
 * 2024-01-24 17:00:32
 */
@Mapper
public interface OrderPaymentMapper extends BaseMapperX<OrderPaymentDO> {

    default WebPage<OrderPaymentDO> selectPage(OrderPaymentQueryParams params) {
        return lambdaQuery()
                .eqIfPresent(OrderPaymentDO::getOrderId, params.getOrderId())
                .eqIfPresent(OrderPaymentDO::getCollectingAccountId, params.getCollectingAccountId())
                .eqIfPresent(OrderPaymentDO::getCollectingAccountName, params.getCollectingAccountName())
                .eqIfPresent(OrderPaymentDO::getRemark, params.getRemark())
                .orderByDesc(OrderPaymentDO::getId)
                .page(params);
    }

}

