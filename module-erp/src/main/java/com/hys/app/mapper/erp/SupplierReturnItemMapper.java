package com.hys.app.mapper.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.erp.dos.SupplierReturnItemDO;
import com.hys.app.model.erp.dto.SupplierReturnItemQueryParams;
import com.hys.app.framework.database.WebPage;

/**
 * 供应商退货项的Mapper
 *
 * @author zs
 * @since 2023-12-14 11:12:46
 */
public interface SupplierReturnItemMapper extends BaseMapperX<SupplierReturnItemDO> {

    default WebPage<SupplierReturnItemDO> selectPage(SupplierReturnItemQueryParams params) {
        return lambdaQuery()
                .eqIfPresent(SupplierReturnItemDO::getSupplierReturnId, params.getSupplierReturnId())
                .eqIfPresent(SupplierReturnItemDO::getWarehouseEntryBatchId, params.getWarehouseEntryBatchId())
                .eqIfPresent(SupplierReturnItemDO::getWarehouseEntryBatchSn, params.getWarehouseEntryBatchSn())
                .eqIfPresent(SupplierReturnItemDO::getProductId, params.getProductId())
                .eqIfPresent(SupplierReturnItemDO::getProductSn, params.getProductSn())
                .eqIfPresent(SupplierReturnItemDO::getProductName, params.getProductName())
                .eqIfPresent(SupplierReturnItemDO::getProductSpecification, params.getProductSpecification())
                .eqIfPresent(SupplierReturnItemDO::getProductPrice, params.getProductPrice())
                .eqIfPresent(SupplierReturnItemDO::getEntryNum, params.getEntryNum())
                .eqIfPresent(SupplierReturnItemDO::getReturnNum, params.getReturnNum())
                .orderByDesc(SupplierReturnItemDO::getId)
                .page(params);
    }

}

