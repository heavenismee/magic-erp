package com.hys.app.mapper.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.erp.dos.StockTransferProductDO;
import com.hys.app.model.erp.dto.StockTransferProductQueryParams;
import com.hys.app.framework.database.WebPage;

/**
 * 调拨单商品明细的Mapper
 *
 * @author zs
 * @since 2023-12-12 16:34:15
 */
public interface StockTransferProductMapper extends BaseMapperX<StockTransferProductDO> {

    default WebPage<StockTransferProductDO> selectPage(StockTransferProductQueryParams params) {
        return lambdaQuery()
                .eqIfPresent(StockTransferProductDO::getStockTransferId, params.getStockTransferId())
                .eqIfPresent(StockTransferProductDO::getWarehouseEntrySn, params.getWarehouseEntrySn())
                .eqIfPresent(StockTransferProductDO::getWarehouseEntryBatchSn, params.getWarehouseEntryBatchSn())
                .eqIfPresent(StockTransferProductDO::getProductId, params.getProductId())
                .eqIfPresent(StockTransferProductDO::getProductSn, params.getProductSn())
                .eqIfPresent(StockTransferProductDO::getProductName, params.getProductName())
                .eqIfPresent(StockTransferProductDO::getProductSpecification, params.getProductSpecification())
                .eqIfPresent(StockTransferProductDO::getProductUnit, params.getProductUnit())
                .eqIfPresent(StockTransferProductDO::getProductPrice, params.getProductPrice())
                .eqIfPresent(StockTransferProductDO::getNum, params.getNum())
                .orderByDesc(StockTransferProductDO::getId)
                .page(params);
    }

}

