package com.hys.app.mapper.erp;

import org.apache.ibatis.annotations.Mapper;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;
import com.hys.app.model.erp.dos.FinanceItemDO;
import com.hys.app.model.erp.dto.FinanceItemQueryParams;
import com.hys.app.framework.database.WebPage;

/**
 * 财务明细 Mapper
 *
 * @author zsong
 * 2024-03-15 17:40:23
 */
@Mapper
public interface FinanceItemMapper extends BaseMapperX<FinanceItemDO> {

    default WebPage<FinanceItemDO> selectPage(FinanceItemQueryParams params) {
        return lambdaQuery()
                .eqIfPresent(FinanceItemDO::getSn, params.getSn())
                .eqIfPresent(FinanceItemDO::getSourceType, params.getSourceType())
                .eqIfPresent(FinanceItemDO::getSourceSn, params.getSourceSn())
                .eqIfPresent(FinanceItemDO::getAmountType, params.getAmountType())
                .betweenIfPresent(BaseDO::getCreateTime, params.getCreateTime())
                .orderByDesc(FinanceItemDO::getId)
                .page(params);
    }

}

