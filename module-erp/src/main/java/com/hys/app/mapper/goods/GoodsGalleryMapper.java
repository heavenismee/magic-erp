package com.hys.app.mapper.goods;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hys.app.framework.cache.MybatisRedisCache;
import com.hys.app.model.goods.dos.GoodsGalleryDO;
import org.apache.ibatis.annotations.CacheNamespace;

/**
 * GoodsGallery的Mapper
 * @author fk
 * @version 1.0
 * @since 7.1.0
 * 2020/7/21
 */
@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface GoodsGalleryMapper extends BaseMapper<GoodsGalleryDO> {
}
