package com.hys.app.mapper.goods;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hys.app.framework.cache.MybatisRedisCache;
import com.hys.app.model.system.dos.ShipTemplateDO;
import com.hys.app.model.system.vo.ShipTemplateSellerVO;
import org.apache.ibatis.annotations.CacheNamespace;

import java.util.List;

/**
 * 运费模版Mapper接口
 * @author duanmingyu
 * @version v1.0
 * @since v7.2.2
 * 2020-07-28
 */
@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface ShipTemplateMapper extends BaseMapper<ShipTemplateDO> {

    /**
     * 根据店铺id获取店铺运费模板集合
     * @return
     */
    List<ShipTemplateSellerVO> selectShipTemplateList();

}
