package com.hys.app.mapper.erp;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hys.app.framework.cache.MybatisRedisCache;
import com.hys.app.model.erp.dos.LendFormProduct;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 借出单产品Mapper
 * @author dmy
 * 2023-12-05
 */
@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface LendFormProductMapper extends BaseMapper<LendFormProduct> {

    /**
     * 查询待归还的借出单商品分页列表数据
     *
     * @param page 分页参数
     * @return
     */
    IPage<LendFormProduct> selectWaitReturnProductPage(Page page);

    /**
     * 查询产品已确认借出信息
     *
     * @param stockSn 入库单号
     * @param productId 产品ID
     * @return
     */
    List<LendFormProduct> selectByConfirm(@Param("stockSn") String stockSn, @Param("productId") Long productId);
}
