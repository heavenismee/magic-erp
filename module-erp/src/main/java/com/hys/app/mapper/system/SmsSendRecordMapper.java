package com.hys.app.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hys.app.framework.cache.MybatisRedisCache;
import com.hys.app.model.system.dos.SmsSendRecord;
import org.apache.ibatis.annotations.CacheNamespace;

/**
 * 短信发送记录Mapper
 * @author zhangsong
 * @version v1.0
 * @since v1.0
 * 2021-02-05 17:17:30
 */
@CacheNamespace(implementation= MybatisRedisCache.class,eviction= MybatisRedisCache.class)
public interface SmsSendRecordMapper extends BaseMapper<SmsSendRecord> {


}