package com.hys.app.mapper.goods;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.framework.util.StringUtil;
import com.hys.app.model.erp.dos.GoodsDO;
import com.hys.app.model.erp.dto.GoodsQueryParams;

/**
 * 商品的Mapper
 *
 * @author zs
 * 2023-12-26 15:56:58
 */
public interface GoodsMapper extends BaseMapperX<GoodsDO> {

    default WebPage<GoodsDO> selectPage(GoodsQueryParams params) {
        return lambdaQuery()
                .andX(StringUtil.notEmpty(params.getKeyword()),
                        w -> w.like(GoodsDO::getName, params.getKeyword()).or().like(GoodsDO::getSn, params.getKeyword()))
                .eqIfPresent(GoodsDO::getSn, params.getSn())
                .eqIfPresent(GoodsDO::getName, params.getName())
                .eqIfPresent(GoodsDO::getCategoryId, params.getCategoryId())
                .orderByDesc(GoodsDO::getId)
                .page(params);
    }

}

