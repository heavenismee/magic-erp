package com.hys.app.mapper.goods;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hys.app.framework.cache.MybatisRedisCache;
import com.hys.app.model.goods.dos.ParameterGroupDO;
import org.apache.ibatis.annotations.CacheNamespace;

/**
 * ParameterGroup的Mapper
 * @author fk
 * @version 1.0
 * @since 7.1.0
 * 2020/7/21
 */
@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface ParameterGroupMapper extends BaseMapper<ParameterGroupDO> {
}
