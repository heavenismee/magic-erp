package com.hys.app.framework.context.app;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * APP类型上下文
 * @author zs
 * @version 1.0
 * @since 7.2.2
 * 2021-08-26
 */
public class AppTypeContext {

    public static AppTypeEnum get(){
        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
//        String appTypeStr =  ThreadContextHolder.getHttpRequest().getHeader("AppType");
        String appTypeStr =  httpServletRequest.getHeader("AppType");
        return AppTypeEnum.valueOf(appTypeStr);
    }


}
