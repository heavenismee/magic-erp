package com.hys.app.framework.context.app;

/**
 * AppType枚举
 * @author zs
 * @version 1.0
 * @since 7.2.2
 * 2021-08-26
 */
public enum AppTypeEnum {

    Buyer("买家端"),

    Admin("平台管理端"),

    Shop("门店管理端"),

    Cashier("收银台"),

    Guide("智慧导购");

    private String description;

    AppTypeEnum(String description) {
        this.description = description;

    }

    public String description() {
        return description;
    }

}
