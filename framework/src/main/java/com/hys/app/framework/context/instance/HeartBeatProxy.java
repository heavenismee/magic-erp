package com.hys.app.framework.context.instance;

/**
 * 心跳检测代理类
 */
public interface HeartBeatProxy {

    /**
     * 服务心跳检测
     */
    void heartbeat();

}
