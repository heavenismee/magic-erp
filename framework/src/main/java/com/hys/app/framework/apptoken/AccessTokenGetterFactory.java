package com.hys.app.framework.apptoken;

import com.hys.app.framework.apptoken.model.AppTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * AccessTokenGetter Bean工厂
 * 根据AccesssTokenAppEnum获取唯一的 TokenGetter
 * @author kingapex
 * @version 1.0
 * @description TODO
 * @data 2021/8/29 13:03
 **/
@Service
public class AccessTokenGetterFactory {

    @Autowired(required = false)
    private List<AccessTokenGetter> getterList;

    /**
     * 根据AccesssTokenAppEnum获取唯一的 TokenGetter
     * @param appTypeEnum token app类型枚举
     * @return
     */
    public AccessTokenGetter getBean(AppTypeEnum appTypeEnum) {
        List<AccessTokenGetter> list = getterList.stream().filter(getter -> getter.id().equals(appTypeEnum)).collect(Collectors.toList());
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }
}
