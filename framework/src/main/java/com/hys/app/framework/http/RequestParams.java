package com.hys.app.framework.http;

import com.hys.app.framework.util.JsonUtil;

import java.util.HashMap;

/**
 * 请求参数
 *
 * @author zs
 * @since 2024-01-05
 */
public class RequestParams extends HashMap<String, Object> {

    public RequestParams put(String key, Object value){
        super.put(key, value);
        return this;
    }

    public String toJson(){
        return JsonUtil.objectToJson(this);
    }

}
