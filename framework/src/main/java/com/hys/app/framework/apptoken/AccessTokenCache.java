package com.hys.app.framework.apptoken;

import com.hys.app.framework.apptoken.model.AccessToken;

/**
 * access token缓存接口<br/>
 * 专门面向access token的缓存
 * @author kingapex
 * @data 2021/8/29 13:36
 * @version 1.0
 **/
public interface AccessTokenCache {

    /**
     * 向缓存中压入token
     * @param accessTokenIdentifier token的唯一标识
     * @param accessToken 要缓存的token
     */
    void put(AccessTokenIdentifier accessTokenIdentifier, AccessToken accessToken);

    /**
     * 由缓存中获取token
     * @param accessTokenIdentifier token的唯一标识
     * @return 缓存的token
     */
    AccessToken get(AccessTokenIdentifier accessTokenIdentifier);


}
