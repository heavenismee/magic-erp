package com.hys.app.framework.security.message;

import com.hys.app.framework.redis.RedisChannel;
import com.hys.app.framework.redis.redismq.RedisMsgReceiver;
import com.hys.app.framework.security.AuthenticationService;
import com.hys.app.framework.util.JsonUtil;

import java.util.List;

/**
 * 用户禁用消息接收器
 * @author kingapex
 * @version 1.0
 * @since 7.1.0
 * 2019/12/27
 * spring:
 *   application:
 *     name: base-api
 */

//@Component
public class UserDisableReceiver implements RedisMsgReceiver {

    public UserDisableReceiver(List<AuthenticationService> authenticationServices) {
        this.authenticationServices = authenticationServices;
    }

    private List<AuthenticationService> authenticationServices;

    @Override
    public String getChannelName() {
        return RedisChannel.USER_DISABLE;
    }

    @Override
    public void receiveMsg(String message) {
        UserDisableMsg userDisableMsg = JsonUtil.jsonToObject(message,UserDisableMsg.class);
        if (authenticationServices != null) {
            for (AuthenticationService authenticationService : authenticationServices) {
                authenticationService.userDisableEvent(userDisableMsg);
            }
        }

    }
}
