package com.hys.app.framework.apptoken;

import com.hys.app.framework.apptoken.model.AccessToken;
import com.hys.app.framework.apptoken.model.AppTypeEnum;

/**
 * access token获取器<br/>
 * 1、为了统一对外提供token的获取接口<br/>
 * 2、兼容各种第三方应用access token获取的情况<br/>
 * 3、提供统一的token缓存管理
 *
 * @author kingapex
 * @data 2021/8/29 11:51
 * @version 1.0
 **/
public interface AccessTokenGetter {

    /**
     * 由第三方服务api获取token
     * @param accessTokenIdentifier token唯一标识
     * @return
     */
    AccessToken getToken(AccessTokenIdentifier accessTokenIdentifier);

    /**
     * 定一个这个token getter的唯一凭证
     * 是和AccesssTokenAppEnum 一一对应的，
     * 也就是说有一个第三方应用，就有一个token getter
     * @return
     */
    AppTypeEnum id();
}
