package com.hys.app.framework.ws;

/**
 * IM 缓存前缀  create by liuyulei 2021/4/2  15:27
 *
 * @author liuyulei
 * @version: 1.0
 * @Since:  7.3.0
 *
 **/
public enum CacheImPrefix {
    /**
     * 未读消息数
     */
    UNREAD_NUM,

    /**
     * 消息缓存
     */
    IM_MESSAGE,

    /**
     * 会话id
     */
    SESSION_ID,

    /**
     * websocket的Token
     */
    IM_TOKEN;

    public String getPrefix() {
        return this.name() + "_";
    }
}
