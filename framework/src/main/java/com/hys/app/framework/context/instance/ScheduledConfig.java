package com.hys.app.framework.context.instance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.stereotype.Service;

/**
 * 定时任务线程池配置
 * @author liuyulei
 * @version 1.0
 * @since 7.2.2
 * 2020/11/18 0018  14:30
 */
@Service
public class ScheduledConfig implements SchedulingConfigurer {

    @Autowired
    private AsyncConfigurer asyncConfigurer;

    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        scheduledTaskRegistrar.setScheduler(asyncConfigurer.getAsyncExecutor());
//        scheduledTaskRegistrar.setScheduler(Executors.newScheduledThreadPool(10));

    }
}
