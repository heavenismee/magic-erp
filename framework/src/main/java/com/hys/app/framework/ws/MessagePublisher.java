package com.hys.app.framework.ws;

import com.hys.app.framework.ws.model.Message;

/**
 * Websocket管理
 * Author: Dawei
 * Datetime: 2022-07-18 15:27
 */
public interface MessagePublisher {


    /**
     * 发送消息
     * @param messageDO
     * @return
     */
    boolean sendMessage(Message messageDO);

}
